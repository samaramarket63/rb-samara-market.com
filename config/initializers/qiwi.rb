module Qiwi
  CONFIG = {}
end

param = YAML.load_file("#{Rails.root}/config/qiwi.yml")[Rails.env]

param.keys.each{|x| Qiwi::CONFIG[x.to_sym] = param[x].to_s }

require 'qiwi'