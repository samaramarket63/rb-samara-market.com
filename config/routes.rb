require 'sidekiq/web'
# == Route Map (Updated 2013-02-05 08:34)
#

Sm::Application.routes.draw do

  root to: 'web/home#index', url: 'samara-market.com'
  scope module: :web  do

    controller :categories, path: 'catalog', format: false do
      get '/:category_id(/page/:page)' => redirect{|params| "/catalog/#{params[:category_id]}.html"}
    end

    controller :notification, path: :notification, format: false do
      post :qiwi, action: :qiwi
    end

    controller :categories, path: 'catalog', format: true, constraints: {format: /(html)/} do
      get '/:category_id(/page/:page)', action: :show, as: :category
      get '/', action: :index, as: :categories
    end
    get 'catalog'  => redirect{|params| "/catalog.html"}
    resources :orders, only:[:new, :create, :index, :show]


    controller :cart do
      get 'cart', action: :index, format: true, constraints: {format: /(html)/}
      post 'cart/set_count', action: :set_count, as: 'cart_set_count'
      put 'cart/push', action: :push, as: 'add_to_cart'
      delete 'cart/pop', action: :pop, as: 'remove_from_cart'
      delete 'cart/destroy_products', action: :destroy, as: 'destroy_from_cart'
    end

    controller :brand, format: true, constraints: {format: /(html)/} do
      get '/brands/:id', action: :show, as: :brand
      get '/brands/:brand_id/products(/page/:page)', action: :products, as: 'brand_products'
      get '/brands', action: :index, as: :brands
    end

    controller :types, format: true, constraints: {format: /(html)/} do
      get '/types/:id', action: :show, as: :type
      get '/types/:type_id/products(/page/:page)', action: :products, as: 'type_products'
      get '/types', action: :index, as: :types
    end
    resources :addresses, only: [:index, :update, :destroy, :edit, :new, :create]

    resources :contacts, only: [:index, :create], format: true, constraints: {format: /(html)/}
    get '/viewed_products', constraints: {format: /(html)/}, format: true, controller: :categories, action: :viewed, as: :viewed_products
    get '/discounts(/page/:page)', constraints: {format: /(html)/}, format: true, controller: :categories, action: :discounts, as: :discounts

    resources :products, only: [:show], path: 'product',
      format: true, constraints: {format: /(html)/} do
      resources :reviews, only: [:create], format: false, constraints: {format: nil} do |variable|
        post '/vote_up', action: :vote_up
        post '/vote_down', action: :vote_down
        collection do
          get '/is_edit', action: 'edit?'
          delete '/', action: :destroy
        end
      end
    end

    controller :search, format: true, constraints: {format: /(html)/} do
      get '/search', action: :index, as: :search
    end


    controller :sessions do
      get '/user/is_auth', action: 'auth?'
      get '/user/logout', action: 'logout'
      post '/user/login', action: 'login'
      get '/user/login', action: 'login_page'
      match '/auth/:provider/callback', action: 'oauth'
    end

    controller :user do
      get '/user', action: :index
      post '/user', action: :create
      put '/user', action: :update, as: :update_user
      get '/user/data_verified/:field', action: :data_verified?, as: :data_verified_field, constraints: {field: /(phone|email)/}
      post '/user/data_verified/:field', action: :data_verified, constraints: {field: /(phone|email)/}, as: :data_verified
      get '/recovery_password', as: :recovery_password, action: :recovery_password
      post '/recovery_password', action: :recovery_password!
      post '/update_password', action: :update_password
    end

    controller :sitemap do
      get '/sitemap', action: :index, format: true, constraints: {format: /(xml)/}, as: 'sitemap'
      get '/sitemap_products', action: :products, format: true, constraints: {format: /(xml)/}, as: 'sitemap_products'
      get '/sitemap_categories', action: :categories, format: true, constraints: {format: /(xml)/}, as: 'sitemap_categories'
      get '/sitemap_brands', action: :brands, format: true, constraints: {format: /(xml)/}, as: 'sitemap_brands'
    end

    controller :static_content, format: true, constraints: {format: /(html)/} do
      get '/public_offer', action: 'public_offer'
      get '/privacy_policy', action: 'privacy_policy'
      get '/return_of_goods', action: 'return_of_goods'
      
    end

    controller :delivery, format: true, constraints: {format: /(html)/} do
      get '/delivery', action: :index, as: :delivery
    end

    controller :payment, format: true, constraints: {format: /(html)/} do
      get '/payments', action: :index, as: :payments
    end

  end


  namespace :admin, path: 'the_simpsons_admin' do

    resources :categories, only: [:index, :create, :update, :destroy, :show] do
      resources :adwords, controller: 'CategoriesAdwords' do
        post 'image', action: 'image'
        post 'background', action: 'background'
        post 'cover', action: 'cover'
        post :position, action: :position
      end
      collection do
        put ':id/position', action: :position
        get ':id/products', action: :products
      end
    end
    resources :adwords, only: [:index]
    resources :products_types, only: [:index, :create, :update, :destroy, :show] do
      get :products, action: :products
      resources :type_property, only: [:index, :create, :update, :destroy, :show] do
        collection do
          post ':id/position', action: :position
        end  
      end
    end

    resources :products_suppliers, only: [:index, :create, :update, :destroy, :show] do
      get :products, action: :products
    end
    resources :brands, only: [:index, :create, :update, :destroy, :show] do
      post 'logo', action: 'upload_logo'
      delete  'logo', action: 'destroy_logo'
      get :products, action: :products 
    end
    resources :products_groups, only: [:index, :create, :update, :destroy, :show] do
      get :products, action: :products
    end
    resources :products, only: [:index, :create, :update, :destroy, :show] do
      put :discount, action: 'create_discount'
      delete :discount, action: 'destroy_discount'
      put :item, action: 'create_item'
      delete 'item/:id', action: 'destroy_item'
      resources  :images, controller: :product_image , only: [:create, :destroy] do
        put :cover, action: :cover
        post :position, action: :position
      end
      resources :products_property, only: [:create, :update, :destroy] do 
        resources :products_property_value, only: [:create, :update, :destroy]
      end

      collection do
        put ':id/categories', action: :set_categories
        get ':id/categories', action: :get_categories
      end   
    end
    resources :articles, only: [:index, :update, :show, :create, :destroy] do
      post '/image', action: :image_upload
      delete '/image', action: :image_destroy
      get '/images', action: :images
    end
    resources :orders, only: [:index, :update, :show] do
      get '/bill', action: :bill
      get '/invoice', action: :invoice
      put :bil_qiwi, action: :bil_qiwi
    end
    controller :main do
      get '/login', as: :login, action: :login
      post '/login', action: :auth
      get '/logout', as: :logout, action: :logout
      get '/expire_sitemap', as: :expire_sitemap, action: :expire_sitemap
    end
    get 'properties_values', controller: :ProductsProperty , action: 'properties_values'
    root to: 'main#index' 

  end
  

end
