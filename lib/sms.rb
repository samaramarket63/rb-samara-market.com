require 'curb-fu'

module Sms

  PROVIDER = 'Sms24x7'
  AUTH_LOGIN = Sm::Application.config.sms_params[:login]
  AUTH_PASSWORD = Sm::Application.config.sms_params[:password]

  class SmsError < Exception; end
  class NumberFormat < SmsError; end
  class ProviderError < SmsError; end
  class InterfaceError < SmsError; end
  class AuthError < SmsError; end
  class NoLoginError < SmsError; end
  class BalanceError < SmsError; end
  class SpamError < SmsError; end
  class EncodingError < SmsError; end
  class NoGateError < SmsError; end
  class OtherError < SmsError; end
  class SessionTimeout < SmsError; end


  module Sms24x7
    SMS_HOST = 'http://api.sms24x7.ru'

    module_function

    def _communicate(request, cookie = nil, secure = true)
      request[:format] = 'json'
      protocol = secure ? 'https' : 'http'
      curl = CurbFu.post(SMS_HOST, request) do |curb|
        curb.cookies = cookie if cookie
      end
      raise InterfaceError, 'cURL request failed' unless curl.success?

      json = JSON.parse(curl.body)
      unless (response = json['response']) && (msg = response['msg']) && (error_code = msg['err_code'])
        raise InterfaceError, 'Empty some necessary data fields'
      end

      error_code = error_code.to_i
      if error_code > 0
        case error_code
          when 2 then raise AuthError
          when 3, 18
            if @_cookie
              @_cookie = nil
              raise SessionTimeout
            else
              raise AuthError
            end
          when 29 then raise NoGateError
          when 35 then raise EncodingError
          when 36 then raise BalanceError, 'No money'
          when 37, 38 then raise SpamError
          else raise OtherError, "Communication to API failed. Error code: #{error_code}"
        end
      end

      { :error_code => error_code, :data => response['data'] }
    end

    def login
      request = {
          method: 'login',
          email: AUTH_LOGIN,
          password: AUTH_PASSWORD
      }
      responce = _communicate(request)
      raise InterfaceError, "Login request OK, but no 'sid' set" unless (sid = responce[:data]['sid'])
      @_cookie = "sid=#{CGI::escape(sid)}"
    end

    def push(phone, text, params = {})
      login unless @_cookie
      request = {
        method: 'push_msg',
        phone: phone,
        text: text
      }.merge(params)
      begin
        responce = _communicate(request, @_cookie)
      rescue SessionTimeout => e
        login
        responce = _communicate(request, @_cookie)
      end

      check_and_result_push_msg(responce)
    end

    def check_and_result_push_msg(responce)
      data = responce[:data]
      unless (n_raw_sms = data['n_raw_sms']) && (credits = data['credits'])
        raise InterfaceError, "Could not find 'n_raw_sms' or 'credits' in successful push_msg response"
      end
      #{ :n_raw_sms => n_raw_sms.to_i, :credits => credits.to_f }
      data
    end
  end


  module_function


  def send(numbers, text)
    numbers = [numbers] unless numbers.kind_of?(Array)

    numbers.each {|number|  raise NumberFormat unless (number.to_s =~ /\A\d+{11}\Z/) }

    numbers.each {|number| Sms.const_get(PROVIDER).push(number, text)}

  end

end
