require 'net/http'
require 'uri'

module YaCleanweb
  class YaCleanwebException < Exception; end

  HOST = 'http://cleanweb-api.yandex.ru/1.0/'

  class CleanWeb
    def initialize(response)
      @id = response['id']
      @spam = response['text']['spam_flag']
      @links = response['links']
    end

    def spam?
      @spam == 'yes'
    end

    def captcha
      YaCleanweb.captcha(@id)
    end

  end


  module_function
  
    def check_spam(user_ip, body, subject, body_type = 'plain', subject_type = 'plain')
      request = {}

      request[:key] = Sm::Application.config.ya_clean_web
      request['body-'+body_type] = body
      request['subject-'+subject_type] = subject

      res = Net::HTTP.post_form(URI(HOST+'check-spam'), request)

      if res.code == '200'
        response = Hash.from_xml(res.body)
        return CleanWeb.new(response['check_spam_result'])
      else
        raise YaCleanwebException
      end     

    end

    def captcha(id = nil)
      uri = URI(HOST+'get-captcha')
      
      request = {}
      request[:key] = Sm::Application.config.ya_clean_web
      request[:id] = id if id

      uri.query = URI.encode_www_form(request)

      res = Net::HTTP.get_response(uri)

      if res.code == '200'
        response = Hash.from_xml(res.body)
        return {
          url: response['get_captcha_result']['url'],
          uid: response['get_captcha_result']['captcha'],
          id: id
        }
      else
        raise YaCleanwebException
      end  

    end

    def captcha_valid?(captcha, value, id = nil)
      uri = URI(HOST+'check-captcha')
      request = {}
      request[:key] = Sm::Application.config.ya_clean_web
      request[:id] = id if id

      request[:captcha] = captcha
      request[:value] = value

      uri.query = URI.encode_www_form(request)

      res = Net::HTTP.get_response(uri)

      if res.code == '200'
        response = Hash.from_xml(res.body)
        return response['check_captcha_result'].has_key?('ok')
      else
        raise YaCleanwebException
      end  

    end



end