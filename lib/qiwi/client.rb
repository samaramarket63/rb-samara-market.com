# encoding: UTF-8
module Qiwi
  class Client


    def initialize
      @headers = {
        'Accept' => 'application/json',
        'Authorization' => 'Basic ' + Base64.encode64([Qiwi::CONFIG[:api_uid],Qiwi::CONFIG[:api_key]].join(':'))
      }
      @provider = Net::HTTP.new(Qiwi::CONFIG[:host], Qiwi::CONFIG[:port])
      @provider.use_ssl = true
      @provider.verify_mode = OpenSSL::SSL::VERIFY_CLIENT_ONCE
    end

    def bil(user_phone, amount, bil_id)
      data = {
        user: "tel:#{user_phone}",
        amount: amount,
        ccy: :RUB,
        comment: 'Оплата заказа № '+bil_id+' в интернет магазине Самара-Маркет',
        lifetime: (Time.now + 2.day).iso8601,
        prv_name: 'Интернет магазин Самара-Маркет (samara-market.com, самара-маркет.рф)'
      }

      JSON.parse(@provider.put2(api("bills/#{bil_id}"), data.to_param, @headers).body)['response']

    end

    def check_bil(bil_id)
      JSON.parse(@provider.get2(api("bills/#{bil_id}"),@headers).body)['response']
    end

    def rejected_bil(bil_id)
      data = {
        status: 'rejected'
      }

      JSON.parse(@provider.patch(api("bills/#{bil_id}"), data.to_param, @headers).body)['response']

    end

    private
      def api path = ''
        '/api/v2/prv/'+Qiwi::CONFIG[:api_uid].to_s+'/'+path
      end

  end
end