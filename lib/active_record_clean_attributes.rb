module CleanAttributes
  module ClassMethods

    def strip_attributes(*attributes)
      attributes.each do |attribute|
        before_save do |record|
          record[attribute].to_s.strip! unless record[attribute].nil?
        end
      end

    end
    
  end
end