# encoding: UTF-8
require 'csv'
require 'sequel'

namespace :trade do

  desc "Выдаёт csv, параметры [:output,:fields, :categories, :return_categories,:status, :types, :brands, :product_ids]"
  task :get_csv, [:output,:fields, :categories, :return_categories,:status, :types, :brands, :product_ids] do |t, args|
    hidden_field = [:created_at, :updated_at, :ratio, :ratio_u, :product_reviews_count, :sort_price]  
    if args[:output].nil?
      file_name = Time.now.to_s(:db)
    else
      file_name = args[:output]
    end  

    finder_options = {}

    fields = args[:fields].split(';').collect!{|x| x.to_sym} unless  args[:fields].nil? 
    categories = args[:categories].split(';').collect!{|x| x.to_i} unless  args[:categories].nil? 
    finder_options[:status] = args[:status].split(';').collect!{|x| x.to_i} unless  args[:status].nil? 
    finder_options[:type] = args[:types].split(';').collect!{|x| x.to_i} unless  args[:types].nil? 
    finder_options[:brand] = args[:brands].split(';').collect!{|x| x.to_i} unless  args[:brands].nil? 
    finder_options[:id] = args[:product_ids].split(';').collect!{|x| x.to_i} unless  args[:product_ids].nil? 

    DB = Sequel.connect(Rails.configuration.database_configuration[Rails.env])
   
    products = DB[:products]


    if !fields.nil? and fields.any? and fields[0] != 'all'
      fields = fields & products.columns
    else
      fields = products.columns
    end
    fields = fields - hidden_field

    products = products.select(*fields)

    if !categories.nil? and categories.any? and categories[0] != 'all'
      puts categories.to_s
      ids = DB[:category_to_products].select(:product_id)
      .where(category_id:categories).distinct(:product_id).collect{|x| x[:product_id]}

      if finder_options[:id].nil?
        finder_options[:id] = ids
      else
        finder_options[:id] = finder_options[:id] + ids
      end
      
    end

    product_categories = {}

    if args[:return_categories] == 'true' and fields.include?(:id)
      
      DB[:category_to_products].select(:product_id,:category_id)
      .where(product_id: products.select_map).each do |c|
        product_categories[c[:product_id]] = [] if product_categories[c[:product_id]].nil?
        product_categories[c[:product_id]].push c[:category_id]
      end
    end

    [:id, :status, :types, :brands].each do |v|
      if !finder_options[v].nil? and finder_options[v].any? and finder_options[v][0] != 'all'
        products = products.where(v => finder_options[v].uniq)
      end
    end

    CSV.open(Rails.root.join("../trade/export/#{file_name}.csv"),'w+',col_sep: ';', quote_char: '"') do |csv|
      fields.push(:categories) if product_categories.size > 0

      csv << fields 

      products.each{|x| csv << (product_categories[x[:id]] ? (x.values.push(product_categories[x[:id]].join(';'))) : x.values)}
      
    end

    puts "Всего экспортированно #{products.count}"
  end

  desc "Обновляет товары из прайс листа"
  task :update_from_csv, [:price] => :environment do |t, args|
    price_list = CSV::read(Rails.root.join('../trade/import/update/'+args[:price]+'.csv'), col_sep: ';',headers:true,return_headers: true)
    hidden_field = [:categories, :created_at, :updated_at, :ratio, :ratio_u, :product_reviews_count, :sort_price, :id].collect{|x| x.to_s}

    fields = (price_list.headers & Product.column_names.collect{|x| x.to_s}) - hidden_field 

    begin
  
      price_list.each_with_index do |p,i|
        next if i == 0
        product = Product.find(p['id'])

        fields.each {|x| product.send("#{x}=",p[x])}
        product.category_ids = p['categories'].split(';') if p['categories']
        product.save!
      end

    rescue Exception => e
      puts e
    end
  
  end

end