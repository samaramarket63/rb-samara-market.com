# encoding: UTF-8

require 'csv'
require 'sequel'

namespace :prices do

  desc 'Сравнивает прайс лист поставщика с товарами в магазине'
  task :run_csv, [:supplier,:price] => :environment do |t, args|
    if args[:supplier].blank?
      puts 'Не передан ID поставщика'
      puts '[\'ID\',\'price_name\']'
      exit 1
    end
    if args[:price].blank?
      puts 'Не передано наименование прайс листа'
      puts '[\'ID\',\'price_name\']'
      exit 1
    end

    price = Rails.root.join('../trade/suppliers/prices/'+args[:price]+'.csv')
    
    supplier_products = {}

    CSV.foreach(price,col_sep: ';', quote_char: '"') do |row|
      supplier_products[Digest::MD5.hexdigest(row[0].gsub(/[^[:alpha:]0-9]+/,'').mb_chars.downcase)] = {name: row[0], price: row[1].to_i}
    end

    DB = Sequel.connect(Rails.configuration.database_configuration[Rails.env])

    shop_sup_products = {}
    shop_sup_products_join = {}

    DB[:product_to_suppliers].select(:id, :price, :product_id, :uid).where(supplier_id: args[:supplier]).each do |row|
      key = Digest::MD5.hexdigest(row[:uid].gsub(/[^[:alpha:]0-9]+/,'').mb_chars.downcase)
      shop_sup_products[key] = {
        id: row[:id],
        price: row[:price],
        product_id: row[:product_id],
        name: row[:uid],
      }
      shop_sup_products_join[row[:product_id].to_i] = key
    end

    shop_sup_products_array = shop_sup_products.keys.sort
    supplier_products_array = supplier_products.keys.sort

    CSV.open(Rails.root.join('../trade/suppliers/prices/export/'+args[:price]+"_#{Time.now.to_s(:db)
}.csv"),'w+',col_sep: ';', quote_char: '"') do |csv|
      #найдём товары которых нет в наличии
      a = (shop_sup_products_array - supplier_products_array).map{|x| shop_sup_products[x][:product_id]}
csv << ['id','status','price','supplier_price','discount_type_id','discount_value','name','supplier_uid','supplier_id']
      csv << ['id','статус','цена','закупочная','Тип скидки(1-проценты,2-в рублях,3-супер цена)','Значение скидки','имя','имя у поставщика','id поставщика']
      
      if a.size > 0
        puts 'Не найденно в прайс листе '+a.size.to_s
        csv << [nil,'Не найденные в прайс листе']
        DB[:products].select(:id, :name, :status, :price, :discount_type_id, :discount_value).where(id: a).each do |p|
          csv << [
            p[:id],
            p[:status],
            p[:price].to_i,
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:price].to_i,
            p[:discount_type_id].to_i,
            p[:discount_value].to_i,
            p[:name],
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:name],
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:id]
          ]
        end
      end

      #найдём товары с изменившейся ценой
      a = (shop_sup_products_array & supplier_products_array).delete_if do |x| 
        shop_sup_products[x][:price].to_i == supplier_products[x][:price].to_i
      end

      a.map!{|x| shop_sup_products[x][:product_id] }

      ab = DB[:products].select(:id, :name, :status, :price, :discount_type_id, :discount_value).where(status: 30, id: a).to_a

      if ab.size > 0
        puts 'На '+ab.size.to_s+' изменилась цена'
        csv << [nil,'Изменилась цена']
        ab.each do |p|
          csv << [
            p[:id],
            p[:status],
            p[:price].to_i,
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:price].to_i,
            p[:discount_type_id].to_i,
            p[:discount_value].to_i,
            p[:name],
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:name],
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:id]
          ]
        end
      end

      #найдём товары с изменившейся ценой и появившиеся в наличии
      ab = DB[:products].select(:id, :name, :status, :price, :discount_type_id, :discount_value).where(id: a).where('status <> 30').to_a

      if ab.size > 0
        puts 'Появились в наличии и изменилась цена на '+ab.size.to_s
        csv << [nil,'Появились в наличии и изменилась цена']
        ab.each do |p|
          csv << [
            p[:id],
            p[:status],
            p[:price].to_i,
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:price].to_i,
            p[:discount_type_id].to_i,
            p[:discount_value].to_i,
            p[:name],
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:name],
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:id]
          ]
        end
      end

      #найдём товары появившиеся в наличии

      a = (shop_sup_products_array & supplier_products_array).delete_if do |x| 
        shop_sup_products[x][:price].to_i != supplier_products[x][:price].to_i
      end
      a.map!{|x| shop_sup_products[x][:product_id] }

      ab = DB[:products].select(:id, :name, :status, :price, :discount_type_id, :discount_value).where(id: a).where('status <> 30').to_a
      if ab.size > 0
        puts 'Появились в наличии '+ab.size.to_s
        csv << [nil,'Появились в наличии']
        ab.each do |p|
          csv << [
            p[:id],
            p[:status],
            p[:price].to_i,
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:price].to_i,
            p[:discount_type_id].to_i,
            p[:discount_value].to_i,
            p[:name],
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:name],
            shop_sup_products[shop_sup_products_join[p[:id].to_i]][:id]
          ]
        end
      end

    end


  end

  desc 'Обновляет товары в магазине из csv формат - ID товара;статус;цена;закупочная цена'
  task :simple_update_csv, [:price] => :environment do |t, args|

    if args[:price].blank?
      puts 'Не передано наименование прайс листа'
      puts '[\'ID\',\'price_name\']'
      exit 1
    end
    
    products = {}


    i = 0

    Product.transaction do
    # ID товара;статус;цена;закупочная цена
    CSV.foreach(Rails.root.join('../trade/suppliers/prices/update/'+args[:price]+'.csv'),col_sep: ';', quote_char: '"') do |row|
      if row[0].to_i != 0
        #products[row[0].to_i] = {
        #  status: row[1],
        #  price: row[2]
        #}
        Product.update(row[0].to_i, status: row[1].to_i, price: row[2].to_f)
      end
    end
      #Product.select('id, status, price').find(products.keys).each do |p|
      #  if p.status.to_i != products[p.id][:status].to_i or p.price.to_i != products[p.id][:price].to_i
      #    p.update_attributes(status: products[p.id][:status].to_i, price: products[p.id][:price].to_i)
      #    i = i+1
      #  end
      #end
    end      
    puts 'Обновленно '+i.to_s
  end

  desc 'Найти новые товары из прайса поставщика [:supplier,:price]'
  task :get_new, [:supplier,:price] => :environment do |t, args|
    if args[:supplier].blank?
      puts 'Не передан ID поставщика'
      puts '[\'ID\',\'price_name\']'
      exit 1
    end
    if args[:price].blank?
      puts 'Не передано наименование прайс листа'
      puts '[\'ID\',\'price_name\']'
      exit 1
    end
    price = Rails.root.join('../trade/suppliers/prices/'+args[:price]+'.csv')

    supplier_products = {}

    CSV.foreach(price,col_sep: ';', quote_char: '"') do |row|
      supplier_products[Digest::MD5.hexdigest(row[0].gsub(/[^[:alpha:]0-9]+/,'').mb_chars.downcase)] = row
    end

    DB = Sequel.connect(Rails.configuration.database_configuration[Rails.env])

    shop_products = []

    DB[:product_to_suppliers].select(:uid).where(supplier_id: args[:supplier]).each do |row|
      shop_products.push(Digest::MD5.hexdigest(row[:uid].gsub(/[^[:alpha:]0-9]+/,'').mb_chars.downcase))
    end

    CSV.open(Rails.root.join('../trade/suppliers/prices/new_products/'+args[:price]+".csv"),'w+',col_sep: ';', quote_char: '"') do |csv|
      (supplier_products.keys - shop_products).each do |p|
        csv << supplier_products[p]
      end
    end

  end  

  desc 'Обновление товара по прайсу id, status, price,supplier_price, discount_type_id, discount_value, name, supplier_uid, supplier_id'
  task :update_csv, [:price] => :environment do |t, args|
    if args[:price].blank?
      puts 'Не передано наименование прайс листа'
      puts '[\'price_name\']'
      exit 1
    end

    price_list = CSV::read(Rails.root.join('../trade/suppliers/prices/update/'+args[:price]+'.csv'), col_sep: ';',headers:true,return_headers: true)

    begin

      data = []

      price_list.each do |p|
        if p['id'].to_i != 0
          data.push({
            product_id: p['id'].to_i,
            supplier_id: p['supplier_id'].to_i,
            product: {
              status: p['status'],
              price: p['price'],
              discount_type_id: (p['discount_type_id'].to_i > 0 ? p['discount_type_id'].to_i : nil),
              discount_value: (p['discount_value'].to_i > 0 ? p['discount_value'].to_i : nil)
            },
            supplier: {
              price: p['supplier_price'].to_i,
              uid: p['supplier_uid']
            }
          })
        end
      end

      Product.transaction do

        data.each do |data_item|
          puts "ID = #{data_item[:product_id]}"
          Product.find(data_item[:product_id]).update_attributes!(data_item[:product])
          puts "Поставщик = #{data_item[:supplier_id]}"
          ProductToSupplier.find(data_item[:supplier_id]).update_attributes!(data_item[:supplier])

        end

        
      end
      
    rescue Exception => e
      puts e
    end

  end
end


