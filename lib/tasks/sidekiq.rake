namespace :sidekiq do

  desc "start daemon sidekiq"
  task :start do |t, args|
    sh "sidekiq -d -L ./log/sidekiq.log -P ./tmp/pids/sidekiq.pid"
    puts "start daemon sidekiq"
  end

  desc "stop daemon sidekiq"
  task :stop do |t, args|
    begin
      sh "sidekiqctl shutdown ./tmp/pids/sidekiq.pid"      
    rescue Exception => e
      puts e
    end

    puts "shutdown daemon sidekiq"
  end

  desc "restart daemon sidekiq"
  task :restart do |t, args|
    puts "restart daemon sidekiq"
    sh "rake sidekiq:stop"
    sh "rake sidekiq:start"
  end

end