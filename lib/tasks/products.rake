# encoding: UTF-8

require 'csv'


namespace :products do
  desc "Обновляет счётчики в категориях"
  task :update_counters, [:file] => :environment do |t, args|
    Category.select('id,products_count,products_visible,products_in_stock, types_ids').all.each do |c|
      c.types_ids = c.type_ids.to_a
      c.products_count = c.products.count
      c.products_visible = c.products.visible.count
      c.products_in_stock = c.products.in_stock.count
      c.save(validate: false)
    end
  end
  desc "Добавляет продукты в каталог из прайса в формате CSV"
  task :add_from_csv, [:file] => :environment do |t, args|
    db = Rails.configuration.database_configuration[Rails.env]
    backup_name = Time.now.strftime("%m_%d_%Y_%H%M")
    tables = %w(category_to_products join_product_properties_of_values product_images product_properties product_to_suppliers products products_suppliers products_type_property_values)
    import = Rails.root.join('../trade/import')
    import_images_path = import.join("images/#{args[:file]}")
    import_prices = import.join("prices/#{args[:file]}.csv")
    csv_params = {col_sep: ';', quote_char: '"', headers: true, return_headers: false}
    begin
      
      fields = %w(name model_name link_rewrite status type brand price meta_description images meta_keywords short_description description categories suppliers properties_meta properties)
      aaa = CSV.open(import_prices,csv_params).read.headers.sort
      puts fields.sort
      puts aaa
      raise 'Строка заголовков в прайсе не верна'  if aaa != fields.sort


      sh "mysqldump -u#{db['username']} -p#{db['password']} #{db['database']} #{tables.join(' ').tr("'",'')} > #{Rails.root}/../db_dumps/#{backup_name}.sql"
      puts 'Создан дамп '+backup_name
      begin

        Product.transaction do
          str_i = 0
          CSV.foreach(import_prices,csv_params) do |row|
            str_i = str_i.next
            puts "Строка #{str_i}"
            d = row.to_hash
            # puts '--------------------------------------'
            # puts d
            # puts '--------------------------------------'
            p = Product.new
            p.name = d['name']
            if d['link_rewrite'].blank?
              p.link_rewrite = I18n.transliterate(d['name']).gsub(/[^A-z^0-9]+/,'_').gsub(/(\A_)+|(_\z)++/,'').gsub(/[_]+/,'_').downcase 
            else
              p.link_rewrite = d['link_rewrite']
            end
            puts d['price']
            p.type_id = d['type']
            p.brand_id = d['brand']
            p.price = d['price']
            p.status = 0
            p.model_name = d['model_name']
            p.meta_description = d['meta_description']
            p.meta_keywords = d['meta_keywords']
            p.short_description = d['short_description']
            p.description = d['description']
            p.save!
            p.category_ids = d['categories'].split(';').collect{|x| x.to_i}
            puts p
            d['images'].to_s.split(';').each do |x|
              i = p.images.new
              i.image = File.open(import_images_path.join(x))
              i.save!
            end

            props = {}
            d['properties_meta'].to_s.split(';').each{|x|x =x.split(':'); props[x[0]]=x[1].to_s.strip }
            d['properties'].to_s.split('$').each do |x|
              p x
              key_values = x.split(':')

              puts '*****'
              puts key_values[0]
              puts props[key_values[0]]
              puts '*****'
              property = p.properties.find_by_property_id(props[key_values[0]])

              key_values[1].split(';').each do |v|
                value = ProductsTypePropertyValues.where(value:v.to_s.strip).first_or_create
                property.values << value
              end

            end
  
            p.status = d['status']
            p.save!

            d['suppliers'].to_s.split('$').each do |x|
              sup = x.split(';')
              raise "В строке #{str_i} не существующий поставщик" if sup[2].nil?
              supplier = ProductsSupplier.find(sup[0])

              p.items.create({
                uid: sup[2],
                price: sup[1],
                supplier_id: supplier.id
              }) 
            end


          end
        end      

      rescue Exception => e
        puts 'Ошибка импорта'
        puts '**********************'
        puts e
        puts '**********************'
          begin
            sh "mysql -u#{db['username']} -p#{db['password']} #{db['database']} < #{Rails.root}/../db_dumps/#{backup_name}.sql"   
            puts 'База востановленна в состяние '+backup_name
          rescue Exception => e
          puts 'Всё, жопа, база не востановленна из дампа '+backup_name
          puts '**********************'
          puts e
          puts '**********************'
          end
      end
    rescue Errno::ENOENT => e
      puts 'Файл не найден'
      puts '**********************'
      puts e
      puts '**********************'
    rescue Exception => e
      puts 'Ошибка создания бэкапа'
      puts '**********************'
      puts e
      puts '**********************'
    end

  end
end
