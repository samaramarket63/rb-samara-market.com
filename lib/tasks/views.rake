namespace :views do

  desc "last views products"
  task :products_last do |t, args|
    puts Rails.env
    time = Time.now
    DB = Sequel.connect(Rails.configuration.database_configuration[Rails.env])
    views = DB[:product_views].with_sql("SELECT COUNT(*) as views, `product_id` FROM product_views WHERE (`created_at` > '#{(time - 3.day).to_s(:db)}') GROUP BY `product_id` ORDER BY `product_id`")

    DB[:products].update(last_views: 0)

    views.each do |v|
      DB[:products].where(id: v[:product_id]).update(last_views: v[:views])
    end

    DB[:product_views].where("`created_at` < '#{(time - 3.day).to_s(:db)}'").delete

    ActionController::Base.new.expire_fragment(:main_page_popular_products)
    ActionController::Base.new.expire_fragment(/product_page_like_products_\d+/)
  end

  desc "last views categories"
  task :categories_last do |t, args|
    puts Rails.env
    time = Time.now
    DB = Sequel.connect(Rails.configuration.database_configuration[Rails.env])
    views = DB[:category_views].with_sql("SELECT COUNT(*) as views, `category_id` FROM category_views WHERE (`created_at` > '#{(time - 3.day).to_s(:db)}') GROUP BY `category_id` ORDER BY `category_id`")

    DB[:categories].update(last_views: 0)

    views.each do |v|
      DB[:categories].where(id: v[:category_id]).update(last_views: v[:views])
    end

    DB[:category_views].where("`created_at` < '#{(time - 3.day).to_s(:db)}'").delete
    ActionController::Base.new.expire_fragment(:main_page)

  end

end
