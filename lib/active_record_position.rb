module ActsAsPositions
  module ClassMethods
    def acts_as_position(options = {})
      options = {
        position_column: :position,
        position_max: 100,
        collection: []
      }.merge(options)

      options[:collection].to_a

      cattr_accessor :acts_as_position_options
      self.acts_as_position_options = options
      after_destroy :reset_position
      before_create :set_position

      default_scope order("#{self.table_name.to_s + '.' + options[:position_column].to_s}")

      scope :position, order("#{self.table_name.to_s + '.' + options[:position_column].to_s}")

      scope :position_revers, order("#{self.table_name.to_s + '.' + options[:position_column].to_s} DESC")

      include InstanceMethods
    end
  end
  module InstanceMethods
    def position_column
      acts_as_position_options[:position_column]
    end

    def position_max
      acts_as_position_options[:position_max]
    end
    def collection_for_update_position
      collection = self
      acts_as_position_options[:collection].each do |step|
        collection = collection.send(step)
      end
      return collection = self.class.position.all if acts_as_position_options[:collection].size == 0
      return collection.position
    end

    def move_to_before_of (node)
      if self[position_column].next != node[position_column]
        if self[position_column]>node[position_column]
          opts = { condition: ">= #{node[position_column]} and #{position_column} < ?", action: :next, new_pos: node[position_column] }
          node[position_column] = node[position_column].next
        else
          opts = { condition: "< #{node[position_column]} and #{position_column} > ?", action: :pred, new_pos: node[position_column].pred }
        end
        self.send :move_to,opts
      end
    end 

    def move_to_after_of (node)
      if self[position_column] != node[position_column].next
        if self[position_column]>node[position_column]
          opts = { condition: "> #{node[position_column]} and #{position_column} < ?", action: :next, new_pos: node[position_column].next }
        else
          opts = { condition: "<= #{node[position_column]} and #{position_column} > ?", action: :pred, new_pos: node[position_column] }
          node[position_column] = node[position_column].next
        end
        self.send :move_to,opts
      end
    end
    protected
      def move_to(opts={})   
        collection = collection_for_update_position.where("#{position_column} " << opts[:condition], self[position_column])
        self.transaction do
          collection.each do |node|
            node[position_column] = node[position_column].send(opts[:action])
            node.save
          end
          self[position_column] = opts[:new_pos]
          self.save
        end
      end

      def reset_position
        collection_for_update_position.where("#{position_column} > #{self[position_column]}").update_all("#{position_column} = #{position_column}-1")
      end

      def set_position
        if collection_for_update_position and (prev = collection_for_update_position.last)
          self[position_column] = prev[position_column].next
        else
          self[position_column] = 0
        end   

        if position_max <= self[position_column]
          self.errors.add(position_column, "Wrong position, maximum #{position_max}, have #{self[position_column]}")
          return false
        end
        
      end
  end
end