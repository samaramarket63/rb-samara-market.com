# encoding: UTF-8
# == Schema Information
#
# Table name: user_password_recoveries
#
#  id           :integer          not null, primary key
#  user_auth_id :integer
#  code         :string(255)
#  attempts     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class UserPasswordRecovery < ActiveRecord::Base
  LIMIT = 300

  belongs_to :user_auth

  
  def self.get(auth)
    u = UserPasswordRecovery.where('user_auth_id = ? and updated_at > ?', auth.id, 180.minutes.ago).first
    if u.nil?
      u = UserPasswordRecovery.new
      u.user_auth = auth
      u.attempts = 0
    end
    u
  end

  def regenerate_password!
    letters = (('a'..'z').to_a+(0..9).to_a)
    password = (0..5).map{ letters[rand(letters.length-1)] }.join
    self.user_auth.password = password
    self.user_auth.save
    self.send_msg(password)
    self.attempts = self.attempts.to_i.next
    self.save
  end



  def ban?
    self.attempts.to_i > UserPasswordRecovery::LIMIT
  end

  def exp_minutes
    self.updated_at - 180.minutes.ago
  end

  def send_msg(password)
    Sms.send(
      '7'<<self.user_auth.user.phone,
       "Новый пароль для samara-market #{password}"
    )
  end

end
