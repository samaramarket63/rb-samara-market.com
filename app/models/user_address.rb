# encoding: UTF-8
# == Schema Information
#
# Table name: user_addresses
#
#  id          :integer          not null, primary key
#  postcode    :string(15)
#  note        :text
#  market_note :text
#  adds        :string(250)      not null
#  user_id     :integer
#  city_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#



class UserAddress < ActiveRecord::Base

  attr_accessible :adds, :note, :postcode, as: :user
  attr_accessible :adds, :market_note, :note, :postcode, as: :admin

  strip_attributes :adds, :note, :postcode

  validates :adds, :note, :market_note, length: { maximum: 250 },
    format: { 
      with: /\A[A-zА-яёЁ &0-9()\-+,.#№!?"'\\\/;]+\z|\A\z/,
      message: 'Только символы русского и латинского алфавита, числовые, знаки припенания и (),№,#,/'
    }

  validates :adds, presence: true

  validates :postcode, length: { maximum: 15 },
    numericality: { only_integer: true }, if: '!postcode.blank?'

  belongs_to :city
  belongs_to :user
  has_many :orders, foreign_key: 'address_id', dependent: :nullify

  def address_editing_allowed?
    self.orders.address_editing_allowed.count != 0
  end

end
