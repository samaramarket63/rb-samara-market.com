# encoding: UTF-8
# == Schema Information
#
# Table name: orders
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  payment_id     :integer
#  delivery_id    :integer
#  address_id     :integer
#  status         :integer
#  paid           :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  note           :text
#  delivery_price :float
#  cart_id        :integer
#  market_note    :text
#  discount       :float
#  paid_bonus     :float
#

class Order < ActiveRecord::Base
  include ActionView::Helpers
  # attr_accessible :title, :body

  belongs_to :user
  belongs_to :cart
  belongs_to :payment
  belongs_to :delivery
  belongs_to :address, class_name: 'UserAddress'
  has_many :history_status, class_name: 'OrderStatusHistory'

  STATUS_LIST = {
    ##Address errors // Пользователь не может редактировать адрес
    0   => 'Новый( Ждёт обработки )',
    1   => 'Обрабатывается',
    10  => 'В процессе доставки',
    ## Transit
    20  => 'Оплачено через Qiwi',
    ## Payments
    50  => 'Выставлен счёт Qiwi',
    51  => 'Выставленный счёт Qiwi отменён',
    ##Errors
    60  => 'Ошибки в оформлении заказа (смотрите примечание)',
    61  => 'Нет обратной связи с покупателем',
    62  => 'Ошибка оплаты Qiwi',
    62  => 'Время на оплату счёта Qiwi истекло',
    ##Address errors // Пользователь может редактировать адрес
    71  => 'Ошибка в адресе',
    72  => 'Ошибка в адресе (почтовый индекс)',
    73  => 'Ошибка в адресе (адрес)',
    74  => 'Ошибка в адресе (не указан дом/кабинет/офис)',
    75  => 'Ошибка в адресе (не указан точный адрес)',
    ##Closed
    96  => 'Брак( возврат/замена )',
    97  => 'Возврат',
    98  => 'Нет в наличии',
    99  => 'Отменён',
    100 => 'Выполнен'
  }

  before_create :default_status
  before_save :status_history
  
  scope :open, where(status: (0..80))
  scope :closed, where(status: (81..100))
  scope :address_error, where(status: (71..80))

  scope :address_editing_allowed, where(status:(0..80))

  validates :note, :market_note, length: { maximum: 500 },
    format: { 
      with: /\A[A-zА-яёЁ &0-9()\-+,.#№!?"'\\\/;]+\z|\A\z/,
      message: 'Только символы русского и латинского алфавита, числовые, знаки припенания и (),№,#,/'
    }
  def bill
    'А'+'-'+self.id.to_s
  end

  def self.by_bil(bil)
    Order.find_by_id(bil[/А-\d+/].to_s[/\d+/])
  end

  def address_exp?
    if self.address
      self.created_at < self.address.updated_at
    else
      return nil
    end
  end

  def text_status
    STATUS_LIST[self.status]
  end

  def all_status
    STATUS_LIST
  end

  def total
    self.delivery_price+(self.cart.sum_goods).ceil
  end

  private 
    def default_status
      self.status = 0      
    end

    def status_history
      if self.status_changed?
        self.history_status.create status: self.status
      end
    end

end
