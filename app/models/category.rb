# encoding: UTF-8
# == Schema Information
#
# Table name: categories
#
#  id               :integer          not null, primary key
#  name             :string(100)      not null
#  link_rewrite     :string(100)      not null
#  status           :boolean          not null
#  parent_id        :integer
#  meta_keywords    :string(400)
#  meta_description :string(400)
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  lft              :integer
#  rgt              :integer
#  depth            :integer
#  brands_ids       :text
#  adword_id        :integer
#  types_ids        :text
#

class Category < ActiveRecord::Base

  serialize :brands_ids
  serialize :types_ids
  
  after_save :touch_all

  before_update :update_status
  before_create :set_defaults
  acts_as_nested_set dependent: :destroy

  attr_accessible :name, :link_rewrite, :parent_id, :status,
    :meta_keywords, :meta_description, :description, :show

  strip_attributes :name, :link_rewrite, :meta_keywords, :meta_description

  validates :name, :link_rewrite, length: { in: 2..100 }, presence: true 
  validates :meta_keywords, :meta_description, length: { maximum: 400 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+,.]+\z|\A\z/}
  validates :name, format: { 
    with: /\A[A-zА-яёЁ,. &0-9()\-+]+\z/}
  validates :link_rewrite, uniqueness: true, format: { 
    with: /\A[A-z_0-9()\-+]+\z/}
  #validates :status, inclusion: { in: [true, false] }

  scope :tree, order(:lft)

  scope :public, where(status: true)

  scope :lasts, where('(categories.rgt - categories.lft)=1')

  scope :order_by_last_views, order('last_views DESC')

  # has_and_belongs_to_many :products

  has_many :category_to_product, dependent: :destroy
  has_many :products, through: :category_to_product
  has_many :brands, through: :products, uniq: true 
  has_many :types, through: :products, uniq: true do
    def visible
     where('`products`.`status` IN (20,30)')
    end 
  end 


  MAIN = 1

  has_many :adwords, class_name: 'CategoryAdvertising',  dependent: :destroy

  belongs_to :adword, class_name: 'CategoryAdvertising', foreign_key: :adword_id

  has_many :childrens, class_name: :Category,
    foreign_key: :parent_id,
    order: :lft,
    inverse_of: :parent,
    conditions: ['status = true and depth=1'],
    select: [:id, :name, :depth, :link_rewrite, :parent_id]


  def indexing_brands
    self.brands_ids = self.brand_ids.uniq
  end

  def self.full_indexing_brands
    self.transaction do
      self.select(:id).all.each {|i| i.brands_ids = i.brand_ids.uniq; i.save(validate: false)}
    end
  end

  def last_depth?
    (self.rgt - self.lft) == 1 
  end

  def main?
    self.id == MAIN
  end
  def self.main
    self.where("id = #{self::MAIN}").first
  end

  def destroy
    ids = self.product_ids
    assc = Category.reflect_on_association(:products)
    self.transaction do
      while ids.size != 0
        sql = "DELETE FROM #{assc.options[:join_table]} WHERE #{assc.foreign_key} = #{self.id}
          and #{assc.association_foreign_key} IN (#{ids.slice!(0,1000).join(',').to_s})";

        ActiveRecord::Base.connection.exec_delete(sql,nil,nil)

      end

      super
      
    end
  end

  private
  def set_defaults
    self.brands_ids = [] if !self.brands_ids.is_a?(Array)
    self.types_ids = [] if !self.types_ids.is_a?(Array)
  end
    def touch_all
      if self.name_changed? or self.link_rewrite_changed? or self.status_changed? or self.parent_id_changed? or self.lft_changed? or self.rgt_changed?
        self.class.find(self.class::MAIN).touch()  
      end
    end

    def update_status
      return false if self.main?
    end

end
