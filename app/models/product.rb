# encoding: UTF-8
# == Schema Information
#
# Table name: products
#
#  id                    :integer          not null, primary key
#  name                  :string(200)      not null
#  status                :integer          not null
#  link_rewrite          :string(200)      not null
#  meta_description      :string(400)
#  meta_keywords         :string(400)
#  short_description     :text
#  description           :text
#  weight                :integer          default(0)
#  length                :float
#  height                :float
#  width                 :float
#  price                 :float
#  type_id               :integer
#  brand_id              :integer
#  color_name            :string(200)
#  string                :string(6)
#  color_code            :string(6)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  cover_id              :integer
#  group_id              :integer
#  ratio                 :float
#  ratio_u               :integer
#  product_reviews_count :integer
#

# status 0 = Зактрыт, не видим
# status 10 = На редактировании, не видим
# status 20 = Нет в наличии, видим, к заказу не доступен
# status 30 = В наличии, к заказу готов, видим
class Product < ActiveRecord::Base

  attr_accessible :name, :status, :link_rewrite,
    :meta_description, :meta_keywords, :short_description,
    :description, :weight, :length, :height,
    :width, :price, :brand_id, :type_id, :group_id, :color_code, :color_name,
    :discount_type_id, :discount_value, :model_name
  strip_attributes :name, :link_rewrite, :meta_description, :meta_keywords, :model_name

  STATUS_LIST = {
    visible: [20, 30],
    sales: [30],
    edit: [10],
    close: [0]
  }

  DISCOUNT_TYPE = {
    1 => :d_percent,
    2 => :d_reduction,
    3 => :d_super_price
  }

  TEXT_STATUS = {
    0 => 'Закрыт',
    10 => 'На редактирование',
    20 => 'Нет в наличии',
    30 => 'В наличии'
  }


  before_validation :set_default
  after_validation :pre_discount
  after_update :reset_properties
  after_create :create_properties

  validates :name, :model_name, :link_rewrite, length: { in: 2..200 }, presence: true 

  validates :name, :model_name, format: { 
    with: /\A[A-zА-яёЁ \/,.№&0-9()\-+]+\z/}

  validates :link_rewrite, uniqueness: true, format: { 
    with: /\A[A-z_0-9\-()]+\z/}

  validates :discount_type_id, unless: "discount_type_id.blank?", inclusion: { in: DISCOUNT_TYPE.keys }
  validates :discount_value, unless: "discount_type_id.blank?", numericality: {less_than_or_equal_to: 999999}, presence: true

  validates_each :discount_value, unless: "discount_type_id.blank?" do |record, attr, value|
    if record.discount_type_id.to_i == 1 #percent
      record.errors.add(attr, 'Скидка не может быть больше 99%') if value > 99
    else
      record.errors.add(attr, 'Скидка не может быть больше цены товара') if value >= record.price
    end
  end

  validates :status, inclusion: { in: [0, 10, 20, 30] }, presence: true
  validates_each :status do |record, attr, value|
    record.errors.add(attr, 'Товар не может быть опубликован без обложки') if ([20,30].include?(value) and !record.cover)    
  end

  validates :price, numericality: {less_than_or_equal_to: 999999}, presence: true

  validates_each :brand_id, if: "brand_id_changed? or brand_id.nil?" do |record, attr, value|
    record.errors.add(attr, 'Данный бренд не существует') unless Brand.select(:id).find_by_id(value)
  end

  validates_each :type_id, if: "type_id_changed? or type_id.nil?" do |record, attr, value|
    record.errors.add(attr, 'Данный тип не существует') unless ProductsType.select(:id).find_by_id(value)
  end
  validates_each :group_id, if: "group_id? and group_id_changed?"  do |record, attr, value|
    record.errors.add(attr, 'Данная группа не существует') unless ProductsGroup.select(:id).find_by_id(value)
  end

  validates :color_name, if: "color_code?", length: { maximum: 200 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+,.]+\z/}

  validates :color_code, if: "color_name?", length: { is: 6 }, format: { 
    with: /\A[A-f0-9]+\z/}

  validates :weight, format: { with: /\A[+-]?\d+\z/}, numericality: {less_than_or_equal_to: 8000000}

  validates :length, :height, :width, numericality: {less_than_or_equal_to: 999}

  validates :meta_description, :meta_keywords , length: { maximum: 400 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+%,.]+\z|\A\z/}


  belongs_to :type, class_name: :ProductsType, inverse_of: :products
  belongs_to :brand, inverse_of: :products
  has_many :properties, class_name: :ProductProperty, inverse_of: :product, dependent: :destroy

  has_many :images, class_name: :ProductImages, dependent: :destroy, inverse_of: :product
  belongs_to :cover, class_name: :ProductImages, foreign_key: :cover_id, inverse_of: :product
  belongs_to :group, class_name: 'ProductsGroup'
  has_many :group_products, through: :group, source: :products
  has_many :items, class_name: 'ProductToSupplier', dependent: :destroy
  has_many :suppliers, through: :items
  has_many :products_in_cart, dependent: :nullify
  has_many :carts, through: :products_in_cart

  has_many :reviews, dependent: :destroy, class_name: 'ProductReview'
  has_many :article_products, dependent: :destroy
  has_many :articles, through: :article_products

  has_many :category_to_product, dependent: :destroy
  has_many :categories, through: :category_to_product,
    after_add: :brands_to_product_index,
    after_remove: :brands_to_product_index_destroy

  scope :in_stock, where(status: 30)
  scope :out_of_stock, where(status: 20)
  scope :editing, where(status: 10)
  scope :close, where( status: 0)
  scope :visible, where(status: [20, 30])
  scope :not_visible, where(status: [0, 10])
  scope :order_stock, order('products.status DESC')
  scope :only_discount, where('products.discount_type_id IS NOT NULL and products.discount_value IS NOT NULL')

  scope :az, order('`products`.`name` ASC')
  scope :za, order('`products`.`name` DESC')
  scope :cheap, order('`products`.`price` ASC')
  scope :expensive, order('`products`.`price` DESC')
  scope :news, order('`products`.`id` DESC')
  scope :discount_first, order('`products`.`sort_price` DESC')
  scope :discount_first_asc, order('`products`.`sort_price` ASC')
  scope :amount_of_discount_asc, order('(`products`.`price`-`products`.`sort_price`)')
  scope :amount_of_discount, order('(`products`.`price`-`products`.`sort_price`) DESC')

  scope :order_by_last_views, order('`products`.`last_views` DESC')

  paginates_per 30
  
  searchable do
    text :name, :meta_description, :meta_keywords, :short_description,
    :description, :model_name

    string :name

    text :type_name do
      type.name
    end

    text :brand_name do
      brand.name
    end

    integer :status

    integer :id

    integer :type_id

    integer :brand_id

    float :real_price

    time :created_at

    integer :discount_type_id

    integer :categories_ids, :multiple => true do
      category_ids
    end

    dynamic_string :properties, multiple: true do
      if status >= 20
        properties
          .joins(:meta)
          .where('products_type_properties.is_filter = true')
          .inject(Hash.new {|h,k| h[k] = []}) do |map, property|
            map[property.property_id] = (property.values.pluck(:value).delete_if { |c| c.empty? })
            map
        end
      end
    end

  end
 
  def price=(v)
    @real_price = nil
    super
  end
  def discount_type_id=(v)
    @real_price = nil
    super
  end
  def discount_value=(v)
    @real_price = nil
    super
  end

  def visible?
    STATUS_LIST[:visible].include? self.status
  end
  def sales?
    STATUS_LIST[:sales].include? self.status
  end
  def discount?
    self.discount_type_id.to_i > 0
  end

  def sum_discount
    self.price.ceil - real_price
  end
  def discount_type
    DISCOUNT_TYPE[self.discount_type_id]
  end
  def in_stock?
    self.status == 30
  end

  def real_price
    @real_price ||= self.life_real_price.ceil
  end

  def color?
    self.color_name? and self.color_code?
  end

  def stars
    return nil if self.ratio.nil? or self.ratio_u.nil?
    rating = self.ratio/self.ratio_u.to_f
    (rating*10).ceil

  end

  def text_status
    TEXT_STATUS[self.status]
  end

  def all_status
    TEXT_STATUS
  end

  def variants
    self.group_products.where("products.id <> self.id")
  end

  def self.brands(ids)
    self.where(brand_id: ids)
  end

  def self.sorting(sort)
    if sort==nil
      self.expensive
    else
      self.send(sort)
    end
  end

  def count_reviews
    self.product_reviews_count.to_i
  end

  protected

    def life_real_price
      if self.discount?
        if self.discount_type
          self.send(self.discount_type)
        else
          self.price
        end
      else
        self.price
      end
    end

    def set_default
      if self.new_record?
        self.weight ||= 0
        self.length ||= 0
        self.height ||= 0
        self.width ||= 0
      end
    end

    def reset_properties
      if self.type_id_changed?
        self.properties.clear
        self.create_properties
      end
    end

    def create_properties
      if self.type
        self.type.properties.each do |property|
          self.properties.new meta: property
        end
      end
    end

    def touch_categories
      if self.name_changed? or self.link_rewrite_changed? or self.price_changed? or self.status_changed?
        self.categories.select(:id).each do |category|
          category.touch 
        end
      end 
    end
    def brands_to_product_index(category)
      category.products_count = category.products.count
      category.products_visible = category.products.visible.count
      category.products_in_stock = category.products.in_stock.count
      if !self.type_id_changed? and !self.status_changed? and self.visible? and !(category.types_ids.to_a.include? self.type_id)
        category.types_ids.to_a.push(self.type_id)
      end 
      if !self.brand_id_changed? and !self.status_changed? and self.visible?
        self.brand.categories_count!
        self.brand.categories_list! 
        if !(category.brands_ids.to_a.include? self.brand_id)
          category.brands_ids.to_a.push(self.brand_id)
        end
      end 
      category.save(validate: false)
    end

    def brands_to_product_index_destroy(category)
      category.products_count = category.products.count
      category.products_visible = category.products.visible.count
      category.products_in_stock = category.products.in_stock.count
      if !category.products.visible.select('products.id, products.type_id').where('products.id <> ? and products.type_id = ?', self.id, self.type_id).first
        category.types_ids.to_a.delete self.type_id
      end
      if !category.products.visible.select('products.id, products.brand_id').where('products.id <> ? and products.brand_id = ?', self.id, self.brand_id).first
        category.brands_ids.to_a.delete self.brand_id
      end
      category.save(validate: false)
    end

    def pre_discount
      if DISCOUNT_TYPE.keys.include?(self.discount_type_id)
        self.sort_price = self.real_price if self.price_changed? or self.discount_value_changed? or self.discount_type_id_changed?
      else
        self.sort_price = nil
        self.discount_value = nil
        self.discount_type_id = nil
      end
    end

    def d_percent
      (self.price-( (self.price/100) * self.discount_value.to_i )).round(2)
    end

    def d_reduction
      self.price - self.discount_value
    end

    def d_super_price
      self.discount_value
    end
end