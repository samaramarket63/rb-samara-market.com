# encoding: UTF-8
# == Schema Information
#
# Table name: deliveries
#
#  id                 :integer          not null, primary key
#  price              :float
#  extra_description  :text(255)
#  description        :text
#  city_id            :integer
#  delivery_method_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  price_for_free     :float
#  note               :string(300)
#

class Delivery < ActiveRecord::Base
  attr_accessible :price, :extra_description, :description, :city_id, :delivery_method_id, :price_for_free, :note,  as: :admin

  validates :price, numericality: {less_than_or_equal_to: 999999}, presence: true

  validates :price_for_free, if: "price_for_free?", numericality: {less_than_or_equal_to: 999999}

  validates :extra_description, format: { 
    with: /\A[A-zА-яёЁ «»"&0-9(),.\-+]+\z/}, length: { maximum: 1500}

  belongs_to :city
  belongs_to :delivery_method

  has_many :delivery_payments

  has_many :payments, through: :delivery_payments

end
