# encoding: UTF-8
# == Schema Information
#
# Table name: product_images
#
#  id         :integer          not null, primary key
#  alt        :string(255)
#  image      :string(255)
#  position   :integer
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProductImages < ActiveRecord::Base

  after_create 'first_image'

  mount_uploader :image, ProductImageUploader

  acts_as_position collection: [:product, :images]

  attr_accessible :alt, :image, :position

  belongs_to :product, inverse_of: :images

  validates :image, presence: true, 
    file_size: { 
      maximum: 20.megabytes.to_i 
    }
  validates :alt, length: { maximum: 300 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+,.]+\z|\A\z/}

  def cover!
    self.product.cover = self
    self.product.save(validate: false)
    self
  end

  def cover?
    self.product and self.product.cover_id == self.id
  end
  def destroy
    super
    self.product.images.first
  end
  private 
    def first_image
      self.cover!  if self.product.cover.nil?
    end
    def set_cover
      self.cover!
    end


end
