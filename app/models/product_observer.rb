class ProductObserver < ActiveRecord::Observer

  def after_save(product)
    brands(product)
    types(product)
    status(product)
  end

  def brands(product)


    if product.brand_id_changed?
      old_brand = Brand.find_by_id(product.brand_id_was) unless product.brand_id_was.blank?
      
      product.brand.count_visible_products!
      product.brand.count_sales_products!
      product.brand.categories_count!
      product.brand.categories_list!

      product.type.products_count!
      product.type.brands_ids!
      product.type.save(validate: false)

      product.categories.select('categories.id, categories.brands_ids').each do |category|
        if !(category.brands_ids.to_a.include? product.brand_id)
          category.brands_ids.to_a.push(product.brand_id)
        end
        if !category.products.visible.select('products.id, products.brand_id').where('products.id <> ? and products.brand_id = ?', product.id, product.brand_id_was).first
          category.brands_ids.to_a.delete product.brand_id_was
        end
        category.save(validate: false)
      end

      if old_brand
        if !product.type.products.where("products.id <> #{product.id}").first
          product.type.brands_ids.delete(product.brand.id)
          product.type.products_count!
          product.type.save(validate: false)
        end
        old_brand.count_visible_products!
        old_brand.count_sales_products!
        old_brand.categories_count!
        old_brand.categories_list!
        old_brand.save!(validate: false)

      end
      product.brand.save!(validate: false)
    elsif product.status_changed? and (product.visible? or (Product::STATUS_LIST[:visible].include? product.status_was))

      if product.visible? and !(Product::STATUS_LIST[:visible].include? product.status_was)
        product.brand.categories_count!
        product.brand.categories_list! 

        product.categories.each do |category|
          if !(category.brands_ids.to_a.include? product.brand_id)
            category.brands_ids.to_a.push(product.brand_id)
            category.save(validate: false)
          end
        end  
      elsif !product.visible?
        product.brand.categories_count!
        product.brand.categories_list! 

        product.categories.each do |category|
          if !category.products.visible.select('products.id, products.brand_id').where('products.id <> ? and products.brand_id = ?', product.id, product.brand_id).first
            category.brands_ids.delete product.brand_id
            category.save(validate: false)
          end
        end
      end

      product.brand.count_sales_products!
      product.brand.count_visible_products!

      product.brand.save!(validate: false)
    end
  
  end

  def types(product)
    if product.type_id_changed?

      old_type =  ProductsType.find_by_id(product.type_id_was) unless product.type_id_was.blank?

      if old_type
        old_type.products_count!
        old_type.brands_ids!
        old_type.save(validate: false)
      end
      product.type.products_count!
      product.type.brands_ids!
      product.type.save(validate: false)
      product.categories.select('categories.id, categories.types_ids').each do |c|
        if !(c.types_ids.to_a.include? product.type_id)
          c.types_ids.to_a.push(product.type_id)
        end
        if (!c.products.visible.select('products.id, products.type_id').where('products.id <> ? and products.type_id = ?', product.id, product.type_id_was).first) and old_type
          c.types_ids.to_a.delete old_type.id
        end
        c.save(validate: false)
      end

    elsif product.status_changed? and (product.visible? and !(Product::STATUS_LIST[:visible].include? product.status_was))
      product.categories.select('categories.id, categories.types_ids').each do |c|
        if !(c.types_ids.to_a.include? product.type_id)
          c.types_ids.to_a.push(product.type_id)
          c.save(validate: false)
        end
        product.type.products_count!
        product.type.brands_ids!
        product.type.save(validate: false)
      end
    elsif  product.status_changed? and !product.visible?
      product.categories.select('categories.id, categories.types_ids').each do |c|
        if !c.products.visible.select('products.id, products.type_id').where('products.id <> ? and products.type_id = ?', product.id, product.type_id).first
          c.types_ids.to_a.delete product.type_id
          c.save(validate: false)
        end
        product.type.products_count!
        product.type.brands_ids!
        product.type.save(validate: false)
      end
    end

  end

  def status(product)
    if product.status_changed?
      product.categories.each do |c|

        c.products_count = c.products.count
        c.products_visible = c.products.visible.count
        c.products_in_stock = c.products.in_stock.count
        c.save(validate: false)
      end
    end
  end

  private

    def old_status_product(product)
        if (product.views? and !product.status_changed?)
          return product.status
        else
          return product.status_was
        end
      
    end

end
