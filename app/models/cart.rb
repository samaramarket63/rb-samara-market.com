# == Schema Information
#
# Table name: carts
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Cart < ActiveRecord::Base

  has_many :products_in_cart, dependent: :destroy

  has_many :products, through: :products_in_cart

  has_one :user

  def add(product, count = 1)
    if self.new_record?
      return -1
    end    
    if count == 0
      return true;
    end
    if count < 0
      return self.pop(product, count.abs)
    end
    unless self.include? product
      p = self.products_in_cart.build
      p.product = product
      p.count = count
      self.save
    else
      p = self.products_in_cart.where("product_id=#{product.id}").first
      p.count += count
      p.save
    end
    p
  end

  def pop(product, count = 1)
    if count == 0 or self.new_record? or !(self.include? product)
      return false;
    end
    p = self.products_in_cart.where("product_id=#{product.id}").first
    p.count -= count
    p.count <= 0 ? p.destroy : p.save
    return p
  end

  def include?(product)
    self.products.include? product
  end

  def contents
    self.products.select('
      `products_in_carts`.`id` AS `uid`,
      `products_in_carts`.`count`,
      `products`.`id`,
      `products`.`name`,
      `products`.`link_rewrite`,
      `products`.`price`,
      `products`.`discount_type_id`,
      `products`.`discount_value`,
      `products`.`status`')
  end

  def goods
    self.products.select('
      `products_in_carts`.`id` AS `uid`,
      `products_in_carts`.`count`,
      `products`.`id`,
      `products`.`name`,
      `products`.`link_rewrite`,
      `products`.`price`,
      `products`.`status`,
      `products`.`discount_type_id`,
      `products`.`discount_value`,
      `products`.`cover_id`,
      `products_in_carts`.`updated_at`')
  end  

  def sum_goods
    @sum ||= sum_calcul
  end

  def sum_goods!
    @sum = sum_calcul
  end

  def free_delivery?(delivery)
    if delivery.price_for_free
      return delivery.price_for_free <= self.sum_goods          
    end
    false
  end

  private 
    def sum_calcul
      sum = 0.00
      self.goods.each  { |x| sum += x.real_price*x.count}
      sum
    end

end
