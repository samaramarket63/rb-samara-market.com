# == Schema Information
#
# Table name: employees
#
#  id         :integer          not null, primary key
#  phone      :string(255)
#  password   :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Employee < ActiveRecord::Base

  validates :phone, uniqueness: true

  def auth?(password)
    self.class::password_digest(password) == self.password
  end

  def self.add(phone, password)
    e = self.new
    e.phone = phone
    e.password = self.password_digest(password)
    e.save
  end

  def self.password_digest(password)
     Digest::MD5.hexdigest('sadkaskjsdlaklsdfjkjdsfjkdfhndfjkjltn54'+password.to_s+'sadkaskjsdlaklsdfjkjdsfjkdfhndfjkjltn54')
  end
end
