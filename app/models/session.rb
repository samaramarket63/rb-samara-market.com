# == Schema Information
#
# Table name: sessions
#
#  id          :integer          not null, primary key
#  token       :string(32)       not null
#  user_id     :integer
#  cart_id     :integer
#  ip_hash     :string(32)       not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  remember_me :boolean          default(TRUE)
#

class Session < ActiveRecord::Base
  
  attr_accessible nil

  strip_attributes :token

  belongs_to :user
  belongs_to :cart

  validates :token, presence: true
 

  def self.get(id, token)
    s = self.find_by_id(id)
    (s and s.token == token) ? s : self.new  
  end


  def cart?
    self.cart
  end

end
