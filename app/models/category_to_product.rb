# == Schema Information
#
# Table name: category_to_products
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  category_id :integer
#

class CategoryToProduct < ActiveRecord::Base
  belongs_to :category
  belongs_to :product



  def brands_to_product_index
    if !self.product.brand_id_changed? and !self.product.status_changed? and self.product.visible?
      product.brand.categories_count!
      product.brand.categories_list! 
      if !(self.category.brands_ids.include? self.product.brand_id)
        self.category.brands_ids.to_a.push(self.product.brand_id)
        self.category.save(validate: false)
      end
    end 
  end

  def brands_to_product_index_destroy
    if !self.category.products.visible.select('id, brand_id').where('id <> ? and brand_id = ?', self.product.id, self.product.brand_id).first
      self.category.brands_ids.delete self.product.brand_id
      self.category.save(validate: false)
    end
  end


end
