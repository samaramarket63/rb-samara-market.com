# encoding: UTF-8
# == Schema Information
#
# Table name: category_advertisings
#
#  id                :integer          not null, primary key
#  name              :string(150)      not null
#  position          :integer          not null
#  description       :text
#  p_top             :integer          not null
#  p_left            :integer          not null
#  p_bottom          :integer          not null
#  p_right           :integer          not null
#  image             :string(255)
#  background        :string(255)
#  background_color  :string(6)
#  background_repeat :string(1)        not null
#  link_model        :integer          not null
#  link_id           :integer          not null
#  width_category    :integer
#  category_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class CategoryAdvertising < ActiveRecord::Base

  LINK_MODEL = {
    0 => :category,
    1 => :product,
    2 => :type,
    3 => :type_products,
    4 => :brand,
    5 => :brand_products
  } 

  BACKGROUND_REPEAT = {
    0 => 'no-repeat',
    1 => 'repeat',
    2 => 'repeat-y',
    3 => 'repeat-x'
  }

  attr_accessible :background_color, :description, :link_id,
    :link_model, :name, :p_bottom, :width_category,
  	:p_left, :p_right, :p_top, :position, :background_repeat

  acts_as_position collection: [:category, :adwords], position_max: 4

  mount_uploader :image, CategoryAdwordsUploader

  mount_uploader :background, CategoryAdwordsBackgroundUploader

  belongs_to :category, touch: true

  validates :name, length: { in: 2..150 }, format: { 
    with: /\A[A-zА-яёЁ,. &0-9()\-+]+\z/}, presence: true 

  validates :description, length: { maximum: 1000 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+,.]+\z|\A\z/}

  validates :p_bottom, :p_left, :p_right, :p_top,
    numericality: { greater_than_or_equal_to: -1000, less_than_or_equal_to: 1000, only_integer: true }

  validates :width_category, if: 'width_category?', numericality: { only_integer: true, less_than_or_equal_to: 1000 }

  validates :link_id,  numericality: { only_integer: true }

  validates :link_model, inclusion: { in: LINK_MODEL.keys, message: LINK_MODEL.to_s }

  validates :background_repeat,numericality: 
    { 
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: 3,
      only_integer: true,
      message: BACKGROUND_REPEAT.to_s
    }

  validates :background_color, length: { is: 6 }, format: { 
    with: /\A[A-f0-9]+\z/}

  def self.main
    self.where('category_id='+Category::MAIN.to_s+' and image IS NOT NULL')
  end

end
