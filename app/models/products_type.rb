# encoding: UTF-8
# == Schema Information
#
# Table name: products_types
#
#  id               :integer          not null, primary key
#  name             :string(100)      not null
#  link_rewrite     :string(100)      not null
#  meta_description :string(400)
#  meta_keywords    :string(400)
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  brands_ids       :text
#  products_count   :integer
#


class ProductsType < ActiveRecord::Base

  serialize :brands_ids

  attr_accessible :description, :link_rewrite, :meta_description,
    :meta_keywords, :name

  strip_attributes  :link_rewrite, :meta_description, :meta_keywords, :name

  before_destroy :valid_destroy
  before_create :set_defaults

  validates :name, :link_rewrite, length: { in: 2..100 }, presence: true 
  validates :name, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+]+\z/}
  validates :link_rewrite, uniqueness: true, format: { 
    with: /\A[A-z_0-9()]+\z/}
  validates :meta_description, :meta_keywords , length: { maximum: 400 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+,.]+\z|\A\z/}
  validates :description, length: { maximum: 5000 }
  
  has_many :properties, class_name: :ProductsTypeProperty, dependent: :destroy, foreign_key: :type_id 

  has_many :products, inverse_of: :type, foreign_key: :type_id, dependent: :restrict
  has_many :article_types, dependent: :destroy
  has_many :articles, through: :article_types
  has_many :brands, through: :products, uniq: true do
    def visible
      where('`products`.`status` IN (20,30)')
    end 
  end 

  def brands_ids!
    self.brands_ids = self.brand_ids 
  end

  def products_count!
    self.products_count = self.products.visible.count  
  end

  def products_count
    self.attributes['products_count'].to_i
  end

  def valid_destroy
    unless self.products.count == 0
      self.errors.add(self.name, 'Не возможно удалить, есть связанные продукты')
      return false
    end
  end
  def set_defaults
    self.brands_ids = [] if !self.brands_ids.is_a?(Array)
  end

end
