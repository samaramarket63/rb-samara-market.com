# == Schema Information
#
# Table name: products_in_carts
#
#  id                   :integer          not null, primary key
#  product_id           :integer
#  cart_id              :integer
#  count                :integer          default(0)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  sale_price           :float
#  purchase_price       :float
#  products_supplier_id :integer
#

class ProductsInCart < ActiveRecord::Base
  
  attr_accessible :sale_price, :purchase_price, :products_supplier_id, :count

  belongs_to :cart, touch: true
  belongs_to :product
  
  validates :purchase_price, numericality: {less_than_or_equal_to: 999999}, presence: true, if: 'products_supplier_id?'
  validates :count, numericality: {greater_than_or_equal_to: 0}
end
