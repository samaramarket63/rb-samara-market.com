# == Schema Information
#
# Table name: payments
#
#  id               :integer          not null, primary key
#  name             :string(32)       not null
#  description      :text
#  full_description :text
#  commission_fixed :float
#  commission_pct   :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  fields           :string(250)
#

class Payment < ActiveRecord::Base
  
  serialize :fields

  attr_accessible :name, :description, :full_description, :commission_fixed, :commission_pct, :fields, as: :admin

  has_many :delivery_payments

  has_many :delivery, through: :delivery_payments

  has_many :delivery_methods, through: :delivery

  has_many :cities, through: :delivery

end
