# encoding: UTF-8
class Article < ActiveRecord::Base
  attr_accessible :name, :link_rewrite, :meta_keywords,
    :meta_description, :title, :text, :intro

  has_many :article_brands
  has_many :brands, through: :article_brands, uniq: true
  has_many :article_types
  has_many :types, through: :article_types, uniq: true
  has_many :article_products
  has_many :products, through: :article_products, uniq: true
  has_many :images, class_name: 'ArticleImage'

  validates :name, :link_rewrite, length: { in: 2..200 }, presence: true 

  validates :name, format: { 
    with: /\A[A-zА-яёЁ \/,.№&0-9()\-+]+\z/}

  validates :link_rewrite, uniqueness: true, format: { 
    with: /\A[A-z_0-9\-()]+\z/}

  validates :meta_description, :meta_keywords , length: { maximum: 400 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+,.]+\z|\A\z/}


  before_save :compile_text


  def compile_text
    if self.text_changed?
      unless self.new_record? 
        self.article_brands.clear
        self.article_types.clear
        self.article_products.clear 
      end

      self.text_compile = self.text.gsub(/\/%[\s\S]+?%\//) do |math|
        params = {
          resource: nil,
          ids: {},
          separator: ','
        }
        math.gsub(/\/%|%\//,'').split(';').each do |tag|
          d = tag.scan(/[a-z]+|\[[\S\s]+\]/)
          d[1] = d[1].to_s.gsub(/\[|\]/,'').strip
          d[0].strip!
          if d[0] == 'link'
            params[:resource] = d[1]
          elsif d[0] == 'ids'
            d[1].split(',').each{|x| dat = x.split(':'); params[:ids][dat[0].to_i] = dat[1]}
          elsif d[0] == 'separator'
            params[:separator] = d[1]
          end
        end
        links = []
        if params[:resource] == 'product'
          Product.where(id: params[:ids].keys).select('id, name, link_rewrite').each do |res|
            self.products << res
            links.push ActionController::Base.helpers.link_to((params[:ids][res.id].blank? ? res.name : params[:ids][res.id]),  Rails.application.routes.url_helpers.product_path(res.link_rewrite, :html), title: res.name) 
          end
        elsif params[:resource] == 'type' 
          ProductsType.where(id: params[:ids].keys).select('id, name, link_rewrite').each do |res|
            self.types << res
            links.push ActionController::Base.helpers.link_to((params[:ids][res.id].blank? ? res.name : params[:ids][res.id]),  Rails.application.routes.url_helpers.type_path(res.link_rewrite, :html), title: res.name)
          end
        elsif params[:resource] == 'brand'
          Brand.where(id: params[:ids].keys).select('id, name, link_rewrite').each do |res|
            self.brands << res
            links.push ActionController::Base.helpers.link_to((params[:ids][res.id].blank? ? res.name : params[:ids][res.id]),  Rails.application.routes.url_helpers.brand_path(res.link_rewrite, :html), title: res.name)
          end
        end
        links.join(params[:separator])
      end
    end

  end

end
#a.gsub(/\/%[\s\S]+?%\//) {|x| x.gsub(/\/%|%\//,'').split(';').each{|p| p.scan(/[a-z]+|\[[\S]+\]/) {|c| puts c}}}