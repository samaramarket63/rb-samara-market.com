class OrderStatusHistory < ActiveRecord::Base
  attr_accessible :status
  belongs_to :order
  default_scope order('id DESC')
end
