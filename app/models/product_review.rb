# encoding: UTF-8
# == Schema Information
#
# Table name: product_reviews
#
#  id          :integer          not null, primary key
#  stars       :integer
#  title       :string(255)
#  value       :text
#  limitations :text
#  comment     :text
#  vote_up     :integer
#  vote_down   :integer
#  user_name   :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  period      :integer
#  moderated   :boolean
#  user_id     :integer
#  product_id  :integer
#

class ProductReview < ActiveRecord::Base
  include ActionView::Helpers::SanitizeHelper

  strip_attributes :title, :user_name, :comment, :value, :limitations

  before_save :sanitize_fields
  before_validation :set_default_vote

  attr_accessible :title, :user_name, :value, :limitations, :comment, :stars, :period

  validates :title, length: { maximum: 70 },
    format: { 
      with: /\A[A-zА-яёЁ &0-9()\-+,.#№!?"'\\\/;]+\z|\A\z/,
      message: 'Только символы русского и латинского алфавита, числовые, знаки припенания и (),№,#,/'
    }, presence: true 

  validates :user_name,
    format: { 
      with: /\A[А-яёЁ'\-]+\z|\A\z/,
      message: 'может содержать только буквы русского алфавита, дефис и знак апострофа'
    },
    length: {
      maximum: 32,
      too_long: 'максимальное количество символов - 32'
    }, presence: true 

  validates :value, :limitations, :comment, length: { maximum: 3000 }

  validates :comment, presence: true 

  validates :vote_up, :vote_down, numericality: {
      only_integer: true
    }

  validates :stars, if: 'stars?', numericality: {
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 5
  }
  validates :stars, presence: true

  validates :period, if: 'period?', numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 4
  }
  validates :period, presence: true

  belongs_to :product, counter_cache: true
  belongs_to :user

  has_many :votes, class_name: 'ProductReviewVote', foreign_key: :review_id, dependent: :destroy

  validates :user_id, uniqueness:{scope: :product_id}, presence: true

  before_create :set_default

  PERIOD_LIST = {
    0 => 'не известно',
    1 => 'менее месяца',
    2 => 'меньше года',
    3 => 'более года'

  }

  def vote_up!
    self.vote_up = self.vote_up + 1
  end

  def vote_down!
    self.vote_down = self.vote_down + 1 
  end

  private
    def sanitize_fields
      self.value = TextToHtml.simple_format(sanitize(self.value)) if self.value_changed?
      self.limitations = TextToHtml.simple_format(sanitize(self.limitations)) if self.limitations_changed?
      self.comment = TextToHtml.simple_format(sanitize(self.comment)) if self.comment_changed?
    end

    def set_default
      self.moderated = false
      self.vote_up = 0
      self.vote_down = 0
    end

    def set_default_vote
      self.vote_up ||= 0
      self.vote_down ||= 0
    end
end
