# encoding: UTF-8
# == Schema Information
#
# Table name: product_to_suppliers
#
#  id          :integer          not null, primary key
#  uid         :string(400)
#  price       :float
#  product_id  :integer
#  supplier_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ProductToSupplier < ActiveRecord::Base
  attr_accessible :uid, :price, :product_id, :supplier_id

  validates :product_id, :supplier_id, :price, presence: true 

  validates :uid, length: { maximum: 200 }, presence: true 
    
  validates :price, numericality: { less_than_or_equal_to: 999999 }

  validates_each :product_id, if: " product_id_changed?"  do |record, attr, value|
    record.errors.add(attr, 'Продукт не существует') unless Product.select(:id).find_by_id(value)
  end

    validates_each :supplier_id, if: "supplier_id_changed?"  do |record, attr, value|
    record.errors.add(attr, 'Поставщик не существует') unless ProductsSupplier.select(:id).find_by_id(value)
  end

  belongs_to :product
  belongs_to :supplier, class_name: 'ProductsSupplier'

end
