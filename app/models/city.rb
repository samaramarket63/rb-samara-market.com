# encoding: UTF-8
# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(100)      not null
#  position   :integer
#  region_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :boolean          not null
#

class City < ActiveRecord::Base
  attr_accessible :name, :status, :region_id
  
  acts_as_position collection: [:region, :cities]

  validates :name, format: { 
    with: /\A[А-яёЁ,. 0-9()\-+]+\z|\A\z/}, presence: true, length: { maximum: 100 }

  belongs_to :region

  has_many :delivery, dependent: :destroy

  has_many :delivery_method, through: :delivery

  has_many :payments, through: :delivery_method

  scope :public, where(status: true)
  
end
