class ArticleImage < ActiveRecord::Base
  attr_accessible :image
  mount_uploader :image, ArticleImageUploader
  belongs_to :article

  validates :image, 
    file_size: { 
      maximum: 10.megabytes.to_i 
    }
end
