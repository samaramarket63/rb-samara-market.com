# encoding: UTF-8
# == Schema Information
#
# Table name: products_type_properties
#
#  id          :integer          not null, primary key
#  name        :string(100)      not null
#  description :text
#  unit        :string(10)
#  position    :integer          default(0)
#  type_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ProductsTypeProperty < ActiveRecord::Base

  attr_accessible :description, :name, :unit, :is_filter
  acts_as_position collection: [:type, :properties]

  strip_attributes :name, :unit

  after_create 'update_product'

  after_save 'index_products'

  validates :name, format: { 
    with: /\A[A-zА-яёЁ &0-9()\/,.*#\-+]+\z/}, length: { in: 2..100 }, presence: true 

  validates :unit, length: { maximum: 10 }

  validates :description, length: { maximum: 2000 }
  
  belongs_to :type, class_name: :ProductsType, inverse_of: :properties

  has_many :product_property, foreign_key: :property_id, dependent: :destroy

  has_many :products, through: :product_property

  has_many :values, through: :product_property

  private
    def update_product
      data = self.type.products.select(:id).pluck(:id).collect{ |id| {product_id:id} }
      self.product_property.create data
    end

    def index_products
      Sunspot.index(Product.where(type_id: self.type_id)) if self.is_filter_changed?
    end
  
end
