class ArticleType < ActiveRecord::Base
  belongs_to :article
  belongs_to :type, class_name: 'ProductsType'
end
