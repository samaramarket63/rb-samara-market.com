# encoding: UTF-8
# == Schema Information
#
# Table name: regions
#
#  id         :integer          not null, primary key
#  name       :string(100)      not null
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :boolean          not null
#

class Region < ActiveRecord::Base
  attr_accessible :name, :status

  acts_as_position

  validates :name, format: { 
    with: /\A[А-яёЁ,. 0-9()\-+]+\z|\A\z/}, presence: true, length: { maximum: 100 } 

  has_many :cities


  scope :public, where(status: true)

end
