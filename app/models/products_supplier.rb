# encoding: UTF-8
# == Schema Information
#
# Table name: products_suppliers
#
#  id         :integer          not null, primary key
#  name       :string(200)      not null
#  address    :text
#  site       :string(200)
#  note       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProductsSupplier < ActiveRecord::Base

  attr_accessible :address, :name, :note, :site

  strip_attributes :address, :name, :note, :site

  validates :name, format: {with: /\A[A-zА-яёЁ &0-9()-+,.]+\z/}, 
    length: { in: 1..200 },
    presence: true

  validates :site, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+:;,.]+\z|\A\z/}, 
    length: { maximum: 200 }

  validates :address, :note, length: { maximum: 2000 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+,.]+\z|\A\z/}

  has_many :items, class_name: 'ProductToSupplier', foreign_key: 'supplier_id', dependent: :destroy

  has_many :products, through: :items



end
