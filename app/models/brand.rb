# encoding: UTF-8
# == Schema Information
#
# Table name: brands
#
#  id                     :integer          not null, primary key
#  name                   :string(100)      not null
#  link_rewrite           :string(100)      not null
#  short_description      :string(600)
#  meta_description       :string(400)
#  meta_keywords          :string(400)
#  description            :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  logo                   :string(255)
#  count_visible_products :integer
#  count_sales_products   :integer
#  categories_list        :text
#  categories_count       :integer
#

class Brand < ActiveRecord::Base
  attr_accessible :description, :link_rewrite, :meta_description,
    :meta_keywords, :name, :short_description

  serialize :categories_list

  has_many :products, dependent: :restrict

  has_many :categories, through: :products
  has_many :article_brands, dependent: :destroy
  has_many :articles, through: :article_brands

  has_many :types, through: :products, uniq: true do
    def visible
     where('`products`.`status` IN (20,30)')
    end 
  end 
  mount_uploader :logo, BrandLogoUploader

  strip_attributes :link_rewrite, :meta_description, :meta_keywords, :name

  validates :name, :link_rewrite, length: { in: 2..100 }, presence: true 
  validates :meta_keywords, :meta_description, length:  { maximum: 400 }
  validates :name, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+]+\z/}
  validates :link_rewrite, uniqueness: true, format: { 
    with: /\A[A-z_0-9()]+\z/}
  validates :short_description, length: { maximum: 600 }
  validates :description, length: { maximum: 5000 }

  validates :logo, 
    file_size: { 
      :maximum => 5.megabytes.to_i 
    }


  def count_visible_products!
    self.count_visible_products = self.products.visible.reset.count
  end

  def count_sales_products!
    self.count_sales_products = self.products.in_stock.reset.count
  end

  def categories_count!
    self.categories_count = self.categories.public.where(products:{status:[20,30]}).select('categories.id').uniq.reset.to_a.count
  end

  def categories_list!
    self.categories_list = self.categories.public.where(products:{status:[20,30]}).select('categories.id').uniq.reset.map! {|x| x.id}
  end

end
