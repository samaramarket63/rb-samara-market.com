# == Schema Information
#
# Table name: ip_bans
#
#  id         :integer          not null, primary key
#  ip         :string(32)
#  operation  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'ipaddr'

class IpBan < ActiveRecord::Base
  attr_accessible :ip, :operation

  LOGIN = 0
  RECOVERY_PASSWORD = 1

  def self.add(ip, operation)
    self.create(ip: IPAddr.new(ip).to_i, operation: operation)
  end

  def self.get(ip, operation, limit=5, time=10)
    ip = IPAddr.new(ip).to_i
    count  = self.where("ip=#{ip} and operation=#{operation} and created_at > '#{time.minutes.ago.to_formatted_s(:db)}'").count 
    {valid?: (count < limit), attempts: count, next_valid?:  ((count+1) < limit) }
  end
end
