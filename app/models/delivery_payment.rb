# == Schema Information
#
# Table name: delivery_payments
#
#  id          :integer          not null, primary key
#  delivery_id :integer
#  payment_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class DeliveryPayment < ActiveRecord::Base
  attr_accessible :delivery_id, :payment_id, as: :admin
  belongs_to :payment
  belongs_to :delivery
end
