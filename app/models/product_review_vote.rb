# == Schema Information
#
# Table name: product_review_votes
#
#  id         :integer          not null, primary key
#  review_id  :integer
#  user_id    :integer
#  vote       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProductReviewVote < ActiveRecord::Base
  
  attr_accessible :user_id, :vote

  belongs_to :review, class_name: 'ProductReview'
  belongs_to :user

end
