# encoding: UTF-8
# == Schema Information
#
# Table name: products_groups
#
#  id         :integer          not null, primary key
#  name       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProductsGroup < ActiveRecord::Base
  attr_accessible :name
  has_many :products, dependent: :nullify, foreign_key: :group_id

  validates :name , length: { maximum: 300 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+\/,.*%]+\z/}, presence: true 
end
