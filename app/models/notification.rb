class Notification
  
  def initialize
    @messages = []
  end

  def get
    @messages
  end

  def add(type, text, title=nil)
    @messages.push({ type: type, text: text, title:title })
  end

  def clear
    @messages.clear  
  end

  def empty?
    @messages.empty?
  end



end