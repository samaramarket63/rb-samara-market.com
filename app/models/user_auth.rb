# encoding: UTF-8
# == Schema Information
#
# Table name: user_auths
#
#  id         :integer          not null, primary key
#  provider   :integer          not null
#  uid        :string(100)      not null
#  password   :string(32)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserAuth < ActiveRecord::Base
  attr_accessible :provider, :uid, :password
  attr_accessible :provider, :uid, :password, :user_id, as: :admin

  attr_accessor :password_hash_skip 

  strip_attributes :password
  PROVIDERS = {
    sm:        0,
    vkontakte: 1,
    facebook:  2,
    google_oauth2:    3,
    yandex:    4,
    mailru:    5,
    twitter:   6
  }

  before_save 'pasword_hash', unless: 'password_hash_skip'

  belongs_to :user
  
  validates :user_id,  presence: true

  validates :uid, uniqueness:{scope: :provider}, presence: true

  validates :password, length: { minimum: 5}, if: "provider==0"


  def self.auth_by(uid, provider, password=nil)
    return nil if !PROVIDERS[provider] or uid.blank?
    provider = PROVIDERS[provider]
    user = self.where(uid: uid, provider: provider).first
    return nil if user.nil?
    case provider
      when PROVIDERS[:sm]
        if user.password == UserAuth::pasword_hash(password)
          return user
        else
          return nil
        end
      else
        return user
    end
    return nil
    
    
  end

  def self.pasword_hash(password)
    Digest::MD5.hexdigest(Sm::Application.config.salt+password.to_s)
  end

  private 
    def pasword_hash
      if self.password_changed?
        self.password = UserAuth::pasword_hash(self.password)
      end
    end



end
