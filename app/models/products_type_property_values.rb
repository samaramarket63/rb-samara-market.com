# encoding: UTF-8
# == Schema Information
#
# Table name: products_type_property_values
#
#  id         :integer          not null, primary key
#  value      :string(300)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProductsTypePropertyValues < ActiveRecord::Base
  attr_accessible :value
  strip_attributes :value
  has_and_belongs_to_many :properties, class_name: :ProductProperty,
    join_table: 'join_product_properties_of_values',
    foreign_key: :value_id,
    association_foreign_key: :property_id
  has_many :products, through: :properties

 #validates :value , length: { maximum: 300 }, format: { 
 #   with: /\A[A-zА-яёЁ &0-9()\-+\/,.*%]+\z/}, presence: true 
  validates :value , length: { maximum: 300 }, presence: true 
end
