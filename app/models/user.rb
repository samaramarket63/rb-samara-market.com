# encoding: UTF-8
# == Schema Information
#
# Table name: users
#
#  id             :integer          not null, primary key
#  first_name     :string(32)
#  last_name      :string(32)
#  middle_name    :string(32)
#  email          :string(64)
#  email_verified :boolean
#  phone          :string(10)
#  phone_verified :boolean
#  cart_id        :integer
#  note           :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class User < ActiveRecord::Base
  attr_accessible :email, :first_name, :last_name, :middle_name, :phone, as: :user

  attr_accessible :email, :first_name, :last_name, :middle_name, :note, :phone, :phone_verified, :email_verified, as: :admin

  strip_attributes :email, :email_verified, :first_name, :last_name, :middle_name, :phone, :phone_verified

  validates :first_name, :last_name, :middle_name,
    format: { 
      with: /\A[А-яёЁ'\-]+\z|\A\z/,
      message: 'может содержать только буквы русского алфавита, дефис и знак апострофа'
    },
    length: {
      maximum: 32,
      too_long: 'максимальное количество символов - 32'
    }

  validates :note, length: { maximum: 400 }, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+,.]+\z|\A\z/}

  validates :email, if: 'email?',
    format: {
      with: /\A([^@\\\/\s]+)@((?:[-a-z0-9]+\.)+[a-z]{1,})\z/i,
      message: 'не подходит под формат, обратитесь в тех.поддержку'
    },
    length: {
      maximum: 64,
    }

  validates :phone,
    length: {
      is: 10,
      message: 'телефон только мобильный, 10 цифер без кода страны'
    },
    numericality: {
      only_integer: true
    }, if: 'phone?'

  has_many :addresses, class_name: 'UserAddress', dependent: :destroy

  has_many :auths, class_name: 'UserAuth', dependent: :destroy

  has_many :orders

  has_many :verifieds, class_name: 'UserDataVerified'

  belongs_to :cart

  before_update :verified
  before_update :auth_login

  def full_name
    self.last_name.to_s+' '+self.first_name.to_s+' '+self.middle_name.to_s
  end


  def verified
    if self.email_changed?
      self.email_verified = false 
      self.verifieds.where(field: self.verifieds.klass::FIELDS[:email]).update_all(exp: true)
    end
    if self.phone_changed?
      self.phone_verified = false
      self.verifieds.where(field: self.verifieds.klass::FIELDS[:phone]).update_all(exp: true)
    end  
    self
  end
  def auth_login
    if self.phone_changed?
      auth = self.auths.where(provider: self.auths.klass::PROVIDERS[:sm]).first
      if auth
        auth.uid = self.phone
        if auth.save
          return true
        else
          self.valid?
          auth.errors.each {|attribute, errors_array| self.errors.add attribute, errors_array  }
          return false
        end 
      end
    end
    true
  end
end
