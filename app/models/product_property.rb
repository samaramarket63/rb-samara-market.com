# == Schema Information
#
# Table name: product_properties
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  property_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ProductProperty < ActiveRecord::Base
  before_destroy :gb

  attr_accessible :meta, :product_id

  belongs_to :product, inverse_of: :properties
  has_and_belongs_to_many :values, class_name: :ProductsTypePropertyValues,
    join_table: 'join_product_properties_of_values',
    foreign_key: :property_id,
    association_foreign_key: :value_id
  belongs_to :meta, class_name: :ProductsTypeProperty, foreign_key: :property_id
  private 
    def gb
      self.values.each do |value|
        value.destroy if value.properties.count == 1
      end
    end 
end
