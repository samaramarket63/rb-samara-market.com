# == Schema Information
#
# Table name: user_data_verifieds
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  token      :string(32)
#  attempts   :integer
#  field      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  exp        :boolean
#

class UserDataVerified < ActiveRecord::Base

  FIELDS = {
    email: 0,
    phone: 1
  }

  BANS = {
    email: 5,
    phone: 3
  }

  before_create :generate_token

  after_save :send_msg

  belongs_to :user

  def generate_token
    self.exp = false
    if self.field == FIELDS[:phone]
      self.token = (0..4).map{ (0..9).to_a[rand(9)] }.join
    else
      self.token = (0..31).map{ ('a'..'z').to_a[rand(26)] }.join
    end
  end

  def compare_token(token)
    if self.token == token 
      self.exp = true
      self.user[FIELDS.key(self.field).to_s+'_verified'] = true
      self.user.save!
    else
      false
    end
  end

  private
    def send_msg
      case self.field
        when FIELDS[:phone]
          Sms.send(
            '7'<<self.user.phone,
            I18n.t('phone.user.verified_phone', token: self.token)
          )
        when FIELDS[:email]
          UserMailer.email_verified(self).deliver
        else
          raise ActiveRecord::RecordNotSaved 
      end
    end

end
