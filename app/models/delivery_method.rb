# encoding: UTF-8
# == Schema Information
#
# Table name: delivery_methods
#
#  id               :integer          not null, primary key
#  name             :string(32)       not null
#  description      :text
#  full_description :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  fields           :string(250)
#

class DeliveryMethod < ActiveRecord::Base
  
  serialize :fields

  attr_accessible :name, :description, :full_description, :fields, as: :admin

  validates :name, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+]+\z/}, presence: true, length: { maximum: 32 }


  validates :description, format: { 
    with: /\A[A-zА-яёЁ &0-9()\-+]+\z|\A\z/}, length: { maximum: 1500 }

  validates :full_description,  length: { maximum: 5000 }

  has_many :delivery

  has_many :city, through: :delivery

  has_many :payments , through: :delivery

end
