SmAdmin.Helpers.Categories =

  reloadSelect: (list=false, selected)->

    if !list 
      list = $('form select[data-model="category"]')

    if kList = list.data("kendoComboBox")
      selected = kList.value()

    collection = SmAdmin.Helpers.Collection
      .toArray(window.Collection.categories,['id', 'name', 'depth'])
    collection.unshift(
        depth: 0
        id: ''
        name: "Главная"
      )


    list.width(380).kendoComboBox({
      dataTextField: "name",
      dataValueField: "id",
      highLightFirst: true
      template: '<span data-model-id="${ data.id }"></span>
        # var backspaces = ""#
        # if (data.depth>0) {#
        # for(var i = 0; i < data.depth; i++){#
        #= "&mdash;"#
        #}#
        #}#
        #= backspaces+data.name #'
      dataSource: collection
    })

    kList = list.data("kendoComboBox")
    kList.select(selected)
    kList

  view_tree: (container)->
    categories = window.Collection.categories
    container
      .kendoTreeView(
        dragAndDrop: true
        dataSource: SmAdmin.Helpers.Collection.toTree(categories)
        dataTextField: 'name'
        children: "children"
        template: '<a href="\\#categories/${item.id}/products">${item.name}</a> \\#${item.id}
          <span class="toolbar data" data-model-id="${item.id}">
            <a href="\\#categories/${item.id}/edit"><i class="icon-edit"></i></a>
            <a href="\\#categories/${item.id}/status" class="status"><i class="k-icon status-${item.status}"></i></a>
            <a href="\\#categories/${item.id}/delete"><i class="icon-trash"></i></a>
          </span>'
      );
    tree = container.data("kendoTreeView")
    tree.bind('dragend', (e)->
      model = categories.get($(e.sourceNode).find('span.data').data('model-id'))
      neighbor = categories.get($(e.destinationNode).find('span.data').data('model-id'))
      position = e.dropPosition
      if position == 'over'
        model.save({
          parent_id:neighbor.get('id')
        }) 
      else
        data =
          position: position
          neighbor: neighbor.get('id')
        data[window.csrf.name] = window.csrf.value
        $.ajax({
          url: window.base_url+'/categories/'+model.id+'/position/'
          type: 'PUT'
          data: data
        })
    )
  view_form_tree: (container)->
    categories = window.Collection.categories
    container.kendoTreeView(
      dataSource: SmAdmin.Helpers.Collection.toTree(categories)
      dataTextField: 'name'
      children: "children"
      template: '<input type="checkbox" name="categories[]" value="${item.id}"> ${item.name} \\#${item.id}'
    );
    
    tree = container.data("kendoTreeView")