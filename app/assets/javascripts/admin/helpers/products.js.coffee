SmAdmin.Helpers.Products =
  showProducts: (collection)->
    for value, key in collection
      collection[key]['type_name'] = window.Collection.ProductsTypes.get(value.type_id).get('name')
      collection[key]['brand_name'] = window.Collection.Brands.get(value.brand_id).get('name')

    container = $('#product_list')

    unless ($('#product_list')).length
      views = new SmAdmin.Views.Catalog()
      $('#container').html(views.renderIndex().el)
      SmAdmin.Helpers.Categories.view_tree($('#a_categories_list .categories_tree'))
      container = $('#product_list')
    if (container.data("kendoGrid"))?
      container.before('<div id="product_list"></div>').remove()
      container = $('#product_list') 


    container.kendoGrid({
      dataSource: 
        data: collection
        pageSize: 20
      selectable: "multiple"
      resizable: true
      pageable: true
      sortable:
        mode: "multiple"
      columns: [ { title: "ID", field: "id", type: "number", width: 50 },
        { title: "Наименование", field: "name", width: 250, filterable: true},
        { title: "Статус", field: "SmAdmin.Helpers.Products.status_to_s(status)", width: 70, filterable: true},
        { title: "Тип", field: "type_name", width: 180 },
        { title: "Бренд", field: "brand_name", width: 120 },
        { title: "Цена", field: "real_price", width: 90, type: "number", format: "{0:c}" },
        { command: [{ text: "delete", click: SmAdmin.Helpers.Products.destroy }, { text: "edit", click: SmAdmin.Helpers.Products.edit }], title: "&nbsp;", width: "210px" }]
      filterable:
        name: "FilterMenu"
        extra: true 
        messages:
          info: "Фильтр:"
          filter: "фильтр"
          clear: "очистить"
          isTrue: "custom is true"
          isFalse: "custom is false"
          and: "и"
          or: "или"
        operators:
          string:
            eq: "совпадает"
            neq: "не совпадает"
            startswith: "начинается с..."
            contains: "содержит"
            endswith: "заканчивается на..."
          number:
            eq: "равно"
            neq: "не равно"
            gte: "больше или равно"
            gt: "больше"
            lte: "меньше или равно"
            lt: "меньше"
          date:
            eq: "равно",
            neq: "не равно",
            gte: "после и равно"
            gt: "после"
            lte: "до и равно"
            lt: "до"  
    }) 
  edit: (e)->
    e.preventDefault();
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));
   
    location.hash='products/'+dataItem.id+'/edit/'+dataItem.uid

  destroy: (e)->
    e.preventDefault();
    dataItem = this.dataItem($(e.currentTarget).closest("tr"));

    location.hash='products/'+dataItem.id+'/destroy/'+dataItem.uid

  show: (collection,id)->
    url = collection.url+'/'+id+'/products'
    window.onLoad()
    $.get(url)
      .success( (data, textStatus, jqXHR)->
        window.offLoad()
        SmAdmin.Helpers.Products.showProducts(data)
      )
      .error( (data, textStatus, jqXHR)->
        alert(jqXHR)
        window.offLoad()
      )
  status_to_s: (status)->
    switch status
      when 0 then status='закрыто'
      when 10 then status='на редактирование'
      when 20 then status='нет в наличии'
      when 30 then status='доступно'
      else status='не определенно'
    status


window.number_format = ( number, separator=" ")->
    number = parseFloat(number)
    (number+'').replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);