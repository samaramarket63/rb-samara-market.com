SmAdmin.Helpers.editor = (win)->
  win.find('textarea.editor, input.editor').kendoEditor(
    encoded: false
    tools: [
      "bold",
      "italic",
      "underline",
      "insertUnorderedList",
      "insertOrderedList",
      "formatBlock",
      "viewHtml"
    ]
  )
