SmAdmin.Helpers.Collection =
  toArray: (collection,fields)->
    arr = []
    collection.map((model)->
      obj = {}
      fields.map((field)->
        obj[field] = model.get(field)
      )
      arr.push(obj)
    )
    arr
    
  toTree: (collection, parent_field='parent_id')->

    condition = {}

    spider = (parent_id)->
      arr=[]
      condition[parent_field] = parent_id
      data = collection.where(condition)
      data.map((model)->
        model = model.toJSON()
        model['items'] = spider(model.id)
        arr.push(model)
      )
      arr
    spider(null)
