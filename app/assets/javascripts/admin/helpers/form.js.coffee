SmAdmin.Helpers.Form =
  success: (model, response, form)-> 
    this.removeErrors(form)
    if form.find('input[name="id"]').size() == 0 
      form.append('<input type="hidden" name="id" value="'+model.get('id')+'">')
   
    form.prepend('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a><strong>Сохранено</strong></div>')
    form.find(".alert").alert()

  error: (response, form)-> 
    this.removeErrors(form)

    html = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">&times;</a>'
    if response.status == 422
      errors = $.parseJSON(response.responseText)
      html += '<ul>'
      for error of errors
        form
          .find('input[name="'+error+'"]')
          .parents('.control-group:first')
          .addClass('error')
        html += '<li>Поле <strong>'+error+'</strong><ul>' 
        for msg in errors[error]
          html += '<li>'+msg+'</li>' 
        html += '</ul></li>'  
      html += '</ul>'
    else
      html += '<p> Ошибка сервиса №'+response.status+' - '+response.statusText+'</p>'
      html += '</div>'
    form.prepend(html)
    form.find(".alert").alert()

  removeErrors: (form)->
    form.find('.alert').remove()
    form.find('.error').removeClass('error')

  openWindow: (form, title, model=null, width=700)->
    form = form
      .get$(model)
      .appendTo('body')
      .kendoWindow(
        width: width+"px",
        title: title,
        actions: ["Refresh", "Minimize", "Maximize", "Close"],
        close: ->
          form.data("kendoWindow").destroy() 

      )

    form.find('button.translit').click(->
      window.Translit(form.find('input[name="name"]'), form.find('input[name="link_rewrite"]'))
    )
    SmAdmin.Helpers.editor(form)
    form.find('.tabstrip').kendoTabStrip()
    wind = form.data("kendoWindow")
    href = window.location.hash
    wind.refresh = ()->
      window.location.hash = href
      wind.close()
    wind.center()
    window.location.hash = "#"
    form

  combox: (collection, select, key='id', value='name')->
    #if kSelect = select.data("kendoComboBox")

    select.kendoComboBox({
        dataTextField: value
        dataValueField: key
        highLightFirst: true
        template: '<span data-model-id="${ data.id }"></span>${data.name}'
        dataSource: collection
    })
    select.data('kendoComboBox')

  sort_position: (list, url)->
    list.sortable(
      stop: (e,node)->
        if node.item.next().length
          data = 
            move: 'before'
            move_id: node.item.next().data('id')
        else
          if node.item.prev().length
            data = 
              move: 'after'
              move_id: node.item.prev().data('id')           
          else
            return false
        
        data[window.csrf.name] = window.csrf.value
        $.post(window.base_url+url+'/'+node.item.data('id')+'/position', data
        ).error ->
          alert('Ошибка изменения позиции')
        )
