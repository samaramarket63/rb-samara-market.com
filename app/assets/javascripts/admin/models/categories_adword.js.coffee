class SmAdmin.Models.CategoriesAdword extends Backbone.Model

  url: ->
    id = ''
    id = @attributes.id if @attributes.id
    window.model_url+'/categories/'+@attributes.category_id+'/adwords/'+id
  
  secureAttributes: [
    'created_at', 'updated_at', 'background', 'category_id', 'id', 'image' 
  ]

  toJSON: ->
    @_cloneAttributes();

  _cloneAttributes: ->
    attributes = _.clone(@attributes)
    for sa in @secureAttributes
      delete attributes[sa]
    _.clone(attributes)