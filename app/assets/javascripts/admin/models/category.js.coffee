class SmAdmin.Models.Category extends Backbone.Model

  initialize: -> 
    @attributes.adwords = (attr = false)->
      window.Collection.CategoriesAdwords.where({category_id:@id})


  toggleStatus: (options)->
    if  this.attributes.status == false
      status = true
    else
      status = false
    this.attributes.status = status
    this.save({status:status}, options)
    this