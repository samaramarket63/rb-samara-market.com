var ru2en = {
  ru_str : 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя,.; -"',
  en_str : ['a','b','v','g','d','e','e','zh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','ts','ch','sh','shh','','y','','e','yu','ya',
'a','b','v','g','d','e','e','zh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','ts','ch','sh','shh','','y','','e','yu','ya','_','_','_','_','_','_'],
  translit : function(org_str) {
    var tmp_str = "";
    var pattern = /^[a-zA-Z0-9№_]+$/;

    for(var i = 0, l = org_str.length; i < l; i++) {
      var s = org_str.charAt(i), n = this.ru_str.indexOf(s);
      if(n >= 0) { 
        tmp_str += this.en_str[n]; 
      }
      else { 
          if(pattern.test(s)) {
              tmp_str += s;
          }
      }
    }
    return tmp_str.toLowerCase();
  }
}

window.Translit = function(source, dest) {

  var name = $(source).val();

  if (name != undefined) {
    var name = $(source).val();

    $(dest).val(ru2en.translit(name));

  }
}