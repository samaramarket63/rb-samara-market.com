class SmAdmin.Collections.Articles extends Backbone.Collection

  model: SmAdmin.Models.Article

  url: window.model_url+'/articles'