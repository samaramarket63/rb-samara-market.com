class SmAdmin.Collections.Categories extends Backbone.Collection

  model: SmAdmin.Models.Category

  url: window.model_url+'/categories'

