class SmAdmin.Collections.Products extends Backbone.Collection

  model: SmAdmin.Models.Product

  url: window.model_url+'/products'