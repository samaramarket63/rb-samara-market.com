class SmAdmin.Collections.CategoriesAdwords extends Backbone.Collection

  model: SmAdmin.Models.CategoriesAdword

  url: window.model_url+'/adwords'

  comparator: (model)->
    model.get('position')