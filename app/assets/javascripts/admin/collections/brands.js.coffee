class SmAdmin.Collections.Brands extends Backbone.Collection

  model: SmAdmin.Models.Brand

  url: window.model_url+'/brands'