class SmAdmin.Collections.ProductsGroups extends Backbone.Collection

  model: SmAdmin.Models.ProductsGroup

  url: window.model_url+'/products_groups'