class SmAdmin.Collections.ProductsTypes extends Backbone.Collection

  model: SmAdmin.Models.ProductsType

  url: window.model_url+'/products_types'