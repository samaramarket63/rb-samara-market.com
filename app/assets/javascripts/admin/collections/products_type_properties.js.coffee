class SmAdmin.Collections.ProductsTypeProperties extends Backbone.Collection

  model: SmAdmin.Models.ProductsTypeProperty
  
  initialize: (models, options) ->
    @type_id = options.type_id

  url: (type_id)->

    window.model_url+'/products_types/'+@type_id+'/type_property'