class SmAdmin.Collections.Orders extends Backbone.Collection

  model: SmAdmin.Models.Order

  url: window.model_url+'/orders'