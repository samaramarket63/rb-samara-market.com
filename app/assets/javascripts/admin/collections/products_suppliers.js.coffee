class SmAdmin.Collections.ProductsSuppliers extends Backbone.Collection

  model: SmAdmin.Models.ProductsSupplier

  url: window.model_url+'/products_suppliers'