class SmAdmin.Views.ProductsTypesList extends Backbone.View

  template: JST['admin/products_types/list']
  
  getHtml: ()->
    @get$().html()
  get$: ()->
    @render().$el
  render: ()->
    $(@el).html(@template())
    this