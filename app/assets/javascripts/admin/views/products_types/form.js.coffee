class SmAdmin.Views.ProductsTypesFrom extends Backbone.View

  template: JST['admin/products_types/form']
  
  events:
    'submit form.products_types': 'save'

  getHtml: (model)->
    @get$(model).html()

  get$: (model)->
    @render(model).$el

  render: (model)->
    
    $(@el).html(@template(model:model))
    
    this

  save: ()->
    event.preventDefault()
    form = $(event.currentTarget).find('form')
    data = form.serializeObject()

    options = 
      wait: true
      success: (model, response)-> 
        if !data.id
          form.parents('div[data-role="window"]').data("kendoWindow").close()
          window.location = '#products_types/'+model.get('id')+'/edit'
        SmAdmin.Helpers.Form.success(model, response, form)
      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)

    if data.id
      window.Collection.ProductsTypes.get(data.id).save(data,options)
    else
      window.Collection.ProductsTypes.create(data,options)