class SmAdmin.Views.Catalog extends Backbone.View
  templateIndex: JST['admin/catalog/index']

  renderIndex: ->
    block = 
      category: new SmAdmin.Views.CategoriesTree().getHtml()
      products_types: new SmAdmin.Views.ProductsTypesList().getHtml()
      brands: new SmAdmin.Views.BrandsList().getHtml()
      products_suppliers: new SmAdmin.Views.ProductsSuppliersList().getHtml()
      products_groups: new SmAdmin.Views.ProductsGroupsList().getHtml()

    $(@el).html(@templateIndex(block: block))
    
    this