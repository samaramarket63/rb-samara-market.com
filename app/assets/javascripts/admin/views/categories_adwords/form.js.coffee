class SmAdmin.Views.CategoriesAdwordsForm extends Backbone.View

  template: JST['admin/categories_adwords/form']

  category_id: null

  events:
    'submit form': 'save'

  getHtml: (model)->
    @get$(model).html()

  get$: (model)->
    @render(model).$el

  render: (model)->
    
    $(@el).html(@template(model:model, category_id: @category_id))
    
    this

  save: ->
    event.preventDefault()
    form = $(event.target)
    data = form.serializeObject()
    window.s = form
    options =
      wait: true
      success: (model, response)->
        if !data.id
          form.parents('div[data-role="window"]').data("kendoWindow").close()
          window.location = '#categories/'+data.category_id+'/adwords/'+model.get('id')+'/edit'
        else
          SmAdmin.Helpers.Form.success(model, response, form) 
      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)

    if data.id
      window.Collection.CategoriesAdwords.get(data.id).save(data,options)
    else
      window.Collection.CategoriesAdwords.create(data,options)