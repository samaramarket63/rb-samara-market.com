class SmAdmin.Views.AdwordsIndex extends Backbone.View

  template: JST['admin/adwords/index']

  render: (adwords)->
    $(@el).html(@template(adwords:adwords))
    
    this