class SmAdmin.Views.OrdersFrom extends Backbone.View

  template: JST['admin/orders/form']
  
  events:
    'submit form.order_form': 'save'
    'click .qiwi-bil-action' : 'qiwi_bil'

  getHtml: (model)->
    @get$(model).html()

  get$: (model)->
    @render(model).$el

  render: (model)->
    
    $(@el).html(@template(model:model))
    
    this

  save: ()->
    event.preventDefault()
    form = $(event.currentTarget).find('form')
    data = form.serializeObject()
    options =
      wait: true
      success: (model, response)-> 
        SmAdmin.Helpers.Form.success(model, response, form)
      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)

    o = new SmAdmin.Models.Order()
    o.save(data,options)

  qiwi_bil: ()->
    event.preventDefault()


    order = $(event.currentTarget).find('input[name="id"]').val()
    confirmation = prompt('Точно выставить счёт за заказ А-'+order+' на сумму '+$(event.currentTarget).find('.total_sum_for_order').text(),'нет')
    if confirmation == 'да'
      $.ajax(
        url: window.model_url+'/orders/'+order+'/bil_qiwi',
        type: 'PUT',
        success: (res)->
          if res.result_code == 0
            alert('Статус счёта '+res.bill.status)
          else
            alert('Ошибка сервиса')
            console.log res
        error: (res)->
          alert('Ошибка')
          console.log res
      )
