class SmAdmin.Views.ProductsPropertiesList extends Backbone.View

  template: JST['admin/products/properties_list']

  getHtml: (collection=null)->
    @get$(collection).html()

  get$: (collection=null)->
    @render(collection).$el

  render: (collection=null)->
    
    $(@el).html(@template(collection:collection))
    
    this