class SmAdmin.Views.ProductsIndex extends Backbone.View

  template: JST['admin/products/index']

  render: (collection=null)->
    unless ($('#product_list')).length
      window.location.hash = "#catalog"
  