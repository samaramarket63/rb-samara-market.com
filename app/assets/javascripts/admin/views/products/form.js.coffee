class SmAdmin.Views.ProductsForm extends Backbone.View

  template: JST['admin/products/form']

  className: 'product_window'

  events:
    'change form.product select.list_discount_type_id': 're_discount'
    'keyup form.product #form_product_discount_value': 're_discount'
    'submit form.product': 'save'
    'submit form.product_categories': 'saveCategories'
    'submit .product_properties form': 'addValue'
    'submit form.suppliers': 'addItem'
    'click .items_list a.close' :'destroyItem'
    'click button.rebrand': 'rebrand'
    'click button.retype': 'retype'
    'click button.regroup': 'regroup'
    'click button.clear_color': 'clear_color'
    'click .product_files .images a.close': 'deleteImage'
    'click .product_files .images input[type="radio"][name="cover"]': 'setCover'

  getHtml: (model=null)->
    @get$(model).html()

  get$: (model=null)->
    @render(model).$el

  render: (model=null)->
    
    $(@el).html(@template(model:model))
    
    this
  regroup: ()->
    select = $(event.currentTarget).find('form.product select[name="group_id"]')
      .parents('.wrap').html('<select type="text" name="group_id" id="form_product_group" class="span5">
        </select>')
      .find('select')
    window.Collection.Groups.fetch({
      success:(collection, response)->
        coll = SmAdmin.Helpers.Collection.toArray(collection,['id', 'name'])
        coll.unshift({name: null, id: null})
        SmAdmin.Helpers.Form.combox(coll,select)
    })

  re_discount: ()->

    form = $(event.currentTarget)
    type = form.find('.list_discount_type_id').val()
    value = form.find('#form_product_discount_value').val()
    price = form.find('#form_product_price').val()
    label = form.find('span.real_price')
    switch parseInt(type)
      when 1 then real_price = (price-((price/100)*value))
      when 2 then real_price = (price-value)
      when 3 then real_price = value
      else real_price = price
    label.text(Math.ceil(real_price))
  retype: ()->

    select = $(event.currentTarget).find('form.product select[name="type_id"]')
      .parents('.wrap').html('<select type="text" name="type_id" id="form_product_type" class="span5">
        </select>')
      .find('select')
    window.Collection.ProductsTypes.fetch({
      success:(collection, response)->
        SmAdmin.Helpers.Form.combox(
          SmAdmin.Helpers.Collection.toArray(collection,['id', 'name']),
          select
        )
    })

  rebrand: ()->
    select = $(event.currentTarget).find('form.product select[name="brand_id"]')
      .parents('.wrap').html('<select type="text" name="brand_id" id="form_product_brand" class="span5">
        </select>')
      .find('select')
    window.Collection.Brands.fetch({
      success:(collection, response)->
        SmAdmin.Helpers.Form.combox(
          SmAdmin.Helpers.Collection.toArray(collection,['id', 'name']),
          select
        )
    })
  clear_color: ()->
    $(event.currentTarget)
      .find('form input[name="color_name"],form input[name="color_code"]')
      .val(null)

  save: ()->
    event.preventDefault()
    form = $(event.currentTarget).find('form.product')
    data = form.serializeObject()
    options = 
      wait: true
      success: (model, response)-> 
        if !data.id
          form.parents('div[data-role="window"]').data("kendoWindow").close()
          window.location = '#products/'+model.get('id')+'/edit'
        if data.uid and (grid = $('#product_list').data("kendoGrid"))
          src = grid.dataSource.getByUid(data.uid)
          model.set('brand_id', Number(model.get('brand_id')))
          model.set('type_id', Number(model.get('type_id')))
          if model.get('brand_id') != src['brand_id']
            src['brand_name'] = window.Collection.Brands.get(model.get('brand_id')).get('name')
          if model.get('type_id') != src['types_id']
            src['type_name'] = window.Collection.ProductsTypes.get(model.get('type_id')).get('name')
          for prop of src
            (src[prop] = model.attributes[prop]) if model.attributes[prop]?
          grid.refresh()
        SmAdmin.Helpers.Form.success(model, response, form)
      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)
    model = new SmAdmin.Models.Product(data)
    model.save({},options)

  saveCategories: ()->
    event.preventDefault()
    form = $(event.currentTarget).find('form.product_categories')
    data = form.serializeObject()
    options = 
      wait: true
      success: (model, response)-> 
        SmAdmin.Helpers.Form.success(model, response, form)
      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)
    model = new SmAdmin.Models.Product(data)
    model.url = model.url()+'/categories'
    model.save({},options)

  deleteImage: ()->
    event.preventDefault()
    if confirm("Удалить изображение?")
      $this = $(event.target)
      $this.data('img-id')
      jqxhr = $.ajax({
                url: window.model_url+'/products/'+$this.data('product-id')+'/images/'+$this.data('img-id')
                type: 'DELETE'
                error: ->
                  alert('Ошибка удаления')
                success: ->
                  $this.parent().remove()
              });
  setCover: ()->
    $this = $(event.target)
    data ={}
    data[window.csrf.name] = window.csrf.value
    jqxhr = $.ajax({
              url: window.model_url+'/products/'+$this.data('product-id')+'/images/'+$this.val()+'/cover'
              type: 'PUT'
              data: data
              error: ->
                alert('Ошибка установки обложки')
              success: ->
                $this.parents('ul').find('li.active').removeClass('active')
                $this.parent().addClass('active')
            });
  addValue: ->
    event.preventDefault()
    form = $(event.target)
    data = form.serializeObject()
    data[window.csrf.name] = window.csrf.value
    $.ajax({
      data: data
      url: window.model_url+'/products/'+data.product_id+'/products_property/'+data.property_id+'/products_property_value'
      type: 'POST'
      success: (property, textStatus, jqXHR)->
        $('.properties.product'+data.product_id+' ul.values.property'+data.property_id).append('
          <li>'+property.value+'
            <a class="close"
              href="#/products/'+data.product_id+'/products_property/'+data.property_id+'/products_property_value/'+property.id+'">
              &times;</a>
          </li>
                  ')
      error: (jqXHR, textStatus, errorThrown)->
        errors = $.parseJSON(jqXHR.responseText)
        text = 'Ошибка '+textStatus+"\r\n"
        for error of errors
          text+= error+"\r\n"
          for msg in errors[error]
            text+= ' -'+msg+"\r\n"
        alert text
      });
  addItem: ->
    event.preventDefault()
    form = $(event.target)
    data = form.serializeObject()
    options = 
      wait: true
      success: (model, response)-> 
        SmAdmin.Helpers.Form.success(model, response, form)
        model = model.attributes
        item = '<p>'+window.Collection.ProductsSuppliers.get(model.supplier_id).get('name')+'
              <span class="pur_price">/'+model.price+' руб.</span>
              <a class="close" href="#" data-product_id="'+data.id+'" data-item_id="'+model.id+'">&times;</a>
              </p>
              <p>'+model.uid+'</p>'
        form.parent().find('.items_list').append('<li>'+item+'<li>')
      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)

    model = new SmAdmin.Models.Product(data)
    model.url = model.url()+'/item'
    model.save({},options)
  destroyItem: ->
    event.preventDefault()
    link = $(event.target)
    options = 
      wait: true
      success: (model, response)->
        link.parents('li').remove()
      error: (model, response)->
        
        text = 'Ошибка '+response.status+"\r\n"

        try
          errors = $.parseJSON(response.responseText)
          for error of errors
            text+= error+"\r\n"
            for msg in errors[error]
              text+= ' -'+msg+"\r\n"
        
        alert text

    data=
      id:link.data('product_id'),
      
    if confirm('Удалить товар из прайса поставщика?')
      model = new SmAdmin.Models.Product(data)
      model.url = model.url()+'/item/'+link.data('item_id')
      model.destroy(options)