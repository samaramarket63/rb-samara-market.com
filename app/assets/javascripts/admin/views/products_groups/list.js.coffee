class SmAdmin.Views.ProductsGroupsList extends Backbone.View

  template: JST['admin/products_groups/list']

  getHtml: ()->
    @get$().html()
  get$: ()->
    @render().$el
  render: ()->
    $(@el).html(@template())
    this