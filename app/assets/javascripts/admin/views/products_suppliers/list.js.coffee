class SmAdmin.Views.ProductsSuppliersList extends Backbone.View

  template: JST['admin/products_suppliers/list']
  
  getHtml: ()->
    @get$().html()
  get$: ()->
    @render().$el
  render: ()->
    $(@el).html(@template())
    this