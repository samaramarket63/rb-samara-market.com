class SmAdmin.Views.CategoriesForm extends Backbone.View

  template: JST['admin/categories/form']

  events:
    'submit form.category': 'save'
    'change .adwords input[name="menu_banner"]': 'changeAdwordForNav'

  getHtml: (model)->
    @get$(model).html()

  get$: (model)->
    @render(model).$el

  render: (model)->
    
    $(@el).html(@template(model:model))
    
    this
  changeAdwordForNav: (e)->
    adword= window.Collection.CategoriesAdwords.get(e.target.value)
    category= window.Collection.categories.get(e.target.getAttribute("data-model-id"))

    $.ajax({
      url: window.model_url+'/categories/'+category.id+'/adwords/'+adword.id+'/cover'
      type: 'POST'
      success: (s_adword, textStatus, jqXHR)->
        category.set('adword_id',adword.id)
      error: (jqXHR, textStatus, errorThrown)->
        errors = $.parseJSON(jqXHR.responseText)
        text = 'Ошибка '+textStatus+"\r\n"
        for error of errors
          text+= error+"\r\n"
          for msg in errors[error]
            text+= ' -'+msg+"\r\n"
        alert text
      });

  save: ->
    event.preventDefault()
    form = $(event.currentTarget).find('form')
    data = form.serializeObject()
    console.log '0'
    options =
      wait: true
      success: (model, response)->
        if !data.id
          form.parents('div[data-role="window"]').data("kendoWindow").close()
          window.location = '#categories/'+model.get('id')+'/edit'
        else
          SmAdmin.Helpers.Form.success(model, response, form) 
      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)

    if data.id
      window.Collection.categories.get(data.id).save(data,options)
    else
      window.Collection.categories.create(data,options)
