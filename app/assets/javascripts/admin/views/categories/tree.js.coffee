class SmAdmin.Views.CategoriesTree extends Backbone.View

  template: JST['admin/categories/tree']



  getHtml: ()->
    @get$().html()
  get$: ()->
    @render().$el
  render: ()->
    $(@el).html(@template())
    this