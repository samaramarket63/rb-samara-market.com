class SmAdmin.Views.ProductsTypePropertiesList extends Backbone.View

  template: JST['admin/products_type_properties/list']

  getHtml: (collection, type_id)->
    @get$(collection, type_id).html()
  get$: (collection, type_id)->
    @render(collection, type_id).$el
  render: (collection, type_id)->
    $(@el).html(@template(collection:collection, type_id:type_id))
    this

  save: ()->
    event.preventDefault()
    form = $(event.currentTarget).find('form')
    data = form.serializeObject()
    $this = @
    options =
      wait: true
      success: (model, response)-> 
        field = form.parent()
        if node = window.properties[type_id].get(data.id)
          node.set(data)
        else
           window.properties[type_id].add(model)
        $this.show_node(field,model,type_id)

      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)


    model = new SmAdmin.Models.ProductsTypeProperty()
    model.url = window.model_url+'/products_types/'+data.type_id+'/type_property'

    type_id = data.type_id
    delete (data.type_id)

    if data.id
      model.url = model.url+'/'+data.id

    model.save(data, options)

  show_node: (field,model,type_id)->

    html = model.get('name')+'
      <span class="toolbar data"">
        <a href="#products_type_properties/'+type_id+'/'+model.get('id')+'/edit"><i class="icon-edit"></i></a>
        <a href="#products_type_properties/'+type_id+'/'+model.get('id')+'/delete"><i class="icon-trash"></i></a>
      </span>'
    field.attr('data-id',model.get('id'))
    field.html(html)