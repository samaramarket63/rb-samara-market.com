class SmAdmin.Views.ProductsTypePropertiesForm extends Backbone.View

  template: JST['admin/products_type_properties/form']

  getHtml: (type_id,model=null)->
    @get$(type_id,model).html()
  get$: (type_id,model=null)->
    @render(type_id,model).$el
  render: (type_id,model=null)->
    $(@el).html(@template(model:model, type_id:type_id))
    this
