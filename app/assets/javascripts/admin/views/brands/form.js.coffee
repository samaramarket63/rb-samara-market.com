class SmAdmin.Views.BrandsFrom extends Backbone.View

  template: JST['admin/brands/form']
  
  events:
    'submit form.brands': 'save'

  getHtml: (model)->
    @get$(model).html()

  get$: (model)->
    @render(model).$el

  render: (model)->
    
    $(@el).html(@template(model:model))
    
    this

  save: ()->
    event.preventDefault()
    form = $(event.currentTarget).find('form')
    data = form.serializeObject()
    options =
      wait: true
      success: (model, response)-> 
        form
          .parents('div.k-window')
          .find('div.k-window-titlebar .k-window-title')
          .text('Редактирование бренда '+ form.find('input[name="name"]').val())
        SmAdmin.Helpers.Form.success(model, response, form)
      error: (model, response)->
        SmAdmin.Helpers.Form.error(response, form)
    if data.id
      window.Collection.Brands.get(data.id).save(data,options)
    else
      window.Collection.Brands.create(data,options)
