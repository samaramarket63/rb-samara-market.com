class SmAdmin.Views.BrandsList extends Backbone.View

  template: JST['admin/brands/list']
  
  getHtml: ()->
    @get$().html()
  get$: ()->
    @render().$el
  render: ()->
    $(@el).html(@template())
    this