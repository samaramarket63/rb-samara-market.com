class SmAdmin.Views.ArticlesIndex extends Backbone.View

  template: JST['admin/articles/index']

  render: (articles)->
    $(@el).html(@template(articles:articles))
    
    this