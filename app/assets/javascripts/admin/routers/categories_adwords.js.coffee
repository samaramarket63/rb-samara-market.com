class SmAdmin.Routers.CategoriesAdwords extends Backbone.Router

  routes:
    'categories/:category_id/adwords/new' : 'new'
    'categories/:category_id/adwords/:id/edit' : 'edit'
    'categories/:category_id/adwords/:id/delete' : 'delete'

  new: (category_id)->
    model = 
    if category = window.Collection.categories.get(category_id)
      @renderForm('Создание баннера для категории '+category.get('name'), category)
    else
      alert('Категория не существует')

  edit: (category_id, id)->
    adword = null
    category = window.Collection.categories.get(category_id)
    if category and (adword = window.Collection.CategoriesAdwords.get(id))

      @renderForm('Редактирование баннера '+adword.get('name')+' для категории '+category.get('name'), category, adword.attributes)
    else
      alert 'Баннер или категория не найденны'
  delete: (category_id, id)->
    window.location = "#"
    model = window.Collection.CategoriesAdwords.get(id)
    if confirm('Удалить баннер '+model.get('name')+'?')
      model.destroy({
        wait: true
        success: ->
          $('.category_form .adword[data-id="'+id+'"]').remove()
        error: ->
          alert("Ошибка удаления баннера")
      })
  renderForm: (title, category, model=null)->
    form = new SmAdmin.Views.CategoriesAdwordsForm()
    form.category_id = category.get('id')
    form = SmAdmin.Helpers.Form.openWindow(form, title, model)
    $form = $(form)
   
    if model
      $form.find('input[name="image"]').kendoUpload({
        async:
          saveField: 'image'
          saveUrl: window.model_url+'/categories/'+category.get('id')+'/adwords/'+model.id+'/image/',
          autoUpload: true
        showFileList: false
        success: (e)->
          if  model = window.Collection.CategoriesAdwords.get(e.response.id)
            model.attributes = e.response
            $(@element[0]).parents('.controls:first').find('.image_preview').html('<img src="'+e.response.image.url+'" >')
          console.log e
        error: (e)->
          alert('Ошибка загрузки')
          console.log e
      });

      $(form).find('input[name="background"]').kendoUpload({
        async:
          saveField: 'image'
          saveUrl: window.model_url+'/categories/'+category.get('id')+'/adwords/'+model.id+'/background/',
          autoUpload: true
        showFileList: false
        success: (e)->
          if  model = window.Collection.CategoriesAdwords.get(e.response.id)
            model.attributes = e.response
            $(@element[0]).parents('.form-inline:first').find('.image_preview').html('<img src="'+e.response.background.url+'" >')

        error: (e)->
          alert('Ошибка загрузки')
          console.log e
      });