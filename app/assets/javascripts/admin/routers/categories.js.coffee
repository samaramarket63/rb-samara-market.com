class SmAdmin.Routers.Categories extends Backbone.Router
  routes:
    'catalog' : 'index'
    'categories/new' : 'new'
    'categories/:id/edit' : 'edit'
    'categories/:id/delete' : 'delete'
    'categories/:id/status' : 'toggleStatus'
    'categories/:id/products' : 'showProducts'
    'categories/update' : 'update'

  index: ->
    views = new SmAdmin.Views.Catalog()
    $('#container').html(views.renderIndex().el)
    SmAdmin.Helpers.Categories.view_tree($('#a_categories_list .categories_tree'))
    
  update: ->
    window.location = "#"
    window.Collection.categories.fetch()
    SmAdmin.Helpers.Categories.view_tree($('.categories_tree').attr('class','categories_tree').html('загружаю'))      
  toggleStatus: (id)->
    options =       
      success: (model, response)->
          model.fetch(
            success: ->
              $('.categories_tree')
                .find('span[data-model-id="'+model.get('id')+'"] a.status i')
                .attr('class','k-icon status-'+model.get('status'))
            error: @error             
          )

        error: (model, response) ->
          alert("Ошибка изменения статуса категории")
    window.Collection.categories.get(id).toggleStatus(options)
    window.location = "#"


  delete: (id)->
    window.location = "#"
    model = window.Collection.categories.get(id)
    if confirm('Удалить категорию '+model.get('name')+'?')
      model.destroy({
        wait: true
        success: ->
          tree = $('.categories_tree').data("kendoTreeView")
          tree.remove($('.categories_tree').
            find('span[data-model-id="'+model.get('id')+'"]').parent()
          ) 
        error: ->
          alert("Ошибка удаления категории")
      })

  new: ->
    @renderForm('Создание новой категории')

  edit: (id)->
    if model = window.Collection.categories.get(id)
      @renderForm('Редактирование категории '+model.get('name'),model.attributes)
    else
      alert('Категория не существует')

  renderForm: (title,model=null)->
    window.s = model
    form = new SmAdmin.Views.CategoriesForm()
    form = SmAdmin.Helpers.Form.openWindow(form, title, model)
    kList = SmAdmin.Helpers.Categories.reloadSelect(form.find('select[name="parent_id"]'))
    if model
      SmAdmin.Helpers.Form.sort_position($(form).find('.adwords form'),'/categories/'+model.id+'/adwords')
    if model and model.parent_id
      selected = kList.list.find('span[data-model-id="'+model.parent_id+'"]').parent()
      kList.select(selected)

  showProducts: (id)->
    SmAdmin.Helpers.Products.show(window.Collection.categories,id)

