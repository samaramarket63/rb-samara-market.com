class SmAdmin.Routers.ProductsSuppliers extends Backbone.Router

  routes:
    'products_suppliers/new' : 'new'
    'products_suppliers/:id/edit' : 'edit'
    'products_suppliers/:id/delete' : 'delete'
    'products_suppliers/:id/products' : 'showProducts'

  new: ->
    @renderForm('Создание нового поставщика')

  edit: (id)->
    if model = window.Collection.ProductsSuppliers.get(id)
      @renderForm('Редактирование поставщика '+model.get('name'),model.toJSON())
    else
      alert('Поставщика не существует')
  delete: (id)->
    window.location = "#"
    model = window.Collection.ProductsSuppliers.get(id)
    if confirm('Удалить поставщика '+model.get('name')+'?')
      model.destroy({
        wait: true
        success: ->
          $('.products_suppliers_list span[data-model-id="'+model.get('id')+'"]')
            .parent()
            .remove()
        error: ->
          alert("Ошибка удаления поставщика")
      })

  renderForm: (title,model=null)->
    form = new SmAdmin.Views.ProductsSuppliersForm()
    form = SmAdmin.Helpers.Form.openWindow(form, title, model)
    
  showProducts: (id)->
    SmAdmin.Helpers.Products.show(window.Collection.ProductsSuppliers,id)