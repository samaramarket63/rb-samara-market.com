class SmAdmin.Routers.ProductsGroups extends Backbone.Router

  routes:
    'products_groups/new' : 'new'
    'products_groups/:id/edit' : 'edit'
    'products_groups/:id/delete' : 'delete'
    'products_groups/:id/products' : 'showProducts'
  new: ->
    @renderForm('Создание новоой группы')

  edit: (id)->
    if model = window.Collection.Groups.get(id)
      @renderForm('Редактирование группы '+model.get('name'),model.toJSON())
    else
      alert('Группа не существует')
  delete: (id)->
    window.location = "#"
    model = window.Collection.Groups.get(id)
    if confirm('Удалить группу '+model.get('name')+'?')
      model.destroy({
        wait: true
        success: ->
          $('.products_groups_list span[data-model-id="'+model.get('id')+'"]')
            .parent()
            .remove()
        error: ->
          alert("Ошибка удаления группы")
      })

  renderForm: (title,model=null)->
    form = new SmAdmin.Views.ProductsGroupsForm()
    form = SmAdmin.Helpers.Form.openWindow(form, title, model)
  showProducts: (id)->
    SmAdmin.Helpers.Products.show(window.Collection.Groups,id)