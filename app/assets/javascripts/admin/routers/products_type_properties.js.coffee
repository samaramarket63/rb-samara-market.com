class SmAdmin.Routers.ProductsTypeProperties extends Backbone.Router

  routes:
    'products_type_properties/:type_id/new' : 'new'
    'products_type_properties/:type_id/:id/edit' : 'edit'
    'products_type_properties/:type_id/:id/delete' : 'delete'

  new: (type_id)->
    window.location = "#"
    wrap = $('.properties_block_'+type_id)

    form = @renderForm(type_id)
  
    SmAdmin.Helpers.editor(wrap.find('ul')
      .append('<li>'+form+'</li>'))
  edit: (type_id,id)->
    window.location = "#"
    field = $('.properties_block_'+type_id+' li[data-id="'+id+'"]')
    data = window.properties[type_id].get(id) 
    SmAdmin.Helpers.editor(field.html(@renderForm(type_id,data)))
  delete: (type_id,id)->
    window.location = "#"
    model = window.properties[type_id].get(id)
    model.url = window.model_url+'/products_types/'+type_id+'/type_property/'+id
    if confirm('Удалить свойство '+model.get('name')+'?')
      model.destroy({
        wait: true
        success: ->
          $('.properties_block_'+type_id+' li[data-id="'+id+'"]').remove()
        error: ->
          alert("Ошибка удаления свойства")

      })   

  renderForm: (type_id, model=null)->
    form = new SmAdmin.Views.ProductsTypePropertiesForm()
    form.getHtml(type_id, model)
