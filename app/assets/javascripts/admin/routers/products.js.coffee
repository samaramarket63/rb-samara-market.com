class SmAdmin.Routers.Products extends Backbone.Router
  routes:
    'products/new' : 'new'
    'products/:id/edit' : 'edit'
    'products/:id/edit/:uid' : 'edit'
    'products/:id/destroy/:uid' : 'destroy'
    'products/:id/destroy' : 'destroy'
    'products/:product_id/products_property/:property_id/products_property_value/:id' : 'delValue'


  new: ()->
    @renderForm('Создание нового товара')

  edit: (id,uid=false)->
    product = new SmAdmin.Models.Product({id:id})
    $this = @
    product.fetch(
      wait: true
      success: (model, response)->
        $this.renderForm 'Редактирование товара '+model.get('name'), model.attributes, uid
      error: ->
        alert 'Ошибка'
    )
  destroy: (id,uid=false)->
    window.location.hash = ''
    if confirm('Удалить товар №'+id+'?')
      model = new SmAdmin.Models.Product({id:id})
      options = 
        wait: true
        success: (model, response)->
          if uid and (grid = $('#product_list').data("kendoGrid"))
            grid.dataSource.remove(grid.dataSource.getByUid(uid))
            grid.refresh()
        error: (model, response)->
          alert('Ошибка')
      model.destroy(options)

  renderForm: (title,model=null,uid=false)->
    form = new SmAdmin.Views.ProductsForm()
    form = SmAdmin.Helpers.Form.openWindow(form, title, model)

    win = 
      p_main: {}
    win.p_main.form = form.find('.product_main form')

    win.p_main.brand = win.p_main.form.find('select[name="brand_id"]')

    colType = SmAdmin.Helpers.Collection
      .toArray(window.Collection.ProductsTypes,['id', 'name'])
    colBrand = SmAdmin.Helpers.Collection
      .toArray(window.Collection.Brands,['id', 'name'])
    colGroup = SmAdmin.Helpers.Collection
      .toArray(window.Collection.Groups,['id', 'name'])
    colGroup.unshift({name: null, id: null})
    win.p_main.type = SmAdmin.Helpers.Form.combox(colType, win.p_main.form.find('select[name="type_id"]'))
    win.p_main.brand = SmAdmin.Helpers.Form.combox(colBrand, win.p_main.form.find('select[name="brand_id"]'))
    win.p_main.group = SmAdmin.Helpers.Form.combox(colGroup, win.p_main.form.find('select[name="group_id"]'))
    if model
      $form = $(form)
      if uid
        $form.find('form.main').append('<input type="hidden" name="uid" value="'+uid+'">')
      selectedType = win.p_main.type.list.find('span[data-model-id="'+model.type_id+'"]').parent()
      win.p_main.type.select(selectedType)
      selectedBrand = win.p_main.brand.list.find('span[data-model-id="'+model.brand_id+'"]').parent()
      win.p_main.brand.select(selectedBrand)
      selectedGroup = win.p_main.group.list.find('span[data-model-id="'+model.group_id+'"]').parent()
      win.p_main.group.select(selectedGroup)

      categories_list = form.find('.product_categories .cat_list')
      SmAdmin.Helpers.Categories.view_form_tree categories_list

      $.get(window.base_url+'/products/'+model.id+'/categories').success((response)->
        response.map((v)->
          categories_list.find('input[value="'+v.id+'"]').attr('checked','checked')
        )
      )
      .error((response)->
        SmAdmin.Helpers.Form.error(response, categories_list.parent('form'))
      )  
      $form.find('.product_files .images .upload').kendoUpload({
        async:
          saveField: 'image'
          saveUrl: window.model_url+'/products/'+model.id+'/images/',
          autoUpload: true
        showFileList: false
        success: (e)->
          $('div.images.product'+e.response.product_id+' ul')
            .append('<li "class="active" data-id="'+e.response.id+'">
              <a href="#" class="close" data-img-id="'+e.response.id+'" data-product-id="'+e.response.product_id+'">&times;</a>
              <img src="'+ e.response.image.thumb.url+'" >
              <input type="radio" name="cover" data-product-id="'+e.response.product_id+'" value="'+e.response.id+'">
            </li>')
        select: (e)->
          files = e.files;
          $.each(files, ->
            if (this.extension != ".jpg") and (this.extension != ".jpeg") and (this.extension != ".png") and (this.extension != ".gif")
              alert("Только jpg/jpeg,gif или png")
              e.preventDefault();
          );
      });
      SmAdmin.Helpers.Form.sort_position($form.find('.product_files .images ul'),'/products/'+model.id+'/images')
      properties_list = form.find('.product_properties ul')

  delValue: (product_id, property_id,value_id)->
    data={}
    url = '/products/'+product_id+'/products_property/'+property_id+'/products_property_value/'+value_id
    data[window.csrf.name] = window.csrf.value
    $.ajax({
      url: window.model_url+'/products/'+product_id+'/products_property/'+property_id+'/products_property_value/'+value_id
      type: 'DELETE'
      data: data
      error: ->
        alert('Ошибка удаления свойства')
      success: ->
        $('.properties.product'+product_id).find('a[href="#'+url+'"]').parent('li').remove()
    });
