class SmAdmin.Routers.ProductsTypes extends Backbone.Router

  routes:
    'products_types/new' : 'new'
    'products_types/:id/edit' : 'edit'
    'products_types/:id/delete' : 'delete'
    'products_types/:id/products' : 'showProducts'

  new: ->
    @renderForm('Создание нового типа')

  edit: (id)->
    if model = window.Collection.ProductsTypes.get(id)
      @renderForm('Редактирование типа '+model.get('name'),model.toJSON())
    else
      alert('Тип не существует')
  delete: (id)->
    window.location = "#"
    model = window.Collection.ProductsTypes.get(id)
    if confirm('Удалить тип '+model.get('name')+'?')
      model.destroy({
        wait: true
        success: ->
          $('.products_types_list span[data-model-id="'+model.get('id')+'"]')
            .parent()
            .remove()
        error: (data, jqXHR, textStatus)->
          errors = $.parseJSON(jqXHR.responseText)
          text = 'Ошибка '+textStatus+"\r\n"
          for error of errors
            text+= error+"\r\n"
            for msg in errors[error]
              text+= ' -'+msg+"\r\n"
          alert text
      })

  showProducts: (id)->
    SmAdmin.Helpers.Products.show(window.Collection.ProductsTypes,id)
    
  renderForm: (title,model=null)->
    form = new SmAdmin.Views.ProductsTypesFrom()
    form = SmAdmin.Helpers.Form.openWindow(form, title, model)
    if type_id = form.find('input[name="id"]').val()
      properties = new SmAdmin.Collections.ProductsTypeProperties({},{type_id:type_id})
      window.properties[type_id] = properties
      properties.fetch(
        success: (data, textStatus, jqXHR)->
          view = new SmAdmin.Views.ProductsTypePropertiesList()
          ul = form.find('.view_properties')
            .html(view.getHtml(data.toArray(),type_id))
            .on('submit','form',->
              view.save()
            )
            .on('reset','form',->
              event.preventDefault()
              $this = $(this)
              if id = $this.find('input[name="id"]').val()
                view.show_node($this.parent(),window.properties[type_id].get(id),type_id)
              else
                $this.parent().remove()
            )
          SmAdmin.Helpers.Form.sort_position(ul.find('ul'),'/products_types/'+type_id+'/type_property')


        error: (data, textStatus, jqXHR)->
          alert ('Получение свойств типа, ошибка .'+textStatus.status+' - '+textStatus.statusText)
      )