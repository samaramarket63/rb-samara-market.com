class SmAdmin.Routers.Adwords extends Backbone.Router

  routes:
    'adword/main' : 'index'

  index: ()->
    views = new SmAdmin.Views.AdwordsIndex()
    adwords = window.Collection.CategoriesAdwords.where({category_id:1})
    fragment = views.render(adwords).el
    $('#container_for_content').html(fragment)

    SmAdmin.Helpers.Form.sort_position($(fragment).find('#main_adwords_banners'),'/categories/1/adwords')