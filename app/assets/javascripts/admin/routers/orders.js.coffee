class SmAdmin.Routers.Orders extends Backbone.Router
  routes:
    'orders' : 'index'
    'orders/active' : 'active'
    'orders/closed' : 'closed'
    'orders/all' : 'all'
    'orders/show/:id' : 'show'

  index: ->
    views = new SmAdmin.Views.Orders()
    $('#container').html(views.renderIndex().el)

  active: ->
    @show_orders('active')

  closed: ->
    @show_orders('closed')
  all: ->
    @show_orders()
  show: (id)->
    order = new SmAdmin.Models.Order()
    order.set('id', id).fetch(
      success: (model, xhr)->
        form = new SmAdmin.Views.OrdersFrom()
        total_sum = 0
        for p in model.attributes.cart.products_in_cart
          total_sum += p.sale_price * p.count
        model.set('total_sum', total_sum)
        arrived = 0
        for p in model.attributes.cart.products_in_cart
          arrived += (p.purchase_price*1) * p.count
        model.set('profit',total_sum-arrived)
        form = SmAdmin.Helpers.Form.openWindow(form, 'Заказ № '+model.get('bill'), model.attributes, 1000)
      error: ()->
        alert('Не удалось получить данные о заказе')
    )

  show_orders: (status=null)->
    orders = new SmAdmin.Collections.Orders()
    $('#loading').show()
    window.www = orders
    orders.fetch(
      data: {status: status}
      success: (collection, response)->
        if $('#container_for_orders').length < 1
          views = new SmAdmin.Views.Orders()
          $('#container').html(views.renderIndex().el)
        $('#loading').hide()
        console.log collection
        $("#orders_grid").kendoGrid(
          dataSource:
            data: collection.toJSON()
            pageSize: 20
          title: 'sd'
          sortable: true,
          pageable:
            refresh: true,
            pageSizes: true
          columns: [ 
            {
              field: "bill",
              width: 50,
              title: "Номер"
            },
            {
              field: "user_name",
              width: 110,
              title: "Пользователь"
            },
            {
              field: "phone",
              width: 70,
              title: "Телефон"
            },
           {
              field: "text_status",
              width: 150,
              title: "Статус"
            },
            {
              field: 'date',
              width: 80,
              title: 'Дата',
              filterable: {
                ui: 'datetimepicker'
              }
            },
            { command: [{ text: "подробно", click: (e)->
                e.preventDefault();
                dataItem = this.dataItem($(e.currentTarget).closest("tr"))
                window.location.hash = 'orders/show/'+dataItem.id
            }], title: "&nbsp;", width: "70px" }
          ]
          filterable:
            name: "FilterMenu"
            extra: true 
            messages:
              info: "Фильтр:"
              filter: "фильтр"
              clear: "очистить"
              isTrue: "custom is true"
              isFalse: "custom is false"
              and: "и"
              or: "или"
            operators:
              string:
                eq: "совпадает"
                neq: "не совпадает"
                startswith: "начинается с..."
                contains: "содержит"
                endswith: "заканчивается на..."
              number:
                eq: "равно"
                neq: "не равно"
                gte: "больше или равно"
                gt: "больше"
                lte: "меньше или равно"
                lt: "меньше"
              date:
                eq: "равно",
                neq: "не равно",
                gte: "после и равно"
                gt: "после"
                lte: "до и равно"
                lt: "до" 
        )

      error: ->
        alert('Ошибка получения заказов')
        $('#loading').hide()
    )