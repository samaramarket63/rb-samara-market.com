class SmAdmin.Routers.Articles extends Backbone.Router

  routes:
    'articles/all' : 'index'
    'articles/edit/:id' : 'edit'
    'articles/destroy/:id' : 'destroy'
    'articles/new' : 'new'


  index: ()->

    views = new SmAdmin.Views.ArticlesIndex()
    articles = new SmAdmin.Collections.Articles()


    opts =
      wait: true
      success: (collection, xhr)->
        console.log collection
        $('#container_for_content').html(views.render(articles).el)
      error: ()->
        alert('Не удалось загрузить статьи')

    articles.fetch(opts)

  edit:(id)->
    window.location.hash = ''
    article = new SmAdmin.Models.Article({id:id})
    $this = @
    article.fetch(
      wait: true
      success: (model, xhr)->
        $this.renderForm 'Редактирование статьи '+model.get('name'), model.attributes

      error: ()->
        alert('Не удалось загрузить статю')
    )

  new: ()->
    @renderForm('Создание новой статьи')

  destroy: (id)->
    window.location = "#"
    article = new SmAdmin.Models.Article({id:id}) 
    if confirm('Удалить статью?')
      article.destroy({
        wait: true
        success: ->
          alert('Удалено')
          
        error: ->
          alert("Ошибка удаления баннера")
      })

  renderForm: (title,model=null)->
    form = new SmAdmin.Views.ArticlesForm()
    form = SmAdmin.Helpers.Form.openWindow(form, title, model)
    if model.id
      $form = $(form)
      $.getJSON(window.model_url+'/articles/'+model.id+'/images',(data)->
          html = ''
          for i in data
            html += '<tr><td><img style="width: 80px;"src="'+i.image.url+'"></td><td>'+i.image.url+'</td></tr>'
          $form.find('#images_for_article_'+model.id).html(html)
      )
      $form.find('.form_articles_images_image_uploader').kendoUpload( 
        async:
          saveField: 'image'
          autoUpload: true
          saveUrl: window.model_url+'/articles/'+model.id+'/image/',
        showFileList: false
        success: (e)->
          img = e.response

          $form.find('#images_for_article_'+model.id).append('<tr><td><img style="width: 80px;"src="'+img.image.url+'"></td><td>'+img.image.url+'</td></tr>')
        error: (e)->
          alert('Ошибка загрузки')
          console.log e
      )
