class SmAdmin.Routers.Brands extends Backbone.Router

  routes:
    'brands/new' : 'new'
    'brands/:id/edit' : 'edit'
    'brands/:id/delete' : 'delete'
    'brands/:id/products' : 'showProducts'
  new: ->
    @renderForm('Создание нового бренда')

  edit: (id)->
    if model = window.Collection.Brands.get(id)
      @renderForm('Редактирование бренда '+model.get('name'),model.toJSON())
    else
      alert('Бренд не существует')
  delete: (id)->
    window.location = "#"
    model = window.Collection.Brands.get(id)
    if confirm('Удалить бренд '+model.get('name')+'?')
      model.destroy({
        wait: true
        success: ->
          $('.brands_list span[data-model-id="'+model.get('id')+'"]')
            .parent()
            .remove()
        error: ->
          alert("Ошибка удаления бренда")
      })

  renderForm: (title,model=null)->
    form = new SmAdmin.Views.BrandsFrom()
    form = SmAdmin.Helpers.Form.openWindow(form, title, model)

    if model
      $(form).find('input[name="logo"]').kendoUpload({
        async:
          saveField: 'image'
          saveUrl: Collection.Brands.get(model.id).url()+'/logo',
          autoUpload: true
        showFileList: false
        success: (e)->
          if  model = window.Collection.Brands.get(e.response.id)
            model.attributes = e.response
        error: (e)->
          alert('Ошибка загрузки')
          console.log e
      });

  showProducts: (id)->
    SmAdmin.Helpers.Products.show(window.Collection.Brands,id)