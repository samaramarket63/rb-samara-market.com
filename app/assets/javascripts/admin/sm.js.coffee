window.SmAdmin =
  Models: {}
  Collections: {}
  Views: {}
  Helpers: {}
  Routers: {}
  init: ->
    new SmAdmin.Routers.Categories
    new SmAdmin.Routers.ProductsTypes
    new SmAdmin.Routers.ProductsSuppliers
    new SmAdmin.Routers.Brands
    new SmAdmin.Routers.ProductsTypeProperties
    new SmAdmin.Routers.Products
    new SmAdmin.Routers.ProductsGroups
    new SmAdmin.Routers.CategoriesAdwords
    new SmAdmin.Routers.Contents
    new SmAdmin.Routers.Adwords
    new SmAdmin.Routers.Orders
    new SmAdmin.Routers.Articles
    Backbone.history.start();


$(document).ready ->
  window.onLoad=()->
    $('#loading').show()
  window.offLoad=()->
    $('#loading').hide()

  window.csrf_token = $('meta[name="csrf-token"]').attr('content')
  window.csrf =
    name: $('meta[name=csrf-param]').attr('content')
    value: window.csrf_token
  window.properties = {}
  SmAdmin.init()
  window.Collection.Values = new kendo.data.DataSource()
  $('body').on 'click', 'button.hide_row', ->
    $this = $(this)
    row = $this.parents('.row_block_hide:first').toggleClass('hide_out')