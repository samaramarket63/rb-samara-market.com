window.Helper['slider'] = (scrollPane, item_width)->

  scrollContent = scrollPane.find('.scroll-content')
  scrollPane.css( "overflow", "hidden" )
  scrollContent.width(scrollContent.find('div.item').length*item_width)

  scrollbar = scrollPane.find(".scroll-bar").slider(
    slide: ( event, ui ) ->
      if scrollContent.width() > scrollPane.width()
        scrollContent.css( "margin-left", Math.round(
         ui.value / 100 * ( scrollPane.width() - scrollContent.width() )
        ) + "px" );
      else
        scrollContent.css( "margin-left", 0 )
  )
      
  handleHelper = scrollbar.find( ".ui-slider-handle" )
  .mousedown( ->
    scrollbar.width( handleHelper.width() );
  )
  .mouseup(->
    scrollbar.width( "100%" );
  )
  .append( "<span class='ui-icon ui-icon-grip-dotted-vertical'></span>" )
  .wrap( "<div class='ui-handle-helper-parent'></div>" ).parent();


  sizeScrollbar = ->
    return true if scrollContent.width() <= scrollPane.width()
    remainder = scrollContent.width() - scrollPane.width();
    proportion = remainder / scrollContent.width();
    handleSize = scrollPane.width() - ( proportion * scrollPane.width() );
    scrollbar.find( ".ui-slider-handle" ).css(
      width: handleSize,
      "margin-left": -handleSize / 2
    );
    handleHelper.width( "" ).width( scrollbar.width() - handleSize );


  resetValue = ->
    return true if scrollContent.width() <= scrollPane.width()
    remainder = scrollPane.width() - scrollContent.width()
    leftVal = scrollContent.css( "margin-left" ) == "auto" ? 0 : parseInt( scrollContent.css( "margin-left" ) )
    percentage = Math.round( leftVal / remainder * 100 )
    scrollbar.slider( "value", percentage )
  
  reflowContent = ->
    return true if scrollContent.width() <= scrollPane.width()
    showing = scrollContent.width() + parseInt( scrollContent.css( "margin-left" ), 10 )
    gap = scrollPane.width() - showing;
    if gap > 0
      scrollContent.css( "margin-left", parseInt( scrollContent.css( "margin-left" ), 10 ) + gap )

   

  window.ObserveResize.getInstance().add('f-p-f', ->
    resetValue();
    sizeScrollbar();
    reflowContent();
  )

  setTimeout( sizeScrollbar, 10 );