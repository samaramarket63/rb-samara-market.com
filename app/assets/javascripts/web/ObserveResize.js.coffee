class window.ObserveResize
  _instance = undefined
  @getInstance: -> 
    _instance ?= new _Singleton

class _Singleton

  _task: {}

  _timer: null

  constructor: ->
      @_init()

  _init: ()->
    $(window).resize(=>
      clearTimeout(@_timer)
      @_timer = setTimeout(=>
        for i of @_task
          @_task[i]()
      ,300)
    )

  add: (key, task)->
    @_task[key] = task
    @_task[key]()

  pop: (key)-> 
    @_task[key] = -> 