window.Helper = {
  is_auth: ()->
    xhr = $.get(window.HelperUrl.user_is_uath())

    return xhr

  style_width_px_to_i: (css)->
    try
      width = css.substring(0, (css.length-2))
      width = ($.trim(width)) * 1
    catch e
      width = 0
    
    width

  loader: ->
   
    box = document.createElement('div')
    box.className = 'loader'

    img = document.createElement('img')
    img.src = "/assets/loader.gif"

    box.appendChild(img)

    box
  el_is_see: (b)->
    (window._$.window.scrollTop()+window._$.window.height()) > b.offset().top

  container_opacity: (show)->
    if show == true
      window.SmDropDown.getInstance().close_all()
      $('.mask_white').remove()
      return true
    return true if $('.mask_white').length > 0

    box = document.createElement('div')
    box.className = 'mask_white'

    box.style.top = (window.Helper.container_offset.top-18)+'px'

    document.body.appendChild(box)
    $box = $(box)
    $(box).click((e)->
      window.SmDropDown.getInstance().close_all()
      e.preventDefault();
      $(this).remove()
    )
    $box

  only_auth: (callback)->
    window.Helper.is_auth()
      .success((e, data, status)->
        callback()
      )
      .error((e, data, status)->
        if e.status == 401
          panel = document.getElementById('authWindow')
          if panel
            $(panel).modal()
          else
            box = new window.View('auth')
            el = box.render({}, window._$.body, 'append')
            $(el).modal()
        else
          Helper.message('error',window.Messages.error.auth.system)
      ) 

    return false;  

  resize_block_products_block: ()->
    container = $('.block_products_block')
    if container.length > 0
      c_width = container.width()-20
      blocks = Math.floor(c_width/260)
      container.find('.product').css('width',Math.floor(240+(c_width-(blocks*260))/blocks))


  resize_block_category_products: ()->
    container = $('.category_box_x')
    if container.length > 0
      c_width = container.width()-20
      blocks = Math.floor(c_width/235)
      for i in container
        box = $(i)
        products = box.find('.product')
        if products.length > blocks
          products.width((c_width/blocks)-20)
        else
          product_width_auto = ((c_width/blocks)-20)
          products.width(product_width_auto > 300 ? 300 : product_width_auto)
  message: (type, message, title, timeout=5000)->
    container = $('div#system-message')
    try
      if typeof message == 'object'
        title = message[0]
        message = message[1]
        

      if container.length == 0
        container = $('<div id="system-message"></div>').appendTo($('body'));


      box = new window.View('message')

      el = box.render({type: type, message: message, title: title}, $('#system-message'), 'prependTo')
      if timeout
        setTimeout(->
          $(el).alert('close').fadeOut()
        ,5000)
    catch e
      alert('Ошибка уведомлений')
    

  number_format: ( number, separator=" ")->
    number = parseFloat(number)
    (number+'').replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);
  
  standart_page: ()->
    window.ObserveResize.getInstance().add(
      'h_n_f_c',
      ->
        if(window._$.container.width() > 1047)
          CatalogNavigation.container_show(false)
        else
          CatalogNavigation.container_hide(true)
        )
  show_confirm: (message, type='primary')->
    box = new window.View('confirm')
    el = box.render({message: message, type: type}, window._$.body, 'append')

    $el = $(el)
    $el.modal()
}

window.HelperUrl = {
  product: (link)->
    '/product/'+link+'.html'

  cart: ->
    '/cart.html'

  cart_set_count: ->
    '/cart/set_count'

  user_is_uath: ->
    '/user/is_auth'  

  delivery_methods_by_city: (city_id)->
    '/orders/delivery_from_city/'+city_id+'.json' 

  payment_methods_for_delivery: (delivery_id)->
    '/orders/payment_for_delivery/'+delivery_id+'.json'  

  orders: (id=null, format='html')->
    '/orders.html' 
  data_verified: (field,token)->
    url = '/user/data_verified/'+field
    url += '/'+token if token
    url
  add_reviews: (product_id)->
    '/product/'+product_id+'/reviews'

  reviews_is_edit: (product_id)->
    '/product/'+product_id+'/reviews/is_edit'
  public_offer: (format='html')->
    '/public_offer.'+format
  privacy_policy: (format='html')->
    '/privacy_policy.'+format
  recovery_password: ()->
    '/recovery_password'
  featured_products_footer: ()->
    '/products/recommended'
}

$ ->
  window.Helper.container_offset = window._$.container.offset()