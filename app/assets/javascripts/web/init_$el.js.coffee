$ ->
  $_ById = (id)->
    $(document.getElementById(id))

  window._$ =
    navbar:           $_ById('navbar')
    body:             $('body')
    catalog_open:     $_ById('catalog_open')
    container:        $_ById('container')
    window:           $(window)
    docoment:         $(document)
    featured_products_footer:  $_ById('featured-products-footer')