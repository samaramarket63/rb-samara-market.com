class window.SmDropDown
  _instance = undefined
  @getInstance: -> 
    _instance ?= new _Singleton

class _Singleton

  buttons: {}

  registr: (button, callback_open, callback_close) ->
    if button.length > 0 and button.attr('href')
      @buttons[button.attr('href')] = 
        button: button
        window: $(button.attr('href'))
        callback_open: callback_open
        callback_close: callback_close
      @_events @buttons[button.attr('href')]

  close_all: ->
    for k,v of @buttons
      @_winHide(v)

  _events: (button)->
    button.button.on 'click', ->
      SmDropDown.getInstance()._winToggle(this.getAttribute('href'))
      return false;
  _winShow: (button) ->

    button.window.show()
    
    if button.callback_open
      button.callback_open(button)

    b = window.Helper.container_opacity()

  _winHide: (button) ->
    return true if @window_is_hide(button.window)

    button.window.hide()
    if button.callback_close
      button.callback_close(button)

  _winToggle: (id_button) ->
    if @buttons[id_button]
      if @window_is_hide(@buttons[id_button].window)
        for w in @buttons
          @_winHide(w)

        @_winShow(@buttons[id_button])
      else
        @_winHide(@buttons[id_button])
        window.Helper.container_opacity(true)

  window_is_hide: (win)->
    win.css('display') == 'none'