class window.Places
  _instance = undefined
  @getInstance: -> 
    _instance ?= new _Singleton

class _Singleton

  _places: []

  init_places: (p)->
    @_places = p

  cities_from: (id_region)->
    id_region = parseInt(id_region)
    for i in @_places
      return i.cities if i.id == id_region
    []

  city_id: (name)->
    for r in @_places
      for c in r.cities
        return c.id if c.name == name
    null


  city_id_from_region_id: (city_name, region_id)->
    region_id = region_id*1
    for c in @cities_from(region_id)
      return c.id if c.name == city_name
    null

  city_name: (city_id, region_id)->
    city_id = parseInt(city_id)
    if region_id
      region_id = parseInt(region_id)
      region = @region_by_id(region_id)
      if region
        for c in region.cities
          return c.name if c.id == city_id
    else
      for r in @_places
        for c in r.cities
          return c.name if c.id == city_id
    null


  region_id: (name)->
    for r in @_places
      return r.id if r.name == name
    null

  region_by_id:(id)->
    id = parseInt(id)
    for i in @_places
      return i if i.id == id
    null

  region_id_by_city_id:(city_id)->
    for r in @_places 
      for c in r.cities
        return r.id if c.id == city_id
    null


  get_regions: ()->
    @_places



