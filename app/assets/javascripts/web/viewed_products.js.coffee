$ ->
  window.ObserveScroll.getInstance().add('featured_products_footer', ->

    if window.Helper.el_is_see(window._$.featured_products_footer) and window._$.featured_products_footer.is(':visible')

      $.post(window.HelperUrl.featured_products_footer(), ->
        window.ObserveScroll.getInstance().pop('featured_products_footer')
      )
      .done((data, textStatus, jqXHR)->
        try
        
          data = $.parseJSON(data)
          if data.length == 0
            window._$.featured_products_footer.hide()
          box = new window.View('products_simple')
          el = box.el({products: data})
          window._$.featured_products_footer.find('.block_products_block').html(el)
          window.Helper.slider(window._$.featured_products_footer, 235)
        catch e
          console.log e
          window._$.featured_products_footer.hide()

  
      )
      .fail((jqXHR, status, textStatus)->
        window._$.featured_products_footer.hide()
      )
  )