class window.CatalogNavigation
  _navbar: null
  _catalog_open: null
  _sub_container: null
  _container: null
  _activ_sub: null
  _ancors: null
  _sub_ancors: {}
  _sub_categories: {}

  _timers:
    container: null
    sub_container: null

  constructor: (nav_hide = true) ->
    @_navbar = window._$.navbar
    @_catalog_open = window._$.catalog_open
    @_container = document.getElementById('nav-catalog-browse')
    @_sub_container = document.getElementById('nav_subcats')
    if nav_hide
      @_events_for_button()
    else
      @container_show()

    ancors = document.getElementById('nav_cats')
      .getElementsByTagName('ul')[0]
      .getElementsByTagName('a')

    for i in ancors
      id = i.getAttribute('data-id')
      @_sub_ancors[id] = i
      @_sub_categories[id] = document.getElementById('sub_category_'+id)


    @_container.style.top = (@_navbar.position().top+@_navbar.height())

    @_ancors = $(ancors)
    @_events_for_container()

  toggleCantainer: ->
    if @_container.className == 'open'
      @container_hide()
    else
      @container_show()

  container_show: ->
    @_navbar.addClass('catalog_open')
    @_container.className = 'open'

  container_hide: ->
    @_navbar.removeClass('catalog_open')
    @_container.className = ''

  toggleSub: (id)->

    if id == @_activ_sub
      @sub_hide(id) 
    else
      @sub_show(id)


  sub_show: (id)->
    clearTimeout  @_timers.sub_container
    if @_activ_sub != null
      @sub_hide(@_activ_sub)

    if @_sub_container.style.display != 'block'
      @_sub_container.style.display = 'block'

    width = @_sub_ancors[id].getAttribute('data-width')

    if width 
      if (width+'px') != @_sub_container.style.width 
        @_sub_container.style.width = (width+'px')
    else
      @_sub_container.style.width = ''

    @_sub_ancors[id].className = 'active'
    @_activ_sub = id
    @_sub_categories[id].style.display = "block"

  sub_hide: (id)->
    clearTimeout  @_timers.sub_container
    @_timers.sub_container = setTimeout =>
      @_sub_container.style.display = 'none'
    ,200
    @_sub_ancors[id].className = ''
    @_activ_sub = null
    @_sub_categories[id].style.display = "none"

  _events_for_button: ->
    @_catalog_open.on 'click', =>
      @toggleCantainer()


    @_catalog_open.on 'mouseleave', =>
      @_timers.container = setTimeout =>
        @container_hide()
      ,300

    @_catalog_open.on 'mouseenter', =>
      clearTimeout @_timers.container 
    
    $_container = $(@_container)

    $_container.on 'mouseleave', =>
      @_timers.container = setTimeout =>
        @container_hide()
      ,300

    $_container.on 'mouseenter', =>
      clearTimeout @_timers.container

    @

  _events_for_container: ->
    nav = @

    @_ancors.on 'mouseenter', ->
      this['id'] = this.getAttribute('data-id')

      if nav._activ_sub == this['id']
        clearTimeout nav._timers['cat_'+this['id']]
        return true


      nav._timers['cat_'+this['id']] = setTimeout =>
        nav.sub_show(this['id'])
      ,300  


    @_ancors.on 'mouseleave', ->
      this['id'] = this.getAttribute('data-id')

      if nav._activ_sub != this['id']
        clearTimeout nav._timers['cat_'+this['id']]
        return true

      nav._timers['cat_'+this['id']] = setTimeout =>
        nav.sub_hide(this['id'])
      ,300  

    $('#nav_subcats .sub_category').on 'mouseenter', ->
          clearTimeout nav._timers['cat_'+this.getAttribute('data-id')]

    $('#nav_subcats .sub_category').on 'mouseleave', ->
      nav._timers['cat_'+this.getAttribute('data-id')] = setTimeout =>
        nav.sub_hide(this.getAttribute('data-id'))
      ,300 

    @_ancors.on 'click', ->
      event.preventDefault()
      nav.toggleSub(this.getAttribute('data-id'))

    @





$ ->
  window.CatalogNavigation = new CatalogNavigation(config.navigation)