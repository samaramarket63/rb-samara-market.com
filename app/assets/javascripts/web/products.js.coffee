$ ->

  $('#product_card .images_box a').on 'click', ->
    id = 'p_i'+(this.getAttribute('title').SmHashCode())
    m = $('#'+id)
    if m.length == 1
      img_open = m.find('.image img')
      if this.getAttribute('href') != img_open.attr('src')
        img_open.attr('src', this.getAttribute('href'))
      m.modal('show')
    else
      tpl = window.JST['web/templates/w_images_of_products'](
        id: id
        img: this.getAttribute('href')
        title: this.getAttribute('title')
        thumb: $('#product_card .images_box .thumbnails').html()
      )
      t = $(tpl)
      t
        .on 'click', '.thumbnail', ->
          t.find('.image img').attr('src', this.getAttribute('href'))
          return false;
        .modal()

    return false