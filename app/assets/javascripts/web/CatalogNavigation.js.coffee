class window.CatalogNavigation
  _navbar: null
  _catalog_open: null
  _container: null
  _activ_sub: null
  _ancors: null
  _sub_ancors: {}
  _sub_categories: {}
  _default_width: 200

  _timers:
    container: null
    sub_container: null

  constructor: (nav_hide = true) ->
    @_navbar = window._$.navbar
    @_catalog_open = window._$.catalog_open
    @_container = document.getElementById('nav-browse-layout')

    if nav_hide
      @_events_for_button()
    else
      @container_show()

    ancors = document.getElementById('nav-browse-categories')
      .getElementsByTagName('ul')[0]
      .getElementsByTagName('a')

    for i in ancors
      id = i.getAttribute('data-id')
      @_sub_ancors[id] = i
      @_sub_categories[id] = document.getElementById('sub-category'+id)


    @_container.style.top = (@_navbar.offset().top+@_navbar.height())

    @_ancors = $(ancors)
    @_events_for_container()

  toggleCantainer: ->
    if @_container.getAttribute('data-open') == 'yes'
      @container_hide()
    else
      @container_show()

  container_show: (off_e=null)->
    if off_e==false
      @_events_for_button_off()
    else if off_e==true
      @_events_for_button_off()
      @_events_for_button()
    
    @_navbar.addClass('catalog_open')
    @_container.setAttribute('data-open','yes')
    @_container.style.display = 'block'
    @

  container_hide: (off_e=null) ->
    if off_e==false
      @_events_for_button_off()
    else if off_e==true
      @_events_for_button_off()
      @_events_for_button()
      
    @_navbar.removeClass('catalog_open')
    @_container.setAttribute('data-open','no')
    @_container.style.display = 'none'
    @

  toggleSub: (id)->

    if id == @_activ_sub
      @sub_hide(id) 
    else
      @sub_show(id)


  sub_show: (id)->
    clearTimeout  @_timers.sub_container
    if @_activ_sub != null
      @sub_hide(@_activ_sub)

    if @_container.className == 'sm-drop-down'
      @_container.className = 'sm-drop-down active'
      window.Helper.container_opacity()
    
    @_sub_ancors[id].className = 'active'
    @_activ_sub = id
    @_sub_categories[id].style.display = "block"



  sub_hide: (id)->
    clearTimeout  @_timers.sub_container
    @_timers.sub_container = setTimeout =>
      @_container.className = 'sm-drop-down'
      window.Helper.container_opacity(true)
    ,200
    @_activ_sub = null
    @_sub_ancors[id].className = ''
    @_sub_categories[id].style.display = "none"

  _events_for_button_off: ->
    @_catalog_open.off 'click'

    @_catalog_open.off 'mouseleave'

    @_catalog_open.off 'mouseenter'
    
    $_container = $(@_container)

    $_container.off 'mouseleave'

    $_container.off 'mouseenter'

  _events_for_button: ->
    @_catalog_open.on 'click', =>
      @toggleCantainer()



    @_catalog_open.on 'mouseleave', =>
      @_timers.container = setTimeout =>
        @container_hide()
      ,300

    @_catalog_open.on 'mouseenter', =>
      clearTimeout @_timers.container 
      if @_container.getAttribute('data-open') == 'no'
        @container_show()
    
    $_container = $(@_container)

    $_container.on 'mouseleave', =>
      @_timers.container = setTimeout =>
        @container_hide()
      ,300

    $_container.on 'mouseenter', =>
      clearTimeout @_timers.container

    @

  _events_for_container: ->
    nav = @

    @_ancors.on 'mouseenter', ->

      this['id'] = this.getAttribute('data-id')
      return true if parseInt(this['id']) == 0
      if nav._activ_sub == this['id']
        clearTimeout nav._timers['cat_'+this['id']]
        return true


      nav._timers['cat_'+this['id']] = setTimeout =>
        nav.sub_show(this['id'])
      ,300  


    @_ancors.on 'mouseleave', ->
      this['id'] = this.getAttribute('data-id')
      return true if parseInt(this['id']) == 0
      if nav._activ_sub != this['id']
        clearTimeout nav._timers['cat_'+this['id']]
        return true

      nav._timers['cat_'+this['id']] = setTimeout =>
        nav.sub_hide(this['id'])
      ,300  

    $('#nav-browse-sub-categories .sub-category').on 'mouseenter', ->
          clearTimeout nav._timers['cat_'+this.getAttribute('data-id')]

    $('#nav-browse-sub-categories .sub-category').on 'mouseleave', ->
      nav._timers['cat_'+this.getAttribute('data-id')] = setTimeout =>
        nav.sub_hide(this.getAttribute('data-id'))
      ,300 

    @_ancors.on 'click', (event)->
      return true if parseInt(this.getAttribute('data-id')) == 0
      event.preventDefault()
      nav.toggleSub(this.getAttribute('data-id'))

    @





$ ->
  window.CatalogNavigation = new CatalogNavigation(config.navigation)