class window.View
  tpl: null;
  path: 'web/templates/';


  constructor: (tpl) ->
    @tpl = window.JST[@path+tpl]
    @

  getTpl: ()->

  el: (data)->
    if @tpl == null
      return null

    @tpl(data)

  render: (data, container, type=null)->

    if (@tpl == null) or !(el = @el(data)) or container == undefined or container.lenght == 0

      return false;
    el = $(el)
    switch type
      when 'append' then container.append(el);
      when 'appendTo' then  el.appendTo(container);
      when 'prependTo' then el.prependTo(container);
      when 'prepend' then container.prepend(el);
      else container.html(el)

    el