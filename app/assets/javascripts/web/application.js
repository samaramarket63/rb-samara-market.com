// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require ./implementations
//= require ./init_$el
//= require ./jquery-ujs
//= require ./text_messages
//= require_tree ./templates
//= require ./cart
//= require ./Places
//= require ./ordering
//= require bootstrap-transition
//= require bootstrap-dropdown
//= require bootstrap-tooltip
//= require bootstrap-alert
//= require bootstrap-button
//= require bootstrap-affix
//= require bootstrap-modal
//= require bootstrap-tab
//= require bootstrap-collapse
//= require bootstrap-popover
//= require ./plugins/jquery.ui.slider
//= require ./plugins/jquery.rating
//= require ./plugins/typeahead
//= require ./plugins/autocomplete_street
//= require ./View
//= require ./plugins/carusel
//= require ./helpers
//= require ./ObserveResize
//= require ./ObserveScroll
//= require ./add_helper_slider
//= require ./SmDropDown
//= require ./products
//= require ./init
//= require ./CatalogNavigation
//= require ./auth
//= require ./viewed_products
