$ ->
    window._$.body.on('click', '#authWindow ul.link-social a, #authNotWindow ul.link-social a', (e)->

        width = 600; height = 400;
        left = (window.screen.width / 2) - (width / 2);
        top = (window.screen.height / 2) - (2 * height / 3);
        features = 'menubar=no,toolbar=no,status=no,width=' + width + ',height=' + height + ',toolbar=no,left=' + left + ',top=' + top
        window.loginWindow = window.open(this.getAttribute('href'), '_blank', features);
        window.loginWindow.focus()
        e.preventDefault();
        return false;
    )

    window._$.body.on('ajax:before', '#authWindow form, #authNotWindow form', ()->
        $form = $(this)
        $form.find('button').button('loading')

        $form.find('.control-group').removeClass('error').find('span.help-inline').remove()
    )

    window._$.body.on('ajax:complete', '#authWindow form, #authNotWindow form', (xhr, status)->
        $form = $(this)
        $form.find('button').button('reset')
    )

    window._$.body.on('ajax:error', '#authWindow form, #authNotWindow form', (e, data, status)->
        $form = $(this)
        $form.find('.control-group.captcha').remove()
        $form.find('span.help-inline').remove()
        if data.status == 406
            Helper.message('error', 'Возможно у вас отключены Cookie','Ошибка регистрации')
        
        else if data.status == 401 or data.status == 423 or data.status == 424
            helper = $form.find('.helper')
            if data.status == 401 or data.status == 423
                Helper.message('error', 'Ошибка в логине или пароле','Не авторизован',10000)
                helper.prepend('<span class="help-inline" style="color:#df0505">Ошибка в логине или пароле </span>')
            else
                Helper.message('error', 'Значение капчи неверно','Не авторизован',10000)
                helper.prepend('<span class="help-inline" style="color:#df0505">Значение капчи введено неверн</span>')
            if data.status == 423 or data.status == 424
                box = new window.View('captcha')
                try
                    data = jQuery.parseJSON(data.responseText)
                    console.log data
                    el = box.render({uid: data.uid, id: data.id, url: data.url}, helper, 'prepend')
                catch e
                    helper.prepend('<span class="help-inline" style="color:#df0505">Авторизация не может быть завершена, каптча не готова, обратитесь в тех. поддержку</span>')
                


        else
            try
                xhr = jQuery.parseJSON(data.responseText)
                if jQuery.isPlainObject(xhr) and data.status == 422
                    for k,v of xhr
                        if k == 'uid'
                            k = 'phone'
                        i_box = $form.find('input[name="'+k+'"]').parent().parent().addClass('error')
                        if i_box
                            for error in v
                                i_box.prepend('<span class="help-inline">'+error+'; </span>')
                else
                    Helper.message('error', window.Messages.error.auth.system)
            catch e
                console.log(e)
                Helper.message('error', window.Messages.error.auth.system)

    )

    window._$.body.on('ajax:success', '#authWindow form, #authNotWindow form', (e, data, status)->
        $form = $(this)
        $form.find('button').button('reset')
        $form.find('.control-group.captcha').remove()
        if window.redirect_url
            window.location = window.redirect_url
        else
            window.location.reload()
    )