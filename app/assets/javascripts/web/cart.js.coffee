class window.Cart
  _instance = undefined
  @getInstance: -> 
    _instance ?= new _Singleton

class _Singleton


  _products: {}

  _render: false

  _render_counts: null

  box_preview: null

  box_pre_cart: null

  constructor: ->
    @products_sum
    for p in window.PRODUCTS_IN_CART
      @_products[p.id] = {
        id:           p.id
        uid:          p.uid
        name:         p.name
        count:        p.count
        price:        p.real_price
        link_rewrite: p.link_rewrite
        discount:      p.sum_discount
        status:       ((p.status*1) == 30)
      }
    @box_pre_cart = $('header .pre-cart')

  get: (id_product)->
    @_products[id_product]

  getAll: (id_product)->
    @_products
  render_pre_cart: ()->
    count = @count()
    if count == 0
      @box_pre_cart.find('.start-order a').hide()
    else if count == 1
      @box_pre_cart.find('.start-order a').show()

    @box_pre_cart.find('li.products span').text(count)
    @box_pre_cart.find('li.sum span').text(window.Helper.number_format(@sum())+' руб.')
  render: (win)->
    if @_render == false
      @box_preview = win
      @_render = new window.View('cart_preview')

    @_render.render({products: @_products}, @box_preview)
  re_render: ()->
    @render_pre_cart()
    if @_render != false
      @render(@box_preview)

  find: (field, value)->
    arr = []
    for id, p of @_products
      arr.push(p) if p[field] == value
    return arr
  count: ()->
    Object.keys(@_products).length

  sum: ()->
    @products_sum  if @products_sum  != null
    @products_sum  = 0
    for id, p of @_products
      if p.status
        @products_sum  += (p.price*p.count)
    @products_sum 

  render_win_count: (button)->
    button = $(button)

    @_render_counts || = @init_cart_page()

    p = @find('uid', button.data('uid'))

    if p.length < 1
      window.location.reload();


    bo = button.offset()

    win = $(@_render_counts.render({id: button.data('uid'), count: p[0].count, top:bo.top, left:bo.left },$('body'),'append'))

    input_count = win.find('input.count')

    win.find('button.plus').click(->
      input_count.val((input_count.val()*1)+1)
    )
    win.find('button.minus').click(->
      input_count.val((input_count.val()*1)-1)
    )

    win.modal({backdrop: false})

  init_cart_event: ()->
    cart = $('#cart_list')
    cart_selected_all = cart.find('thead th.select input')
    actions_block = $('#cart_list_wrap .begin_order .action')
    obj_cart = @

    cart.find('tr.line td').click((event)->
      $this = $(this)
      checkbox = $this.parent().find('input:first')


      if event.target == this or event.target == checkbox[0]
              
        if cart_selected_all.is(":checked")
          cart_selected_all.prop("checked",false)

        $this.parent().toggleClass('selected')

        if event.target.nodeName.toUpperCase() != 'INPUT'
          checkbox.prop("checked", !checkbox.is(":checked"))

        selected = cart.find('td.select input:checked')

        if selected.length > 0
          unless actions_block.is(':visible')
            actions_block.show()
          actions_block.find('.count').text(selected.length)
        else
          if actions_block.is(':visible')
            actions_block.hide()
    );

    cart_selected_all.click((event)->

      $this = $(this)
      if $this.is(":checked")
        cart.find('tr.line').addClass('selected').find('td.select input').prop("checked",true) 
        actions_block.show().find('.count').text(cart.find('tr.line').length)    
      else
        cart.find('tr.line').removeClass('selected').find('td.select input').prop("checked",false) 
        actions_block.hide()

    )

    button_delete_products = actions_block.find('a[data-action="delete"]')

    button_delete_products.on('ajax:before', (e)->
      $this = $(this)
      $this.button('loading')
      $this.data('params', cart.find('td.select input:checked').serialize())
    )

    button_delete_products.on('ajax:error', (e, data, status)->
      Helper.message('error', window.Messages.error.cart.system_edit_abort)
    )
    button_delete_products.on('ajax:success', (e, data, status)->
      for i in data
        cart.find('tr.line[data-uid="'+i+'"]').remove()
        obj_cart.pop_by_uid(i)

      obj_cart.re_render()
      cart.find('tr.total td.sum').html('<span>'+window.Helper.number_format(obj_cart.sum())+' руб</span>')
      actions_block.hide()
      cart_selected_all.prop("checked",false) 
    )
    button_delete_products.on('ajax:complete', (e, data, status)->
      button_delete_products.button('reset')
    )

    not_ready_actions = $('#products_not_ready table.products td.actions')
    not_ready_actions_delete = not_ready_actions.find('a.delete')

    not_ready_actions_delete.on('ajax:before', (e)->
      $(this).button('loading')
    )
    not_ready_actions_delete.on('ajax:success', (e, data, status)->
      $(this).parent().parent().remove()
      for i in data
        obj_cart.pop_by_uid(i)
      obj_cart.re_render()
    )
    not_ready_actions_delete.on('ajax:error', (e, data, status)->
      Helper.message('error', window.Messages.error.cart.system_edit_abort)
    )
    not_ready_actions_delete.on('ajax:complete', (e, data, status)->
      $(this).button('reset')
    )
  init_cart_page: ()->

    $('body').on('ajax:success', 'form.cart_set_count', (e, data, status)->
      
      form = $('#cart_list')
      line = form.find('tr[data-uid="'+data.id+'"]')

      cart = window.Cart.getInstance()
      window.ww = form
      p = cart.find('uid',data.id)
      
      if p.length > 0
        p = p[0]
        if data.count > 0
          p.count = data.count
          line.find('td.count a').text(p.count)
          line.find('td.sum').html('<span>'+window.Helper.number_format(p.price*p.count)+' руб</span>')
          cart.re_render()
        else
          delete(cart._products[p.id]) 
          cart.re_render()
          line.remove()
        form.find('tr.total .sum').html(window.Helper.number_format(cart.sum())+' руб')

      else
        window.location.reload();

    )

    $('body').on('ajax:error', 'form.cart_set_count', (e, data, status)->
      if data.status == 417
        for i in jQuery.parseJSON(data.responseText)
          Helper.message(i.type, i.text, i.title)  
      else
        Helper.message('error', window.Messages.error.cart.system_edit_abort) 
    )
    $('body').on('ajax:before', 'form.cart_set_count', ->
      $(this).find('button.btn-primary').button('loading')
    )
    $('body').on('ajax:complete', 'form.cart_set_count', (e, data, status)->
      $(e.target).parent().remove()
    )

    new window.View('count_products')

  pop_by_id: (id)->
    p =  @find('uid', id)[0]
    if p
      p = null
      delete @_products[id]

  pop_by_uid:(uid)->
    p = @find('uid', uid)[0]
    if p
      id = p.id
      p = null
      delete @_products[id]


  add: (id, uid, name, count, price, link_rewrite, sum_discount, status) ->
    @products_sum = null
    if @get(id)
      product = @get(id)

      if price != product.price
        Helper.message(
          'info',
          'Цена на '+product.name+' изменилась и сотавляет '+price+' руб.',
          'Обратите внимание'
        )

      if count < 1
        Helper.message(
          'success',
          'Из корзины удален '+product.name,
          'Удаление из корзины'
        )
        product == null
        delete(@_products[id]) 
        @re_render()
      else if count < product.count
        Helper.message(
          'success',
          'Из корзины удалено '+(product.count-count)+ ' ед. '+product.name+' осталось '+count,
          'Удаление из корзины'
        )
        product.count = count
        @re_render()
      else if count > product.count
        Helper.message(
          'success',
          'В корзину добавлено еще '+(count-product.count)+ ' ед. '+product.name+' всего в корзине '+count,
          'Добавление в корзину'
        )
        product.count = count
        @re_render()
      else
        Helper.message(
          'block',
          'Действие не выполнено, возможно произошла ошибка',
          'Корзина'
        )

    else
      Helper.message(
        'success',
        'В корзину добавлено '+count+ ' ед. '+name,
        'Добавление в корзину'
      )
      @_products[id] = {
        id:           id
        uid:          uid
        name:         name
        count:        count
        price:        price*1
        link_rewrite: link_rewrite
        discount:      sum_discount
        status:       ((status*1) == 30)
      }
      @re_render()


$ ->
  $('body').on('ajax:before', 'form.add_to_cart', ->
    $(this).find('button').button('loading')
  )


  $('body').on('ajax:complete', 'form.add_to_cart', (e, data, status)->
    $(this).find('button').button('reset')
  )

  $('body').on('ajax:success', 'form.add_to_cart', (e, data, status)->
    cart = window.Cart.getInstance()
    cart.add(data.product.id,data.cart_product.id,data.product.name,data.cart_product.count,data.product.real_price,data.product.link_rewrite, data.product.sum_discount,data.product.status)
  )

  $('body').on('ajax:error', 'form.add_to_cart', (e, data, status)->
    if status == 'error'

      try
        xhr = jQuery.parseJSON(data.responseText)
        if typeof xhr == 'object' and data.status == 422
          for i in xhr
             Helper.message(i.type, i.text, i.title)  
        else
          Helper.message('error', window.Messages.error.cart.system_add_abort)
      catch e
        Helper.message('error', window.Messages.error.cart.system_add_abort)
  )

