$ ->
  $("[rel=tooltip]").tooltip()

  document.cookie = 'test=true; path=/';

  $_ById = (id)->
    $(document.getElementById(id))

  d = SmDropDown.getInstance()

  d.registr($('#b_cart_preview'), (b)->
    Cart.getInstance().getAll().length
    if b.window.find('.products').length == 0
      Cart.getInstance().render(b.window)

  )

  search_field = $('#search input.query')
  search_button = $('#search button[type="submit"]')
  search_field.focusin(->
    search_button.removeClass('btn-primary')
    search_button.addClass('btn-orange')
  )
  search_field.focusout(->
    search_button.removeClass('btn-orange')
    search_button.addClass('btn-primary')
  )

  $('.sm-drop-down').css('top', (window._$.navbar.offset().top+40)+'px')

  window.redirect_url = null

  $('a[data-auth]').click((e)->

    if parseInt(this.getAttribute('data-redirect')) == 0
      window.redirect_url = window.location.href
    else if this.getAttribute('href') and this.getAttribute('href') != '#'
      window.redirect_url = this.getAttribute('href')
  
    window.Helper.is_auth()
    .success((e, data, status)->
      window.location = window.redirect_url
    )
    .error((e, data, status)->
      if e.status == 401
        panel = document.getElementById('authWindow')
        if panel
          $(panel).modal()
        else
          box = new window.View('auth')
          el = box.render({}, window._$.body, 'append')
          $(el).modal()
      else
        Helper.message('error',window.Messages.error.auth.system)
    ) 

    return false;  
  )

  $('body').on('click', 'a[data-popup-content]', (e)->
    url = e.target.getAttribute('href')
    title = e.target.getAttribute('title')

    $.get(url)
      .done((data, textStatus, jqXHR)->

        box = new window.View('content_modal')
        el = box.render({title: title, content: data}, window._$.body, 'append')
        $(el).modal()
        $(el).one('hidden', ->
          $(this).remove()
        )

      )
      .fail((data, textStatus)->
        window.location = url
      )
    return false
  )
  Cart.getInstance().render_pre_cart()
