$ = jQuery

class Carusel
  
  _items: {}
  
  _buttons: {}

  _next: {}

  _container: null

  stop: false

  res_buttons: []
  res_items: []

  _active: null

  constructor: (buttons, items, container) ->
    @res_buttons = buttons
    @res_items = items
    @container = container
    @_register_events()

    for i in [0...buttons.length]
      if i == (buttons.length-1)
        @_next[buttons[i].getAttribute('data-id')] = buttons[0].getAttribute('data-id')
      else
        @_next[buttons[i].getAttribute('data-id')] = buttons[i+1].getAttribute('data-id')

      @_buttons[buttons[i].getAttribute('data-id')] = $(buttons[i])

    for item in items
      item.style.display = 'none'
      @_items[item.getAttribute('data-id')] = $(item)

    @item_show(buttons[0].getAttribute('data-id'))

    setInterval(=>
      if @stop == false
        @tick()
    , 5000)


  tick: ->
    @item_show(@_next[@_active])



  item_show: (id)->
    if @_active == id
      return true;

    if @_active != null
      @_item_hide(@_active)

    @_active = id
    @_buttons[id].addClass('active')
    @_items[id].fadeIn(600,->
      this.style.display = 'block'
    )
    true


  _item_hide: (id)->
    @_buttons[id].removeClass('active')
    @_items[id].fadeOut(600,->
      this.style.display = 'none'
    )
    true

  _register_events: ->
    c = @
    @container.on 'mouseenter', =>
      @stop = true
    @container.on 'mouseleave', =>
      @stop = false

    @res_buttons.on 'click', ->
      c.item_show this.getAttribute('data-id')





$.fn.extend

  s_carusel: (options) ->

    #settings =
    #  option1: true
    #  option2: false

    #settings = $.extend settings, options

    items = this.find('div.item')
    buttons = this.find('a.bookmark')
  
    if buttons.length > 1
      new Carusel buttons, items, this

