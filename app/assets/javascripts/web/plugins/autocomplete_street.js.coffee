$ = jQuery

$.fn.extend
  autocompleteStreet: (options) ->
    settings =
      country: null
      city: null

    settings = $.extend settings, options
    
    if $.fn.typeahead

      this.typeahead({
        source: (query, process)->
          if settings.city
            this.$element.data('city', settings.city)
          _obj = this
          if this.$element.data('city')
            query = this.$element.data('city')+', '+query
          ymaps.ready(->
            q = ymaps.geocode('Россия, '+query,{json:true,kind:'street'});
            q.then(
              (res)->
                data = []
                for i in res.GeoObjectCollection.featureMember
                  if !_obj.$element.data('city') and i.GeoObject.description
                    data.push i.GeoObject.description+', '+i.GeoObject.name
                  else
                    if i.GeoObject.metaDataProperty.GeocoderMetaData.kind == 'street' or i.GeoObject.metaDataProperty.GeocoderMetaData.kind == 'house'
                      data.push i.GeoObject.name
                return process(data)
              ,(err)->

            )
          )
        matcher: (item)->
          return item
      })
