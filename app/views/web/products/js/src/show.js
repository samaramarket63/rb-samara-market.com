$ ->

  window._$.body.on('ajax:complete', '.from_for_review', (e, xhr, status)->
    console.log(xhr)
    $form = $(this)
    $form.find('button[type="submit"]').button('reset')
  )

  window._$.body.on('ajax:success', '.from_for_review', (e, xhr, status)->
    $form = $(this)
    $form.html('<h2>Благодарим Вас за оставленный отзыв!</h2>')
    console.log $form
    console.log $form.parent()
    modal = $form.parent()
    setInterval(->
      modal.modal('hide')
    ,1200
    )
  )

  window._$.body.on('ajax:before', '.from_for_review', (e)->
    $form = $(this)
    $form.find('button[type="submit"]').button('loading')
  )

  window._$.body.on('ajax:error', '.from_for_review', (e, data, status)->
    $form = $(this)
    $form.find('.control-group.error').removeClass('error')
    $form.find('ul.errors').html('')
    if data.status == 422
      $form.find('.server-message').html('<span class="label label-important">ошибки в заполнение формы</span>')
      try 
        errors = $.parseJSON(data.responseText)
        for k,v of errors
          box = $form.find('#box_field_form_review_for_'+k)
          box.addClass('error')
          errors_box = box.find('ul.errors')
          html_errors = ''
          for err in v
            html_errors += '<li class="help-inline">'+v+'</li>'
          errors_box.html(html_errors)

      catch e
        $form.find('.server-message').html('<span class="label label-important">непредвиденная ошибка</span>')
  
    else if data.status == 417

      $form.find('.server-message').html('<span class="label label-important">необходимов ввести капчу</span>')
    
      try
        captcha = $.parseJSON(data.responseText)
        html_captcha = ''
        html_captcha += '<input type="hidden" name="captcha[uid]" value="'+captcha.uid+'">'
        html_captcha += '<input type="hidden" name="captcha[id]" value="'+captcha.id+'">' if captcha.id
        html_captcha += '<img alt="Капча" src="'+captcha.url+'" >'
        html_captcha += '<input type="text" name="captcha[value]">'
        $form.find('#box_field_form_review_for_captcha').html(html_captcha)
      catch e
        $form.find('.server-message').html('<span class="label label-important">непредвиденная ошибка</span>')
      

    else
      Helper.message('error', window.Messages.error.system_error)
      $form.find('.server-message').html('<span class="label label-important">непредвиденная ошибка</span>')
  )

  render_form_comment= (data = null)->
    
    el = SM_PRODUCT_TPL['review_window'](
      product_name: SM_PRODUCT_DATA.name,
      product_id: SM_PRODUCT_DATA.id,
      data: data
    )
    $el = $(el)
    $el.find('#box_field_form_review_for_stars input').rating()
    window._$.body.append($el)
    $el.modal()
    $el.on('hidden',->
      window.location.hash = window.location.hash.replace('add_review','')
    )

  add_review_button = $('#add_review_button')
  add_review_button.click(->
    window.location.hash = 'add_review'
    auth = window.Helper.only_auth( ->
        $.getJSON(window.HelperUrl.reviews_is_edit(SM_PRODUCT_DATA.id))
        .success((data, textStatus, jqXHR)->

          if jqXHR.status == 200
            render_form_comment(data)
            console.log data
          else
            render_form_comment()
        )
        .error( ->
          render_form_comment()
        )
    )
  )

  add_review_button.trigger('click') if window.location.hash == '#add_review'

  vote_buttons = $('#product_reviews_product_page a.vote')


  vote_buttons.on('ajax:before', (e)->
    this.style.display = 'none'
  )
  vote_buttons.on('ajax:complete', (e)->
    this.style.display = 'inline-block'
  )
  vote_buttons.on('ajax:success', (e, xhr, status)->
    console.log xhr.vote_up+'  '+xhr.vote_down
    try
      votes = xhr
      $this = $(this)
      if $this.data('action') == 'up'
        window.www = $this
        console.log $this.find('span')
        $this.find('span').text(votes.vote_up)
        $this.parent().find('a[data-action="down"] span').text(votes.vote_down)
      else
        $this.find('span').text(votes.vote_down)
        $this.parent().find('a[data-action="up"] span').text(votes.vote_up)
    catch e
      Helper.message('succes', 'Ваш голос учтён')
    
  )

  vote_buttons.on('ajax:error', (e, xhr, status)->
    if xhr.status == 409
      Helper.message('error', 'За один отзыв можно проголосовать только один раз')
    else if xhr.status == 400
      Helper.message('error', 'Нельзя голосовать за свои отзывы', 'Голос не учтён')
    else if xhr.status == 401
      Helper.message('error', 'Необходимо авторизоваться', 'Голос не учтён')
    else
      Helper.message('error', 'Cистемная ошибка', 'Голос не учтён')

  )