$O = window.$O
$ ->

  form = document.getElementById('ordering_form')

  $form = $(form)


  window.ymaps_is_ready = false  

  $O.domRender = new DomRender(form)

  $O.domRender.region()


  ymaps.ready(->
    window.ymaps_is_ready = true
    if $O.domRender.f_region.block == false and ymaps.geolocation.region
      r = window.Places.getInstance().region_id(ymaps.geolocation.region)
      if r
        $O.domRender.f_region.$.val(r).trigger('change')
  )
  $form.on('ajax:before',(e)->
    $(this).find('button[type="submit"]').button('loading')
  )
  $form.on('ajax:complete',(e, xhr, status)->
    $(this).find('button[type="submit"]').button('reset')
  )

  $form.on('ajax:success',(e, xhr, status)->
    window.location = window.HelperUrl.orders()
  )

  $form.on('ajax:error',(e, xhr, status)->
    if parseInt(xhr.status) == 422
    
      try
        $O.domRender.$form.find('.control-group.error').removeClass('error')
        $O.domRender.$form.find('.span.help-block').text('')
        errors = $.parseJSON(xhr.responseText)
        for error in errors
          for k,v of error
            k = 'address' if k == 'adds'

            if k == 'delivery'
              box = $O.domRender.$form.find('#from_delivery_methods_block .control-group')

            else if k == 'payment'
              box = $O.domRender.$form.find('#from_payment_methods_block .control-group')

            else
              field = $O.domRender.$form.find('input[name="'+k+'"],select[name="'+k+'"]')
              box = field.parents('.control-group')
            
            e_box = box.find('span.help-block')
            box.addClass('error').removeClass('success')
            
            list = ''
            for e in v 
              list += '<li>'+e+'</li>'

            e_box.html('<ul>'+list+'</ul>') 

      catch e
        window.Helper.message('error', 'Системная ошибка, попробуйте перезагрузить страницу или обратитесь в техподдержку магазина', null, 25000)
    else

      
  )

class DomRender

  form: null
  $form: null

  f_region: {
    box: null
    field: null
    block: false
    $: null
  }

  f_city: {
    box: null
    field: null
    $: null
  }

  f_delivery: {
    box: null
  }

  f_address: {
    value: null
    block: false
  }

  f_payment: {
    box: null
  }
  f_postcode: {
    value: null
    block: false
  }
  f_other_fields: {
    box: null
  }

  fields: {
    payment: []
    delivery: []
  }
  address_list: null
  default_address: null

  _delivery: null

  constructor: (form)->
    @form = form
    @$form = $(form)
    @_delivery = new Delivery(this)
    address_list = $('#ordering_form_address_id') 

    @address_list = address_list if address_list.length > 0 

    if @address_list
      @address_list.change((e)=>
        address_id = parseInt(@address_list.val())
        address = null
        if address_id > 0
          for i in $O.UserAddresses
            @default_address = i if i.id == address_id

          if @default_address
            region_id = Places.getInstance().region_id_by_city_id(@default_address.city_id)
            if region_id
              if @default_address.postcode
                @f_postcode.value = @default_address.postcode
                @f_postcode.block = true
              if @default_address.adds
                @f_address.value = @default_address.adds
                @f_address.block = true
              @f_region.$.val(region_id).change().attr('disabled', '')

          
          return true
        else
          @f_postcode.block = false
          @f_address.block = false
          @$form.find('input:disabled, select:disabled').removeAttr('disabled')
      )


  region: ->
    el = document.createElement('div')
    el.id = 'ordering_region_box'
    el.innerHTML = sm.TPL['form_field_region']({regions: Places.getInstance().get_regions()})
    @form.appendChild(el)
    @f_region.box = el
    @f_region.field = el.getElementsByTagName('select')[0]
    @f_region.$ = $(@f_region.field)
    @$form.find('.loader').remove()

    that = @

    @f_region.$.one 'change', (e)->
      that.f_region.block = true

    @f_region.$.on 'change', (e)->
      if that.f_city.box
        that.form.removeChild(that.f_city.box)
        that.remove_delivery()
      that.city()

    @

  city: ->
    el = document.createElement('div')
    el.id = 'ordering_city_box'

    default_id = null

    default_id = @default_address.city_id if @default_address

    if ymaps_is_ready and ymaps.geolocation.city and default_id == null
    
      default_id = Places.getInstance().city_id_from_region_id(ymaps.geolocation.city, @f_region.$.val())

    el.innerHTML = sm.TPL['form_field_city']({
      cities: Places.getInstance().cities_from(@f_region.$.val())
      default: default_id
      }
    )
    @form.appendChild(el)
    @f_city.box = el
    @f_city.field = el.getElementsByTagName('select')[0]
    @f_city.$ = $(@f_city.field)

    that = @

    @f_city.$.change((e)->
      that.remove_delivery()
      return false if this.value == '0'
      that.form.appendChild(window.Helper.loader())
      $.getJSON(window.HelperUrl.delivery_methods_by_city(this.value))
      .success((data, textStatus, xhr)->
        that.delivery(data)
      )
      .error((xhr, textStatus, error)->
        window.Helper.message('error', error+'. Методы доставки не загружены, обратитесь в тех. поддержку', 'Системная ошибка')
      )
      .complete((xhr, textStatus)->
        that.$form.find('.loader').remove()
      )


    )

    if default_id
      @f_city.$.trigger('change')

    @f_city.$.attr('disabled', '') if @default_address

  delivery: (methods)->
    that = @
    
    el = document.createElement('div')
    el.id = 'ordering_delivery_box'

    el.innerHTML = sm.TPL['form_field_delivery'](methods: methods)
    
    @f_delivery.box = el 

    @form.appendChild(el)

    trs = el.getElementsByTagName('tbody')[0]
    trs = trs.getElementsByTagName('tr') if trs
    if trs
      $trs = $(trs)
      $trs.popover({trigger:'hover'})

      $trs.find('input[name="delivery"]').change(->
        that.form.appendChild(window.Helper.loader())
        that.remove_payment()
        fields = this.getAttribute('data-fields')
        if fields
          fields = fields.split(';')
          if fields.length > 1 or (fields.length == 1 and fields[0] != '') 
            that.fields.delivery = fields
        $.getJSON(window.HelperUrl.payment_methods_for_delivery(this.value))     
        .success((data, textStatus, xhr)->
          that.payment(data)
        )
        .error((xhr, textStatus, error)->
          window.Helper.message('error', error+'. Методы оплаты не загружены, обратитесь в тех. поддержку', 'Системная ошибка')
        )
        .complete((xhr, textStatus)->
          that.$form.find('.loader').remove()
        )  
      )

      if trs.length == 1
        element = trs[0].getElementsByTagName('input')[0]
        element.checked = true
        $(element).trigger('change')

  payment: (methods)->
    that = @
    @remove_others()
    el = document.createElement('div')
    re = document.getElementById('ordering_payment_box')
    re.parentNode.removeChild(re) if re
    el.id = 'ordering_payment_box'

    el.innerHTML = sm.TPL['form_field_payment'](methods: methods)
    
    @f_payment.box = el 

    @form.appendChild(el)

    trs = el.getElementsByTagName('tbody')[0].getElementsByTagName('tr')
    $trs = $(trs)
    $trs.find('input[name="payment"]').change(->
      fields = this.getAttribute('data-fields')
      that.remove_others()
      if fields
        fields = fields.split(';')
        if fields.length > 1 or (fields.length == 1 and fields[0] != '') 
          that.fields.payment = fields
      that.show_fields()
    )

    if trs.length == 1
      element = trs[0].getElementsByTagName('input')[0]
      element.checked = true
      $(element).trigger('change')

  show_fields: ()->
    that = @
    el = document.createElement('div')
    el.id = 'ordering_others_fields_box'

    el.innerHTML = sm.TPL['form_field_others'](fields: @fields_to_param())
    
    @f_other_fields.box = el 

    @form.appendChild(el)

    fields = el.getElementsByTagName('input')

    for f in fields
      if f.getAttribute('data-rules')
        $(f).focusout((e)->
          window.$O.Validater.get(this).validate()
        )

    f_phone = document.getElementById('ordering_form_phone')

    f_address = document.getElementById('ordering_form_address')
    if f_address
      $f_address = $(f_address)
      
      if window.ymaps_is_ready 
        $f_address.autocompleteStreet({city:Places.getInstance().city_name(@f_city.$.val(), @f_region.$.val())})
      if @f_address.value
        f_address.value = @f_address.value

    if f_phone
      $f_phone = $(f_phone)
      $f_phone.keyup((e)->
        this.value = this.value.replace(/\D+/g,"")
      )

  fields_to_param: ->
    fields = {}
    arr = [].concat(@fields.delivery, @fields.payment)
    for i in arr
      unless fields[i]
        fields[i] = true


    fields


  remove_delivery: ()->
    if @f_delivery.box
      @form.removeChild(@f_delivery.box)
      @f_delivery.box = null
      @fields.delivery = []
      @remove_payment()

  remove_payment: ()->
    if @f_payment.box
      @form.removeChild(@f_payment.box)
      @fields.payment = []
      @f_payment.box = null
      @remove_others()

  remove_others: ()->
    if @f_other_fields.box
      @form.removeChild(@f_other_fields.box)
      @f_other_fields.box = null

class Delivery 
  domrender: null

  constructor: (domrender)->
    @domrender = domrender

  get_line: (id)->
    return document.getElementById('delivery_method_'+id)




class $O.Validater
  _instance = {}
  @get: (el) -> #
    _instance[el.getAttribute('id')] ?= new _Validater(el)

class _Validater
    
  rules: {}
  errors: []
  el: null

  timer: null

  box: null

  e_box: null

  constructor: (el)->
    @el = $(el)
    @box = @el.parents('.control-group')
    @e_box = @box.find('span.help-block')
    try
      @rules = jQuery.parseJSON(el.getAttribute('data-rules'))
    catch e
      console.log(e)

  required: ()->
    if @el.attr('name') == 'checkbox'
      return @el.attr("checked")=='checked'
    else
      return jQuery.trim(@el.val()) != ''

  is_valid: ()->

    @errors = []
    if @rules['required']
      @errors.push(@rules['required']['on']) unless @required()
    if @rules['min']
      @errors.push(@rules['min']['on']) unless @min(@rules['min']['value'])

    if @rules['max']
      @errors.push(@rules['max']['on']) unless @max(@rules['max']['value'])

    if @rules['eq']
      @errors.push(@rules['eq']['on']) unless @eq(@rules['eq']['value'])

    if @rules['min_length']
      @errors.push(@rules['min_length']['on']) unless @min_length(@rules['min_length']['value'], @rules['min_length']['backspace'])

    if @rules['max_length']
      @errors.push(@rules['max_length']['on']) unless @max_length(@rules['max_length']['value'], @rules['max_length']['backspace'])

    if @rules['eq_length']
      @errors.push(@rules['eq_length']['on']) unless @eq_length(@rules['eq_length']['value'], @rules['eq_length']['backspace'])

    if @rules['numeric']
      @errors.push(@rules['numeric']['on']) unless @numeric()

    if @rules['pattern']
      @errors.push(@rules['pattern']['on']) unless @pattern(@rules['pattern']['value'],@rules['pattern']['backspace'])

    @errors.length == 0


  get_errors: (is_a = false)->
    if is_a
      return @errors
    else
      return @errors.join(';')

  show_errors: ()->
    if @box
      @box.addClass('error')
      @box.removeClass('success')

    if @e_box
      errors = @get_errors(true)
      html = ""
      for e in errors
        html += "<li>"+e+"</li>"
      @e_box.html("<ul>"+html+"</ul>")

  validate: ()->
    is_valid = @is_valid()
    if is_valid
      if @box
        @box.addClass('success')
        @box.removeClass('error')

      if @e_box
        @e_box.text('')
    else
      @show_errors()

    return is_valid


  min: (rule)->
    parseInt(@el.val()) >= parseInt(rule)

  max: (rule)->
    parseInt(@el.val()) <= parseInt(rule)

  eq: (rule)->
    parseInt(@el.val()) == parseInt(rule)

  min_length: (rule, backspace = false)->
    val = @_backspaces(backspace)
    val.length >= rule


  max_length: (rule, backspace = false)->
    val = @_backspaces(backspace)
    val.length <= rule

  eq_length: (rule, backspace = false)->
    val = @_backspaces(backspace)
    val.length == (rule*1)

  numeric: ()->
    $.isNumeric(@el.val())

  pattern: (rule, backspace)->
    val = @_backspaces(backspace)
    regexp = new RegExp(rule)
    regexp.test(val)


  _backspaces: (backspace)->
    if backspace
      return @el.val()
    else
      return jQuery.trim(@el.val())




      
    
    

