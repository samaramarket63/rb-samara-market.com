xml.instruct! :xml, :version=>"1.0"
xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  xml.url do
    xml.loc(brands_url(:html))
    xml.lastmod((Time.now-1.day).strftime("%Y-%m-%dT%H:%M:%S+00:00"))
    xml.changefreq('weekly')
    xml.priority(0.6)
  end
  @brands.each do |b|
    xml.url do
      xml.loc(brand_url(b.link_rewrite, format: :html))
      xml.lastmod(b.updated_at.strftime("%Y-%m-%dT%H:%M:%S+00:00"))
      xml.changefreq('weekly')
      xml.priority(0.7)
    end
    if !b.count_visible_products.nil? and b.count_visible_products > 0
      pages = (b.count_visible_products.to_f / Product.default_per_page.to_f).ceil
      (1..pages).map do |page|
        xml.url do
          xml.loc(brand_products_url(b.link_rewrite, format: :html, page: (page>1 ? page : nil )))
          xml.lastmod(b.updated_at.strftime("%Y-%m-%dT%H:%M:%S+00:00"))
          xml.changefreq('weekly')
          xml.priority(0.6)
        end
      end
    end
  end
end