xml.instruct! :xml, :version=>"1.0"
xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  @categories.find_each do |c|
    pages = (c.products.visible.count.to_f / c.products.klass.default_per_page.to_f).ceil
    (1..pages).map do |page|
      xml.url do
        xml.loc(category_url(c.link_rewrite,  format: :html, page: (page>1 ? page : nil )))
        xml.lastmod(c.updated_at.strftime("%Y-%m-%dT%H:%M:%S+00:00"))
        xml.changefreq('weekly')
        xml.priority(0.7)
      end
    end
  end
end