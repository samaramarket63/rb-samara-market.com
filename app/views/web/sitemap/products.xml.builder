xml.instruct! :xml, :version=>"1.0"
xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  @products.find_each do |p|
    xml.url do
      xml.loc(product_url(p.link_rewrite, :html))
      xml.lastmod(p.updated_at.strftime("%Y-%m-%dT%H:%M:%S+00:00"))
      xml.changefreq('weekly')
      xml.priority(0.8)
    end
  end
end