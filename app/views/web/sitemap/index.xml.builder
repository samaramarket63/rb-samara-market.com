xml.instruct! :xml, :version=>"1.0"
xml.sitemapindex(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  @maps.map do |x|
    xml.sitemap do
      xml.loc(root_url+x+'.xml')
      xml.lastmod(Time.now.strftime("%Y-%m-%dT%H:%M:%S+00:00"))
    end
  end
end