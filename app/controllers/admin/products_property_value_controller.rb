class Admin::ProductsPropertyValueController < Admin::ApplicationController
  def create
    product = Product.find(params[:product_id])
    value = ProductsTypePropertyValues.where(value:params[:value].strip).first_or_create
    add_value = product.properties.find(params[:property_id]).values << value

    if value.errors.blank? and add_value
      Sunspot.index(product)   
      render json: value, status: :created
    else
      if value.errors.blank?
        render json: {product:['fack','fack','fack']}, status: :unprocessable_entity 
      else
        value.destroy if value.properties.empty?
        render json: value.errors, status: :unprocessable_entity 
      end
    end
  end

  def destroy
    property = Product.find(params[:product_id]).properties.find(params[:products_property_id])
    value = property.values.find(params[:id])
    property.values.delete(value)
    if value.products.count.zero?
      value.destroy
    end
    Sunspot.index(Product.find(params[:product_id]))  
    head :no_content
  end

end
