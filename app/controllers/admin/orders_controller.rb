# encoding: UTF-8
class Admin::OrdersController < Admin::ApplicationController
  include ActionView::Helpers::NumberHelper
  helpers = %w(default_price full_price)
  def index
    @orders = Order.order('id DESC')
      .select('
        orders.id, orders.status, orders.discount, orders.paid_bonus, DATE_FORMAT(orders.created_at, \'%y/%m/%d %H:%m\') as date , CONCAT_WS(\' \' ,users.first_name, users.last_name) AS user_name,
        users.phone AS phone, CONCAT_WS(\',\',regions.name, cities.name) AS country')
      .joins(:user,
       'LEFT JOIN `user_addresses` ON `user_addresses`.`id` = `orders`.`address_id`
       LEFT JOIN `cities` ON `cities`.`id` = `user_addresses`.`city_id`
       LEFT JOIN `regions` ON `regions`.`id` = `cities`.`region_id`')
    case params[:status]
      when 'active'
        @orders = @orders.open
      when 'closed'
        @orders = @orders.closed
      else
        @orders = @orders
      end
      
    render json: @orders.to_json(methods:[:bill,:text_status])
  end

  def show
    @order = Order.find(params[:id])
    render json: @order.to_json(include:
      {
        user: nil,
        payment: 
          {
            only: [:id, :name, :commission_fixed, :commission_pct]
          },
        history_status: nil,
        delivery:
          {
            only: [:id, :price, :price_for_free, :city_id, :extra_description],
            include: 
              {
                delivery_method: {
                  only: [:id,:name]
                },
                payments: nil,
                city: {
                  only: [:id, :name],
                  include: {
                    region: {
                      only: [:id, :name]
                    }
                  }
                }
            }
          },
        address: nil,
        cart: 
          {
            include: 
              {
                products_in_cart: {
                  include: {
                    product: {
                      only: [:id, :name, :status, :price],
                      methods: [:real_price, :text_status],
                      include:
                        {
                          items: {
                            only: [:id, :price, :supplier_id],
                            include: {
                              supplier: {
                                only: [:id, :name]
                              }
                            }
                          }
                        }
                    }
                  }
                }
              }
          },
      },
      methods:[:bill,:text_status, :all_status]
    )
  end

  def update
    @errors = []

    @order = Order.find params[:id]

    begin
      
      @order.transaction do

        @order.status = params[:order][:status].to_i

        @order.discount = params[:order][:discount].to_i
        @order.market_note = params[:order][:market_note]
        @order.note = params[:order][:note]
        @order.delivery_price = params[:order][:delivery_price].to_i
        @order.payment = Payment.find(params[:order][:payment_id])

        if @order.address and params[:address]
          @order.address.adds = params[:address][:adds]
          @order.address.postcode = params[:address][:postcode]
          @order.address.save!
        end

        @order.user.update_attributes!({
          first_name: params[:user][:first_name],
          last_name: params[:user][:last_name],
          middle_name: params[:user][:middle_name],
          phone: params[:user][:phone],
          phone_verified: params[:user][:phone_verified],
          email: params[:user][:email],
          email_verified:  params[:user][:email_verified]
        },as: :admin)


        products = {}
        d = params[:products_in_cart].map { |p|
          products[p[:id].to_i] = {
              count:  p[:count],
              purchase_price: p[:purchase_price],
              products_supplier_id: p[:products_supplier_id]
          }
        }

        @order.cart.products_in_cart.where(id: products.keys).each do |p|
          product = products[p.id]
          if product
            p.count = product[:count]
            p.purchase_price = product[:purchase_price]
            p.products_supplier_id = product[:products_supplier_id]
            p.save!

          end

        end

        @order.save!

      end

    rescue Exception => e
      logger.error e.message
      logger.error e.backtrace.join("\n")
      return render json: e.record.errors, status: :unprocessable_entity 
    end

    render json: {status: true}
  end

  def invoice
    @order = Order.find params[:order_id]
    @products = @order.cart.products_in_cart
    .select('
      products_in_carts.count,
      products_in_carts.sale_price AS price,
      products.name,
      products.id
      ')
    .where('products_in_carts.count > 0')
    .joins('LEFT JOIN products ON products_in_carts.product_id = products.id')

    tpl = "#{config.root}app/views/odt/invoice.odt"
    file = "#{config.root}tmp/#{@order.id}invoice.odt"
    
    @sum_of_products = 0
    @products.map {|x| @sum_of_products += (x.count.to_i * x.price.to_i) }
    @total_sum = (@sum_of_products + @order.delivery_price.to_i - @order.discount.to_i - @order.paid_bonus.to_i)
    r = render_odt tpl, file
    send_file file, 
      type: 'application/vnd.oasis.opendocument.text',
      disposition: 'inline',
      filename: "Заказ #{@order.bill} - чек.odt"
  end

  def bill
    @order = Order.find params[:order_id]
    @products = @order.cart.products_in_cart
    .select('
      products_in_carts.count,
      products_in_carts.sale_price AS price,
      products.name,
      products.id
      ')
    .where('products_in_carts.count > 0')
    .joins('LEFT JOIN products ON products_in_carts.product_id = products.id')

    tpl = "#{config.root}app/views/odt/bill.odt"
    file = "#{config.root}tmp/#{@order.id}bill.odt"
    
    @delivery_method = @order.delivery.delivery_method.name.to_s + ' ' + @order.delivery.extra_description.to_s

    @sum_of_products = 0
    @products.map {|x| @sum_of_products += (x.count.to_i * x.price.to_i) }
    @total_sum = (@sum_of_products + @order.delivery_price.to_i - @order.discount.to_i - @order.paid_bonus.to_i)
    render_odt tpl, file
    send_file file, 
      type: 'application/vnd.oasis.opendocument.text',
      disposition: 'inline',
      filename: "Заказ #{@order.bill} - накладная.odt"
  end

  def default_price(s)
    number_to_currency(s, unit:  'руб.', separator: ',', delimiter: ' ', format: "%n %u", precision: 0)
  end
  def full_price(s)
    number_to_currency(s, unit:  'руб.', separator: ',', delimiter: ' ', format: "%n %u", precision: 2)
  end

  def bil_qiwi
    @order = Order.find(params[:order_id])  
    bil = Qiwi::API.bil('+7'+@order.user.phone, @order.total.to_i, @order.bill)
    if bil['result_code'] == 0
      @order.status = 50
      @order.save
    end
    render json: bil
  end
end