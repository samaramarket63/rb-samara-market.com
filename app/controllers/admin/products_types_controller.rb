class Admin::ProductsTypesController < Admin::ApplicationController
  def index
    @types = ProductsType.all
    render json: @types
  end

  def create
    @type = ProductsType.new(params[:products_type])
    if @type.save
      render json: @type, status: :created
    else
      render json: @type.errors, status: :unprocessable_entity 
    end
  end

  def update
    @type = ProductsType.find(params[:id])
    if @type.update_attributes(params[:products_type])
      render json: {status: true}
    else
      render json: @type.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    @type = ProductsType.find(params[:id])
    if @type.destroy
      head :no_content
    else
      render json: @type.errors, status: :unprocessable_entity 
    end
  end
  
  def products
    @type = ProductsType.find(params[:products_type_id])  
    render json: @type.products
      .select('products.id, products.name, products.status, products.type_id, products.brand_id, products.price, products.discount_type_id, products.discount_value')
      .to_json(only:[:id, :name, :status, :type_id, :brand_id], methods: :real_price)
  end
end
