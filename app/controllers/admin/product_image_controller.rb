class Admin::ProductImageController < Admin::ApplicationController

  def create
    product = Product.find(params[:product_id])
    image = product.images.new()
    image.image = params[:image]
    if image.save
      render json: image, status: :created
    else
      render json: image.errors, status: :unprocessable_entity 
    end
  end 

  def cover
    product = Product.find(params[:product_id])
    img = product.images.find(params[:image_id])
    if img.cover!
      render json: img, status: :created
    else
      render json: img.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    product = Product.find(params[:product_id])
    img = product.images.find(params[:id])
    img.destroy

    if img.cover?
      product.images.first.cover! if product.images.first
    end
    head :no_content
  end
  def position
    move = Product.find(params[:product_id]).images.find(params[:image_id])
    target = Product.find(params[:product_id]).images.find(params[:move_id])
    if :before == params[:move].to_sym
      move.move_to_before_of target
    elsif :after == params[:move].to_sym
      move.move_to_after_of target
    end
    head :no_content
  end
end
