class Admin::CategoriesController < Admin::ApplicationController
  def index
    @categories = Category.includes(:adwords).tree
    render json: @categories
  end
  def show
    @category = Category.find(params[:id])
    render json: @category.to_json(include:[:adwords])
  end
  def create
    @category = Category.new(params[:category])
    if @category.save
      render json: @category, status: :created
    else
      render json: @category.errors, status: :unprocessable_entity 
    end
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category])
      render json: {status: true}
    else
      render json: @category.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    @category = Category.find(params[:id])
    if @category.id != 1
      @category.destroy
    end 
    head :no_content
  end
  def position
    @category = Category.find(params[:id])
    if params[:position] == 'before'
      @category.move_to_left_of(params[:neighbor])
    elsif params[:position] == 'after'
      @category.move_to_right_of(params[:neighbor])
    end  
    render json: {status: true}
  end
  def products
    @category = Category.find(params[:id])
    render json: @category.products
      .select('products.id, products.name, products.status, products.type_id, products.brand_id, products.price, products.discount_type_id, products.discount_value')
      .to_json(only:[:id, :name, :status, :type_id, :brand_id], methods: :real_price)
  end
end
