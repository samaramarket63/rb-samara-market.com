class Admin::ArticlesController < Admin::ApplicationController

  def index
    render json: Article.select('id, name, link_rewrite, updated_at')
  end

  def show
    render json: Article.find(params[:id])
  end

  def create
    @article = Article.new(params[:article])
    if @article.save
      render json: @article, status: :created
    else
      render json: @article.errors, status: :unprocessable_entity 
    end
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      render json: {status: true}
    else
      render json: @article.errors, status: :unprocessable_entity 
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    head :no_content
  end

  def images
    render json: @article = Article.find(params[:article_id]).images
  end

  def image_upload
    @article = Article.find(params[:article_id])
    @img = @article.images.new(image: params[:image])
    if @img.save
      render json: @img
    else
      render json: @img.errors, status: :unprocessable_entity 
    end
  end

  def image_destroy
    
  end

end