class Admin::TypePropertyController < Admin::ApplicationController
  def index
    type = ProductsType.find(params[:products_type_id])
    render json: type.properties
  end

  def create
    type = ProductsType.find(params[:products_type_id])
    @property = type.properties.new(params[:type_property])
    if @property.save
      render json: @property, status: :created
    else
      render json: @property.errors, status: :unprocessable_entity 
    end
  end

  def update
    @property = ProductsTypeProperty.find(params[:id])
    if @property.update_attributes(params[:type_property])
      render json: {status: true}
    else
      render json: @property.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    @property = ProductsTypeProperty.find(params[:id])
    @property.destroy
    head :no_content
  end
  def position
    move = ProductsTypeProperty.find(params[:id])
    target = ProductsTypeProperty.find(params[:move_id])
    if :before == params[:move].to_sym
      move.move_to_before_of target
    elsif :after == params[:move].to_sym
      move.move_to_after_of target
    end
    head :no_content
  end
end