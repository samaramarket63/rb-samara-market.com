class Admin::MainController < Admin::ApplicationController
  before_filter :auth?, except: [:auth,:login]
  def index
  end

  def login
    render layout: false  
  end

  def auth
    if request.env["HTTP_USER_AGENT"].index('Chrom') and !params[:login].blank? and !params[:password].blank? and params[:login].to_s.length == 10
      employee = Employee.where(phone: params[:login].to_i).first
      if employee and employee.auth?(params[:password])
        session[:empl] = (request.ip.to_i + 23)+employee.id
        session[:empl_token] = Digest::MD5.hexdigest(request.ip.to_s+request.env["HTTP_USER_AGENT"].to_s+employee.phone.to_s)
        redirect_to admin_root_url
        return true
      end
    end
    flash[:error] = 'Invalid password or login'
    render layout: false, template: 'admin/main/login' 
    flash[:error] = nil
  end

  def logout
    session[:empl] = nil
    session[:empl_token] = nil
    redirect_to admin_login_url
  end

  def expire_sitemap
    expire_page '/sitemap.xml'
    expire_page '/sitemap_products.xml'
    expire_page '/sitemap_categories.xml'
    expire_page '/sitemap_brands.xml'
    render text: 'TRUE'
  end
end
