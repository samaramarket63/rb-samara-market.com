class Admin::ProductsController < Admin::ApplicationController
  def index
    @products = Product.all
    render json: @products
  end

  def show
    @product = Product.includes(:images,properties:[:meta,:values]).find(params[:id])
    render json: @product.to_json(methods: :real_price, include:[:images, :items, properties: {include:[:values,:meta]}])
  end

  def create
    @product = Product.new(params[:product])
    if @product.save
      render json: @product, status: :created
    else
      render json: @product.errors, status: :unprocessable_entity 
    end
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(params[:product])
      render json: {status: true}
    else
      render json: @product.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    head :no_content
  end
  def set_categories
    @product = Product.find(params[:id])
    if params.has_key? :categories
      @product.category_ids = params[:categories]
    else
      @product.categories.clear
    end
    head :no_content
  end

  def get_categories
    @product = Product.find(params[:id])
    render json: @product.categories
  end

  def create_item
    item = Product.find(params[:id]).items.build({
      uid: params[:uid],
      price: params[:price],
      supplier_id: params[:supplier_id]
    })
    if item.save
      render json: { status: true}
    else
      render json: item.errors, status: :unprocessable_entity 
    end
  end

  def destroy_item
    item = Product.find(params[:product_id]).items.find(params[:id])
    item.destroy
    head :no_content
  end

end
