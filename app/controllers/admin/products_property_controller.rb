class Admin::ProductsPropertyController < Admin::ApplicationController
  def index

  end
  def properties_values
    values = ProductsTypePropertyValues.connection.select_all(ProductsTypePropertyValues.select([:id,:value]).arel)

    render json: values
  end
end
