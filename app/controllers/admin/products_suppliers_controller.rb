class Admin::ProductsSuppliersController < Admin::ApplicationController
  def index
    @suppliers = ProductsSupplier.all
    render json: @suppliers
  end

  def create
    @supplier = ProductsSupplier.new(params[:products_supplier])
    if @supplier.save
      render json: @supplier, status: :created
    else
      render json: @supplier.errors, status: :unprocessable_entity 
    end
  end

  def update
    @supplier = ProductsSupplier.find(params[:id])
    if @supplier.update_attributes(params[:products_supplier])
      render json: {status: true}
    else
      render json: @supplier.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    @supplier = ProductsSupplier.find(params[:id])
    @supplier.destroy
    head :no_content
  end

  def products
    @supplier = ProductsSupplier.find(params[:products_supplier_id])  
    render json: @supplier.products
      .select('products.id, products.name, products.status, products.type_id, products.brand_id')
      .to_json(only:[:id, :name, :status, :type_id, :brand_id])
  end
end
