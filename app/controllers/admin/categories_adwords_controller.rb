class Admin::CategoriesAdwordsController < Admin::ApplicationController
  def show
    adword = CategoryAdvertising.find(params[:id])
    render json: adword
  end
  def create
    adword = Category.find(params[:category_id]).adwords.new(params[:categories_adword])

    if adword.save
      render json: adword, status: :created
    else
      render json: adword.errors, status: :unprocessable_entity 
    end
  end

  def update
    adword = CategoryAdvertising.find(params[:id])
    if adword.update_attributes(params[:categories_adword])
      render json: adword
    else
      render json: adword.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    category = Category.find(params[:category_id])
    adword = category.adwords.find(params[:id])
    if adword.destroy
      category.adword_id = nil
      category.save(validate: false)
    end
    head :no_content
  end
  def position
    category = Category.find(params[:category_id])
    move = category.adwords.find(params[:adword_id])
    target = category.adwords.find(params[:move_id])
    if :before == params[:move].to_sym
      move.move_to_before_of target
    elsif :after == params[:move].to_sym
      move.move_to_after_of target
    end
    head :no_content
  end

  def image
    adword = Category.find(params[:category_id]).adwords.find(params[:adword_id])
    adword.image = params[:image]
    if adword.save
      render json: adword, status: :created
    else
      render json: adword.errors, status: :unprocessable_entity 
    end
  end

  def background
    adword = Category.find(params[:category_id]).adwords.find(params[:adword_id])
    adword.background = params[:image]
    if adword.save
      render json: adword, status: :created
    else
      render json: adword.errors, status: :unprocessable_entity 
    end

  end

  def cover
    category = Category.find(params[:category_id])
    adword = category.adwords.find(params[:adword_id])
    category.adword_id = adword.id

    if category.save!
      render json: category, status: :created
    else
      render json: category.errors, status: :unprocessable_entity 
    end
  end

end
