class Admin::ApplicationController < ActionController::Base
  include Serenity::Generator
  before_filter :auth?
  helpers = %w('auth?')

  def auth?
    if request.env["HTTP_USER_AGENT"].index('Chrom') and !session[:empl].blank? and !session[:empl_token].blank?
      id =(session[:empl].to_i - request.ip.to_i - 23)

      employee = Employee.where(id: id).first
      if employee and session[:empl_token] == Digest::MD5.hexdigest(request.ip.to_s+request.env["HTTP_USER_AGENT"].to_s+employee.phone.to_s)
        return true
      end
    end
    redirect_to admin_login_url
    return false
  end
end
