class Admin::ProductsGroupsController < Admin::ApplicationController
  def index
    @groups = ProductsGroup.all
    render json: @groups
  end
  def show
    @group = ProductsGroup.find(params[:id])
    render json: @group
  end
  def create
    @group = ProductsGroup.new(params[:products_group])
    if @group.save
      render json: @group, status: :created
    else
      render json: @group.errors, status: :unprocessable_entity 
    end
  end

  def update
    @group = ProductsGroup.find(params[:id])
    if @group.update_attributes(params[:products_group])
      render json: {status: true}
    else
      render json: @group.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    @group = ProductsGroup.find(params[:id])
    @group.destroy
    head :no_content
  end
  def products
    @group = ProductsGroup.find(params[:products_group_id])  
    render json: @group.products
  end
end
