class Admin::BrandsController < Admin::ApplicationController
  def index
    @brands = Brand.all
    render json: @brands
  end

  def create
    @brand = Brand.new(params[:brand])
    if @brand.save
      render json: @brand, status: :created
    else
      render json: @brand.errors, status: :unprocessable_entity 
    end
  end

  def update
    @brand = Brand.find(params[:id])
    if @brand.update_attributes(params[:brand])
      render json: {status: true}
    else
      render json: @brand.errors, status: :unprocessable_entity 
    end
  end
  def destroy
    @brand = Brand.find(params[:id])
    @brand.destroy
    head :no_content
  end
  def products
    @brand = Brand.find(params[:brand_id])
    render json: @brand.products
      .select('products.id, products.name, products.status, products.type_id, products.brand_id, products.price, products.discount_type_id, products.discount_value')
      .to_json(only:[:id, :name, :status, :type_id, :brand_id], methods:[:real_price])
  end


  def upload_logo
    brand = Brand.find(params[:brand_id])
    if brand.update_attribute(:logo, params[:image])
      render json: brand
    else
      render json: brand.errors, status: :unprocessable_entity 
    end
  end

  def destroy_logo
    brand = Brand.find(params[:brand_id])
    img = product.images.find(params[:id])
    img.destroy

    head :no_content
  end
end
