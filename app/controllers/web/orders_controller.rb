class Web::OrdersController < Web::ApplicationController
  before_filter :only_user, only: [:new, :create, :index, :show]
  def index
    @orders = current_session.user.orders.includes(
      :payment,
      :cart,
      address: [city:
        [:region]
      ],
      delivery: [:delivery_method]
    ).order('`orders`.`id` DESC')


  end
  def new
    if current_cart? and current_cart.goods.size > 0
  
    else
      redirect_to cart_url(:html)
    end
  
  end

  def create
    errors = []

    address = current_session.user.addresses.find_by_id(params[:address_id])

    if address
      params[:city] = address.city.id
      params[:region] = address.city.region.id
      params[:address] = address.adds if !address.adds.blank?
      params[:postcode] = address.postcode if !address.postcode.blank?
    end

    return head status: :no_content  if !current_cart? or current_cart.products.empty?
    return formError([{agreement: [t('order.errors.agreement.not')]}]) if params[:agreement].blank? or params[:agreement] != 'true'
    return formError([{region: [t('order.errors.region.not_select')]}]) if params[:region].blank?
    return formError([{city: [t('order.errors.city.not_select')]}]) if params[:city].blank?
    return formError([{delivery: [t('order.errors.delivery.not_select')]}]) if params[:delivery].blank?
    return formError([{payment: [t('order.errors.payment.not_select')]}]) if params[:payment].blank?

    city = City.select('id,region_id, name').find_by_id(params[:city])

    return formError([{city: [t('order.errors.city.not_found')]}]) if city.nil? or city.region_id != params[:region].to_i

    delivery = Delivery.select('id, city_id, delivery_method_id, price, price_for_free').find_by_id(params[:delivery])

    return formError([{delivery: [t('order.errors.delivery.not_found', city: city.name)]}]) if delivery.nil? or delivery.city_id != city.id
 
    payment = delivery.payments.find_by_id(params[:payment])

    return formError([{payment: [t('order.errors.payment.not_found')]}]) if payment.nil?

    fields = delivery.delivery_method.fields | payment.fields

    fields.each do |x|
      errors.push({x => [t('order.errors.required')]}) if params[x].blank? 
    end

    return formError(errors) if !errors.empty?

    order = Order.new
    order.user = current_session.user
    order.delivery = delivery
    order.payment = payment
    order.note = params[:note]
    order.delivery_price = current_session.cart.free_delivery?(delivery) ? 0 : delivery.price
    
    if fields.include?('address') or fields.include?('postcode')
      address = UserAddress.new({adds: params[:address], postcode: params[:postcode]},as: :user) if address.nil?
      
      address.user = current_session.user
      address.city = city

      address.valid?
    else
      address = nil
    end
    order.address = address
    ['first_name', 'last_name', 'middle_name', 'email', 'phone'].each do |x|
      current_session.user[x] = params[x] if fields.include?(x)
    end
 

    begin
      Order.transaction do
        current_session.user.cart.products_in_cart.joins(:product).select('`products_in_carts`.`id`, `products_in_carts`.`product_id`, `products_in_carts`.`cart_id`, `products`.`status` AS `status`').where('status <> 30').destroy_all
        current_session.user.cart.reload
        order.cart = current_session.cart
        current_session.user.cart = nil
        current_session.user.save! 

        current_session.cart = nil
        current_session.save!

        address.save! if !address.nil?
        order.save!
        order.cart.products.select('`products`.`id`, `products`.`price`, `products_in_carts`.`id` AS pic_id, products.discount_type_id, products.discount_value').each do |p|
          order.cart.products_in_cart.update(p.pic_id, sale_price: p.real_price)
        end 
      end
    rescue ActiveRecord::RecordInvalid => e
      errors.push current_session.user.errors.messages unless current_session.user.errors.empty?
      errors.push address.errors.messages unless (address.nil? and address.errors.empty?)
      errors.push order.errors.messages unless order.errors.empty?
      return formError(errors)
    end

    begin
      OrderWorker.delay.new_order(order.id)
    rescue Exception => e
      logger.error e
    end

    flash[:new_order] = order.id
    head status: :ok
    
  end

  def show
    render 'Oops!'
  end


  private

    def formError(errors)
      render json: errors.to_json, status: :unprocessable_entity
    end

end

