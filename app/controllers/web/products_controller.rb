class Web::ProductsController < Web::ApplicationController
  
  def show
    @product = Product.visible.find_by_link_rewrite(params[:id]) 
    if @product.nil?
      begin
        old = OldProduct.find_by_link_rewrite(params[:id])
        p_id =  Product.search_for_ids(conditions: {name: old.name}).to_a.first unless old.nil?
        @product = Product.select('link_rewrite').find(p_id)
        redirect_to product_url(id: @product.link_rewrite, format: :html, only_path: false)
      rescue Exception => e
        not_found
      end
    end
    add_viewed_product(@product.id)
  end

end
