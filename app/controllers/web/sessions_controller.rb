class Web::SessionsController < Web::ApplicationController
  before_filter :only_user, only: [:logout]
  before_filter :except_user, only:[:login, :login_page, :oauth ]
  skip_before_filter :verify_authenticity_token, only: :oauth
  def login
    ipBan = IpBan.get(request.ip, IpBan::LOGIN)
    puts ipBan
    unless ipBan[:valid?]
      if params[:captcha].is_a?(Hash) and params[:captcha][:value] and params[:captcha][:uid]
        begin
          ipBan[:valid?] = YaCleanweb.captcha_valid?(params[:captcha][:uid], params[:captcha][:value])
        rescue Exception => e
          ipBan[:valid?] = true
        end
      end
    end
    auth = UserAuth.auth_by(params[:uid], :sm, params[:password])
    if auth and ipBan[:valid?]
      session_regenerate(params[:remember_me])
      current_session.user = auth.user
      if (!current_cart? or current_cart.products.size == 0) and auth.user.cart
        current_session.cart = auth.user.cart
      elsif current_cart?
        auth.user.cart.destroy if auth.user.cart
        auth.user.cart = current_session.cart
        auth.user.save
      end
      current_session.save
      return head status: :ok
    else
      IpBan.add(request.ip, IpBan::LOGIN)
      unless ipBan[:next_valid?]
        begin
          captcha = YaCleanweb.captcha
          return render json: captcha.to_json, status: (auth ? :failed_dependency : :locked)
        rescue Exception => e
          
        end
          
      end
      head status: :unauthorized
    end
  end

  def login_page
    
  end

  def logout
    session_destroy
    redirect_to request.env["HTTP_REFERER"]
  end

  def auth?
    head user_signed_in? ? :ok : :unauthorized 
  end

  def oauth
    provider = ('provider_'<<request.env['omniauth.auth'].provider.to_s)
    
    if self.respond_to? provider, true

      if UserAuth.auth_by(request.env['omniauth.auth'].uid, request.env['omniauth.auth'].provider.to_sym)
        u_auth = UserAuth.auth_by(request.env['omniauth.auth'].uid, request.env['omniauth.auth'].provider.to_sym)
        Session.destroy_all("user_id = #{u_auth.user.id}") if u_auth.user

      else
        u = User.new

        u_auth = UserAuth.new do |ua|
          ua.provider = UserAuth::PROVIDERS[request.env['omniauth.auth'].provider.to_sym]
          ua.uid = request.env['omniauth.auth'].uid
        end

        UserAuth.transaction do

          begin
            self.send(provider,u)
          rescue Exception => e
            u = User.new
            u.save!
          end
          u_auth.user = u

          unless u_auth.save
            render 'web/auth/fail', layout: false
            return
          end
        end
      end
      session_regenerate
      current_session.user = u_auth.user

      if (!current_cart? or current_cart.products.size == 0) and u_auth.user.cart
        current_session.cart = u_auth.user.cart
      elsif current_cart?
        u_auth.user.cart.destroy if u_auth.user.cart
        u_auth.user.cart = current_session.cart
        u_auth.user.save
      end
      current_session.save
      render 'web/auth/success', layout: false
    else
      render text: 'provider not found'
      #raise request.env['omniauth.auth'].to_yaml
    end
  end

  private
    def provider_twitter(user)
     
      full_name = request.env['omniauth.auth'].info.name.split(' ')

      if full_name.size > 1 
        user.first_name = full_name[0]
        user.last_name = full_name[1]
      elsif full_name.size == 1
        user.first_name = full_name[0]
      end
      user.save!

    end

    def provider_facebook(user)
      user.email = request.env['omniauth.auth'].info.email
      user.email_verified = true
      user.save!
    end

    def provider_vkontakte(user)

      user.first_name = request.env['omniauth.auth'].info.first_name
      user.last_name = request.env['omniauth.auth'].info.last_name
      user.save!

    end

    def provider_yandex(user)
      
      full_name = request.env['omniauth.auth'].info.name.split(' ')

      if full_name.size == 3 
        user.first_name = full_name[1]
        user.last_name = full_name[0]
        user.middle_name = full_name[2]
      elsif full_name.size == 2
        user.first_name = full_name[1]
        user.last_name = full_name[0]
      elsif full_name.size == 1
        user.first_name = full_name[0]
      end
      user.email = request.env['omniauth.auth'].info.email
      user.email_verified = true

      user.save!

    end

    def provider_google_oauth2(user)

      user.first_name = request.env['omniauth.auth'].info.first_name
      user.last_name = request.env['omniauth.auth'].info.last_name
      user.email = request.env['omniauth.auth'].info.email
      user.email_verified = true
      user.save!

    end

    def provider_mailru(user)

      user.first_name = request.env['omniauth.auth'].info.first_name
      user.last_name = request.env['omniauth.auth'].info.last_name
      user.email = request.env['omniauth.auth'].info.email
      user.email_verified = true
      user.save!

    end

end