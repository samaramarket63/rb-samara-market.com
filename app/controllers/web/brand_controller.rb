class Web::BrandController < Web::ApplicationController
  def index
    @brands = Brand
    .select('id, name, link_rewrite, short_description, logo, count_visible_products, count_sales_products, categories_count')
    .order('categories_count DESC')
  end
  def show
    @brand = Brand.find_by_link_rewrite(params[:id]) || not_found
  end

  def products
    @brand = Brand.select('id, name, link_rewrite, meta_description, logo, short_description, count_visible_products, count_sales_products, categories_count, categories_list')
      .find_by_link_rewrite(params[:brand_id]) || not_found

    @filter = {
      sort: sorting_products(params[:sort]),
      types: normalize_types_params(@brand.types.visible.select('`product_types`.`id`,`product_types`.`name`'))
    }

    @products = @brand.products.select('products.id,
      products.name, products.link_rewrite, products.cover_id, products.status,
      products.price, products.brand_id, products.discount_type_id, products.discount_value').order('products.status DESC')
      .sorting(@filter[:sort]).visible().includes(:cover)
      .page(params[:page])

    @products = @products.where(type_id: @filter[:types]) if @filter[:types]
  end
  private
    def normalize_types_params(real_types)
      types = nil
      if params[:types]
        params[:types] = (params[:types].to_a.collect{|x| x.to_i if x.to_i > 0}).compact
        types = (params[:types].size > 0) ? params[:types] : nil
      end
      return types ? types : nil
    end

    def sorting_products(sort)
      case sort
        when 'cheap'
          :cheap
        when 'az'
          :az
        when 'za'
          :za
        when 'news'
          :news
        when 'discount_first'
          :discount_first
        else
          nil
        end
    end
end
