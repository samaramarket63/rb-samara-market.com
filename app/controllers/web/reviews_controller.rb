class Web::ReviewsController < Web::ApplicationController
  include ActionView::Helpers::SanitizeHelper

  before_filter :only_user, only: [:create, :destroy, :edit?, :vote_down, :vote_up ]


  def create
    product = Product.find params[:product_id]

    review = product.reviews.where("user_id = #{current_session.user.id}").first

    if review
      review.period = params[:period]
      review.stars = params[:stars]
      review.title = params[:title]
      review.value = params[:value]
      review.limitations = params[:limitations]
      review.comment = params[:comment]
      review.user_name = params[:user_name]
    else

      review = product.reviews.build(
        period: params[:period],
        stars: params[:stars],
        title: params[:title],
        value: params[:value],
        limitations: params[:limitations],
        comment: params[:comment],
        user_name: params[:user_name]
      )
      review.user = current_session.user

    end
    if params[:captcha].is_a?(ActiveSupport::HashWithIndifferentAccess)
      begin
        captcha = YaCleanweb.captcha_valid?(params[:captcha][:uid], params[:captcha][:value], params[:captcha][:id])
      rescue Exception => e
        logger.error e.message
        captcha = true
      end
      
    else
      captcha = nil
    end
    
    if review.valid?
      if captcha.nil?
        check_spam = YaCleanweb.check_spam(
          request.ip,
          params[:title],
          (params[:comment].to_s + params[:limitations].to_s + params[:value].to_s)
        )

        if check_spam.spam?
          return render json: check_spam.captcha.to_json, status: :expectation_failed
        end
      elsif captcha == false 
        return render json: YaCleanweb.captcha(params[:captcha][:id]).to_json, status: :expectation_failed
      end

      if review.save 
        if current_session.user.first_name.blank?
          current_session.user.first_name = review.user_name
          current_session.user.save(validate: false)
        end
        head status: :ok
      else
        return render json: review.errors.to_json, status: :unprocessable_entity
      end
    else
      return render json: review.errors.to_json, status: :unprocessable_entity
    end
  end

  def destroy
    product = Product.find params[:product_id]
    review = product.reviews.where("user_id = #{current_session.user.id}").first
    review.destroy if review
    head status: :ok
  end

  def edit?
    product = Product.find params[:product_id]
    review = product.reviews.where("user_id = #{current_session.user.id}").first

    
    if review
    
      review.value.gsub!(/\r\n?|\n|<br>|<\/p><p>/, "\n")
      review.limitations.gsub!(/\r\n?|\n|<br>|<\/p><p>/, "\n")
      review.comment.gsub!(/\r\n?|\n|<br>|<\/p><p>/, "\n")

      review.value.gsub!(/<p>|<\/p>/, '')
      review.limitations.gsub!(/<p>|<\/p>/, '')
      review.comment.gsub!(/<p>|<\/p>/, '')

      render json: review.to_json(only: [:period, :title, :user_name, :stars, :value, :limitations, :comment])
    else 
      head status: :not_found  
    end
  end

  def vote_down
    review = ProductReview.find params[:review_id]

    return head status: :bad_request if review.user == current_session.user

    vote = review.votes.where("user_id = #{current_session.user.id}").first
    if vote 
      if vote.vote == 0
        return head status: :conflict
      else
        review.transaction do
          review.vote_up = review.vote_up - 1
          review.vote_down!
          vote.vote = 0
          vote.save!
          review.save!
        end
      end
    else
      review.transaction do
        review.vote_down!
        review.votes.create!(user_id: current_session.user.id, vote: 0)
        review.save!
      end
    end
    render json: {vote_up: review.vote_up, vote_down: review.vote_down}.to_json
  end

  def vote_up

    review = ProductReview.find params[:review_id]
    
    return head status: :bad_request if review.user == current_session.user

    vote = review.votes.where("user_id = #{current_session.user.id}").first

    if vote 
      if vote.vote == 1
        return head status: :conflict
      else
        review.transaction do
          review.vote_down = review.vote_down - 1
          review.vote_up!
          vote.vote = 1
          vote.save!
          review.save!
        end
      end
    else
      review.transaction do
        review.vote_up!
        review.votes.create!(user_id: current_session.user.id, vote: 1)
        review.save!
      end
    end
    render json: {vote_up: review.vote_up, vote_down: review.vote_down}.to_json
  end

end