class Web::SitemapController < Web::ApplicationController

  caches_page :index, :products, :categories, :brands

  def index
    @maps = ['sitemap_products', 'sitemap_categories', 'sitemap_brands']
  end
  def products
    @products = Product.select('id, link_rewrite, updated_at').visible
  end
  def categories
    @categories = Category.select('id, link_rewrite, updated_at').public
  end
  def brands
    @brands = Brand.select('id, link_rewrite, updated_at, count_visible_products').all
  end
end