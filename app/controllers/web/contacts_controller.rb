class Web::ContactsController < Web::ApplicationController
  def index
    @errors = []
    begin
      @captcha = YaCleanweb.captcha
    rescue Exception => e
      @captcha = nil
    end
  end

  def create
    begin
      @captcha = YaCleanweb.captcha
    rescue Exception => e
      @captcha = nil
    end
    @errors = []
    @types = [0,1,2,3]
    if params[:captcha].is_a?(Hash) and params[:captcha][:value] and params[:captcha][:uid]
      begin
        @errors << t('contacts.errors.captcha_wrong') unless YaCleanweb.captcha_valid?(params[:captcha][:uid], params[:captcha][:value])
      rescue Exception => e
      end
    else
      @errors << t('contacts.errors.captcha_wrong')
    end

    @errors << t('contacts.errors.email_blank') if params[:email].blank?
    @errors << t('contacts.errors.msg_blank') if params[:msg].blank?
    @errors << t('contacts.errors.type_not_found')  unless @types.include? params[:type].to_i
    @errors << t('contacts.errors.order_bill_req') if params[:order_bill].blank? and (params[:type] == '0' or params[:type] == '1')

    if @errors.empty?
      if params[:type] == '0' or params[:type] == '1'
        title =  t('contacts.types.type_'+params[:type], order_bill: params[:order_bill])
      else
        title =  t('contacts.types.type_'+params[:type])
      end
      begin
        UserMailer.contact_form_to(
          type: title,
          order_bill: params[:order_bill],
          msg: params[:msg],
          email: params[:email]
        ).deliver
        flash[:feedback_form_success] = t('contacts.success')
      rescue Exception => e
        logger.error e
        @errors << t('contacts.errors.system_error')
      end    

    end
    render 'web/contacts/index'
  end
end