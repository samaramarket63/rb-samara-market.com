class Web::DeliveryController < Web::ApplicationController
  def index
    @regions = Region.public.includes(cities:[delivery:[:delivery_method]]).where('cities.status = true')
  end
end
