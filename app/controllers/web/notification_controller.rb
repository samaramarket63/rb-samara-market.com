# encoding: UTF-8
class Web::NotificationController < Web::ApplicationController
  protect_from_forgery except: [:qiwi]

  http_basic_authenticate_with name: Qiwi::CONFIG[:api_uid], password: Qiwi::CONFIG[:api_key], only: :qiwi

  def qiwi
    @order = Order.by_bil(params[:bill_id])
    if @order
      case params[:status]
        when 'paid'
          @order.status = 20
        when 'waiting'
          @order.status = 50
        when 'rejected'
          @order.status = 51
        when 'unpaid'
          @order.status = 62
        when 'expired'
          @order.status = 63
      end
      @order.save
    end
    render xml: {result: {result_code: 0}}
  end
  
end
