class Web::StaticContentController < Web::ApplicationController

  def public_offer
    render layout: request.xhr? ? nil : 'application'
  end

  def privacy_policy
    render layout: request.xhr? ? nil : 'application'
  end

  def return_of_goods
    render layout: request.xhr? ? nil : 'application'
  end

end