# encoding: UTF-8
class Web::UserController < Web::ApplicationController
  
  before_filter :only_user, only: [:index, :update, :edit, :data_verified, :data_verified?, :update_password ]
  before_filter :except_user, only: [:recovery_password, :recovery_password! ]

  def index
    @user = current_session.user
  end

  def create
    user = User.new({first_name: params[:first_name], phone: params[:phone]},as: :user)
    auth = UserAuth.new(provider:UserAuth::PROVIDERS[:sm], uid: user.phone, password:params[:password])
    user.cart  = current_cart if current_cart?
    auth.valid?
    begin
      User.transaction do
        user.save!
        auth.user_id = user.id
        auth.save!
        if session_regenerate == false
          raise Exception
        end
        current_session.user_id = user.id
        current_session.save
      end
    rescue ActiveRecord::RecordInvalid => e
      render ({json: user.errors.messages.merge(auth.errors.messages), status: :unprocessable_entity})
      return
    rescue Exception => e
      puts e
      head status: :not_acceptable 
      return 
    end
    

    head status: :ok
    
  end

  def update
    @user = current_session.user
    attributes = {last_name: params[:last_name], 
      first_name: params[:first_name],
      middle_name: params[:middle_name],
      email: params[:email],
      phone: params[:phone]}

    if @user.update_attributes(attributes, as: :user)
      respond_to do |format|
        format.html {
          flash[:success] = t('user.success.update')
          redirect_to user_url
        }
        format.js {}
      end

    else
      respond_to do |format|
        format.html {
          render 'index'
        }
        format.js {}
      end
    end
  end

  def data_verified

    field = UserDataVerified::FIELDS[params[:field].to_sym]

    return data_verified_error t('data_verified.error.field') if field.nil?

    return data_verified_error t('data_verified.error.field_blank', field: User.human_attribute_name(params[:field])) if current_session.user[params[:field].to_sym].blank?

    verified = current_session.user.verifieds.where(field: field).where('updated_at > ?', 10.minute.ago).last

    return data_verified_error t('data_verified.error.timeout', min: ((10.minutes - (Time.now - verified.updated_at))/60).to_i ,timeout: Russian.p(((10.minutes-(Time.now - verified.updated_at))/60).to_i, 'минута', 'минуты', 'минут')) if verified

    verified = current_session.user.verifieds.where(field: field).where('created_at > ?', 1.day.ago).last

    if verified
      return data_verified_error t('data_verified.error.maximum',  field: User.human_attribute_name(params[:field]), count: verified.class::BANS[params[:field].to_sym]) if verified.attempts >= verified.class::BANS[params[:field].to_sym]
      verified.attempts = verified.attempts.to_i.next
      verified.exp = false
      verified.generate_token        


    else
      verified = UserDataVerified.new do |udv|
        udv.user = current_session.user
        udv.field = field
        udv.attempts = 0 
      end
    end
    begin
      verified.save!
    rescue Exception => e
      logger.error e
      return data_verified_error t('data_verified.error.system')
    end

    flash[:data_verified_success] = t("data_verified.success.#{params[:field]}", addr: current_session.user[params[:field]] )
    redirect_to user_url

  end

  def data_verified?
    field = UserDataVerified::FIELDS[params[:field].to_sym]
    
    return data_verified_error t('data_verified.error.field') if field.nil?
    
    return data_verified_error t('data_verified.error.already_verified', field: User.human_attribute_name(params[:field]), addr: current_session.user[params[:field]]) if current_session.user[params[:field]+'_verified']

    verified = current_session.user.verifieds.where(field: field, exp: false).where('updated_at > ?', 1.day.ago).last

    if verified
      if verified.compare_token(params[:token])
        flash[:data_verified_success] = t("data_verified.success.#{params[:field]}_verified", addr: current_session.user[params[:field]])
        return redirect_to user_url
      else
        return data_verified_error t('data_verified.error.token_invalid',  field: User.human_attribute_name(params[:field]), addr: current_session.user[params[:field]])
      end
    else
      return  data_verified_error t('data_verified.error.token_exp',  field: User.human_attribute_name(params[:field]), addr: current_session.user[params[:field]])
    end

  end

  def recovery_password
    @errors = []
    @recovery = nil
    @captcha = YaCleanweb.captcha
  end

  def recovery_password!
    @errors = []
    @recovery = nil
    @captcha = YaCleanweb.captcha
    if params[:captcha].is_a?(Hash) and params[:captcha][:value] and params[:captcha][:uid]
      begin
        @errors << t('errors.recovery_password.captcha_wrong') unless YaCleanweb.captcha_valid?(params[:captcha][:uid], params[:captcha][:value])
      rescue Exception => e
      end
    else
      @errors << t('errors.recovery_password.captcha_wrong')
    end
  
    if @errors.empty?
      user = User.where(phone: params[:phone]).first
      @errors << t('errors.recovery_password.user_not_found') if user.nil? 
      
      if @errors.empty?
        auth = user.auths.where(provider: UserAuth::PROVIDERS[:sm]).first

        @errors << t('errors.recovery_password.auth_not_found') if auth.nil?

        if @errors.empty?
          @recovery = UserPasswordRecovery.get(auth)
          @errors << t('errors.recovery_password.limit',time: (@recovery.exp_minutes.to_i/60), time_s: Russian.p(@recovery.exp_minutes.to_i/60, 'минуту', 'минуты', 'минут')) if @recovery.ban?

          if @errors.empty?
            @recovery.regenerate_password!
          end

        end

      end

    end
    if @errors.any?
      render template: 'web/user/recovery_password'
    else
      flash[:phone] = @recovery.user_auth.user.phone
      flash[:success] = t('recovery_password.success', phone: @recovery.user_auth.user.phone )
      redirect_to user_login_url
    end
  end

  def update_password

    return update_password_error t('update_user_password.errors.passwords_same') if params[:password] == params[:old_password]
    return update_password_error t('update_user_password.errors.retry_password')  unless params[:password] == params[:retry_password]
    
    auth = current_session.user.auths.where(provider: UserAuth::PROVIDERS[:sm]).first
    
    return update_password_error t('update_user_password.errors.not_update') if auth.nil?
    
    auth.password = params[:password]
    
    if auth.save
      flash[:update_password_success] = t('update_user_password.success')
      redirect_to user_url(anchor: 'user_password_form')
    else
      return update_password_error auth.errors.full_messages
    end
  end

  private

    def data_verified_error(msg)
      flash[:data_verified_error] = msg
      redirect_to user_url
    end

    def update_password_error(msg)
      flash[:update_password_error] = msg
      redirect_to user_url(anchor: 'user_password_form')
    end

end