class Web::SearchController < Web::ApplicationController

  def index
    params[:query].to_s.strip!
    
    if params[:query].to_s.length > 1
      @query = Product.search(
        include: :cover,
        select: 
          [ :id, 
            :name,
            :status,
            :price,
            :link_rewrite,
            :short_description,
            :cover_id,
            :type_id,
            :brand_id,
            :discount_type_id,
            :discount_value
          ]) do

        
        facet :type_id, exclude: (with(:type_id, params[:types].map{|x| x.to_i}) unless params[:types].blank?)

        facet :brand_id, exclude: (with(:brand_id, params[:brands].map{|x| x.to_i}) unless params[:brands].blank?)
         

        fulltext params[:query]

        with :status, 20..30

        if params[:page] and params[:page].to_i > 0
          paginate page: params[:page].to_i 
        end

      end

      @products = @query.results

      if @query.total > 0
        @filter = {
          brands: params[:brands].to_a.uniq.collect!{|x| x.to_i},
          types: params[:types].to_a.uniq.collect!{|x| x.to_i}
        }

        @brands = Brand.where(id: @query.facet(:brand_id).rows.map{|x| x.value})
        
        @types = ProductsType.where(id: @query.facet(:type_id).rows.map{|x| x.value})
      end
    end
  end

end