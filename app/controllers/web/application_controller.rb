# encoding: UTF-8
class Web::ApplicationController < ApplicationController
  
  helpers = %w(add_viewed_product session_destroy current_session current_session? create_sesion user_signed_in? session_expired? create_cart current_cart current_cart? only_user)
 
  helper_method *helpers

  private

    def session_destroy
      current_session.destroy if current_session?
      cookies.delete(:client_token)
      cookies.delete(:client_id)
    end

    def current_session
      @current_session ||= set_session  
    end

    def current_session?
      !current_session.id.nil?  
    end

    def session_expired?
      current_session.ip_hash == Digest::MD5.hexdigest(cookies[:client_token]+request.env["HTTP_USER_AGENT"])
    end

    def session_regenerate(remember_me=false)
      return create_session(remember_me) if !current_session?

     token = Digest::MD5.hexdigest(Time.now.to_f.to_s+request.env["HTTP_USER_AGENT"]+request.ip)
      cookies[:client_token] = { value: token, expires: Time.now + 14.day, httponly: true}
      cookies[:client_id] = { value: current_session.id, expires: Time.now + 14.day, httponly: true}
      current_session.token = Digest::MD5.hexdigest(token+(remember_me ? request.env["HTTP_USER_AGENT"] : request.ip ))
      current_session.remember_me = ActiveRecord::ConnectionAdapters::Column.value_to_boolean(remember_me)
      current_session.save

      current_session
    end

    def create_session(remember_me=false)
      return current_session if current_session?
   
      if cookies[:test] != 'true'
        notification.add(:error, t('session.error.cookie_disabled'))
        return false
      end

      token = Digest::MD5.hexdigest(Time.now.to_f.to_s+request.env["HTTP_USER_AGENT"]+request.ip)

      session = Session.new

      session.ip_hash = Digest::MD5.hexdigest(request.remote_ip)
      session.token = Digest::MD5.hexdigest(token+(remember_me ? request.env["HTTP_USER_AGENT"] : request.ip ))
      session.remember_me = ActiveRecord::ConnectionAdapters::Column.value_to_boolean(remember_me)
      if session.save
        cookies[:client_id] = { value: session.id, expires: Time.now + 14.day, httponly: true}
        cookies[:client_token] = { value: token, expires: Time.now + 14.day, httponly: true}
      else
        notification.add(:error, t('session.error.not_created'))
        return false
      end
      @current_session = session
    end

    def set_session
      if (cookies[:client_id] and cookies[:client_token]) and !(cookies[:client_id].blank? or cookies[:client_id].blank?)
       
        ses = Session.find_by_id(cookies[:client_id])
        if ses  
          token = Digest::MD5.hexdigest(cookies[:client_token]+(ses.remember_me ? request.env["HTTP_USER_AGENT"] : request.ip ))
          
          return ses if token == ses.token
        end
      end
      Session.new
    end

    def user_signed_in?
      current_session.user_id?  
    end
   
    def current_cart
      current_session.cart
    end
   
    def current_cart?
      current_session.cart
    end


    def create_cart
      if current_cart?
        return true 
      end

      c = Cart.new

      current_session.transaction do


        if !current_session?
          if !create_session
            return false
          end
        end
        c.save
        if user_signed_in?
          current_session.user.cart = c
          current_session.user.save
        end
        current_session.cart = c
        current_session.save
      end
    end

    def only_user
      if !user_signed_in?
        respond_to do |format|
          format.html {
            flash[:error] = "You must be logged in to access this section"
            flash[:redirect_url] = request.path
            redirect_to user_login_url
          }
          format.js { head status: :unauthorized  }
        end
      end
    end

    def except_user
      if user_signed_in?
        respond_to do |format|
          format.html {
            flash[:error] = "You must be logged in to access this section"
            redirect_to user_url
          }
          format.js { head statue: :ok  }
        end
      end
    end

    def add_viewed_product(product_id)
      if cookies[:viewed_products] and cookies[:viewed_products].is_a?(String)
        products = cookies[:viewed_products].split(',').map!{|x| x.to_i}
      else
        products = []
      end
      unless products.include?(product_id)
        products = (products.unshift(product_id))[0..30]
        cookies[:viewed_products] = { value: products.join(','), expires: Time.now + 90.day, httponly: true}
        return true
      else
        return false
      end
    end

end