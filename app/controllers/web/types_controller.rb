class Web::TypesController < Web::ApplicationController
  
  def index
    @types = ProductsType.select('id, link_rewrite, name, products_count, brands_ids').order('name')
  end

  def show
    @type = ProductsType.find_by_link_rewrite(params[:id]) || not_found

  end

  def products
    params[:filters] = {} unless params[:filters].instance_of? ActiveSupport::HashWithIndifferentAccess
    @type = ProductsType.select('id, link_rewrite, name, products_count, brands_ids, meta_description').find_by_link_rewrite(params[:type_id]) || not_found
    type = @type
    @query = Product.search(
        include: :cover,
        select: 'products.id,
          products.name, products.link_rewrite, products.cover_id, products.status,
          products.price, products.brand_id, products.ratio, products.ratio_u, products.discount_type_id, products.discount_value',
      ) do

      with :status, 20..30     

      with :type_id, type.id

      facet_exclude = []
      facet_exclude.push with(:brand_id, params[:filters][:brands].map{|x| x.to_i}) unless params[:filters][:brands].blank?
      facet_exclude.push with(:type_id, params[:filters][:types].map{|x| x.to_i}) unless params[:filters][:types].blank?

      if type.properties.where(is_filter: true).count > 0
        
        dynamic(:properties) do
          type.properties.where(is_filter: true).each do |prop|
            q = nil
            q = with(prop.id.to_s, params[:filters][prop.id.to_s]) unless params[:filters][prop.id.to_s].blank?
            facet prop.id, exclude: q, zeros: true
            facet_exclude.push q if q
          end
        end
      end

      facet :brand_id, exclude: facet_exclude

      order_by :status, :desc
      case params[:sort]
        when 'news'
          order_by :id, :desc
        when 'cheap'
          order_by :real_price
        when 'discount_first'
          order_by :discount_type_id, :desc
          order_by :real_price, :desc
        when 'az'
          order_by :name
        when 'za'
          order_by :name, :desc
        else
          order_by :real_price, :desc
      end
      if params[:page] and params[:page].to_i > 0
        paginate page: params[:page].to_i 
      end
      
    end

    @products = @query.results
    # @type = ProductsType.select('id, link_rewrite, name, products_count, brands_ids, meta_description').find_by_link_rewrite(params[:type_id]) || not_found
    
    # @filter = {
    #   sort: sorting_products(params[:sort]),
    #   brands: normalize_brend_params(@type.brands_ids)
    # }
    # @products = @type.products.select('products.id,
    #   products.name, products.link_rewrite, products.cover_id, products.status,
    #   products.price, products.brand_id, products.discount_type_id, products.discount_value').order('products.status DESC')
    #   .sorting(@filter[:sort]).visible().includes(:cover)
    #   .page(params[:page])

    # @products = @products.where(brand_id: @filter[:brands]) if @filter[:brands]
    @brands = Brand.where(id: @query.facet(:brand_id).rows.map{|x| x.value}).select('id, name')
  end

  private

    def normalize_brend_params(real_brands)
      brands = nil
      if params[:brands]
        params[:brands] = (params[:brands].to_a.collect{|x| x.to_i if x.to_i > 0}).compact
        params[:brands] = (params[:brands].size > 0) ? params[:brands] : nil
        brands = (params[:brands] & real_brands)
      end
      return brands ? brands : nil
    end

    def sorting_products(sort)
      case sort
        when 'cheap'
          :cheap
        when 'az'
          :az
        when 'za'
          :za
        when 'news'
          :news
        when 'discount_first'
          :discount_first
        else
          nil
        end
    end
end
