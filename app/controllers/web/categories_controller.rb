class Web::CategoriesController < Web::ApplicationController

  #before_filter :normalize_brend_params, only: [:show]

  helper_method :normalize_brend_params, :sorting_products, :normalize_types_params

  def index
    @categories = Category.select('id, name, link_rewrite, depth, parent_id, lft, rgt, types_ids, products_visible').public.tree
  end

  def show

    params[:filters] = {} unless params[:filters].instance_of? ActiveSupport::HashWithIndifferentAccess

    category = Category.public.find_by_link_rewrite(params[:category_id]) || not_found
    
    
    properties = []

    if category.types_ids.to_a.count == 1 or params[:filters][:types].to_a.count == 1
      if category.types_ids.to_a.count == 1
        current_type = category.types_ids.to_a[0]
      else
        current_type = params[:filters][:types].to_a[0]
      end 
      properties = ProductsType.find(current_type).properties.select([:id, :name, :unit]).where(is_filter: true).order(:position)
    end
    @query = Product.search(
        include: :cover,
        select: 'products.id,
          products.name, products.link_rewrite, products.cover_id, products.status,
          products.price, products.brand_id, products.ratio, products.ratio_u, products.discount_type_id, products.discount_value',
      ) do
      with :categories_ids, category.id

      with :status, 20..30     

      facet_exclude = []
      facet_exclude.push with(:brand_id, params[:filters][:brands].map{|x| x.to_i}) unless params[:filters][:brands].blank?
      facet_exclude.push with(:type_id, params[:filters][:types].map{|x| x.to_i}) unless params[:filters][:types].blank?

      if properties.count > 0
        
        dynamic(:properties) do
          properties.each do |prop|
            q = nil
            q = with(prop.id.to_s, params[:filters][prop.id.to_s]) unless params[:filters][prop.id.to_s].blank?
            facet prop.id, exclude: q, zeros: true
            facet_exclude.push q if q
          end
        end
      end

      facet :brand_id, exclude: facet_exclude

      facet :type_id, exclude: facet_exclude


      order_by :status, :desc
      case params[:sort]
        when 'news'
          order_by :id, :desc
        when 'cheap'
          order_by :real_price
        when 'discount_first'
          order_by :discount_type_id, :desc
          order_by :real_price, :desc
        when 'az'
          order_by :name
        when 'za'
          order_by :name, :desc
        else
          order_by :real_price, :desc
      end
      if params[:page] and params[:page].to_i > 0
        paginate page: params[:page].to_i 
      end
      
    end
    @category = category
    @properties = properties
    @products = @query.results
    @types = ProductsType.order(:name).select([:id, :name]).find(@query.facet(:type_id).rows.map{|x| x.value})
  end

  def discounts

    @brands_ids = Product.only_discount.uniq.pluck('brand_id')
    @types_ids = Product.only_discount.uniq.pluck('type_id')

    @filter = {
      brands: normalize_brend_params(@brands_ids),
      types: normalize_types_params(@types_ids),
      sort: nil
    }

    @filter[:sort] = case params[:sort]
      when 'cheap'
        :discount_first_asc
      when 'az'
        :az
      when 'za'
        :za
      when 'news'
       :news
      when 'expensive'
        :discount_first
      else
        :amount_of_discount
    end

    @products = Product.joins(:brand).select('products.id,
      products.name, products.link_rewrite, products.cover_id, products.status,
      products.price, products.brand_id, brands.name AS `brand_name`, products.ratio, products.ratio_u, products.discount_type_id, products.discount_value').order('products.status DESC')
      .sorting(@filter[:sort]).only_discount.visible().includes(:cover).page(params[:page])

    @products = @products.brands(@filter[:brands]) if @filter[:brands]
    @products = @products.where(type_id: @filter[:types]) if @filter[:types]

    respond_to do |format|
      format.html do
        @brands = Brand.where(id: @brands_ids).select('id, name')
        @types = ProductsType.where(id: @types_ids).select('id, name')
      end
      format.json { render json: category } 
    end
  end

  def viewed
    @viewed_ids = cookies[:viewed_products].to_s.split(",")

    ids = Product.select('brand_id, type_id').visible().where(id: @viewed_ids).limit(30)

    brand_ids = ids.collect{|x| x.brand_id}.uniq
    type_ids = ids.collect{|x| x.type_id}.uniq

    @filter = {
      brands: normalize_brend_params(brand_ids),
      sort: sorting_products(params[:sort]),
      types: normalize_types_params(type_ids)
    }

    @products = Product.joins(:brand).select('products.id,
      products.name, products.link_rewrite, products.cover_id, products.status,
      products.price, products.brand_id, brands.name AS `brand_name`, products.ratio, products.ratio_u, products.discount_type_id, products.discount_value').order('products.status DESC')
      .sorting(@filter[:sort]).visible().includes(:cover).where(id: @viewed_ids).limit(30)

    @products = @products.brands(@filter[:brands]) if @filter[:brands]
    @products = @products.where(type_id: @filter[:types]) if @filter[:types]
    
    respond_to do |format|
      format.html do
        @brands = Brand.where(id: brand_ids).select('id, name')
        @types = ProductsType.where(id: type_ids).select('id, name')
      end
      format.json { render json: category } 
    end
  
  end

  private

    def normalize_brend_params(real_brands)
      brands = nil
      if params[:brands]
        params[:brands] = (params[:brands].to_a.collect{|x| x.to_i if x.to_i > 0}).compact
        params[:brands] = (params[:brands].size > 0) ? params[:brands] : nil
        brands = (params[:brands] & real_brands)
      end
      return brands ? brands : nil
    end

    def normalize_types_params(real_types)
      types = nil
      if params[:types]
        params[:types] = (params[:types].to_a.collect{|x| x.to_i if x.to_i > 0}).compact
        types = (params[:types].size > 0) ? params[:types] : nil
      end
      return types ? types : nil
    end

    def sorting_products(sort)
      case sort
        when 'cheap'
          :cheap
        when 'az'
          :az
        when 'za'
          :za
        when 'news'
          :news
        when 'discount_first'
          :discount_first
        else
          nil
        end
    end

end