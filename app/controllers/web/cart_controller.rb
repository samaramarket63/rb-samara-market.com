class Web::CartController < Web::ApplicationController

  def index
    @covers = {}
    if current_cart?
      t_covers = ProductImages.where(id: current_cart.products.select('cover_id').collect{|x|x.cover_id})

      t_covers.each do |c|
        @covers[c.id] = c
      end
      
    end

  end

  def push

    product = Product.select('id, name, link_rewrite, price, status, discount_type_id, discount_value').in_stock.find_by_id(params[:product_id])

    if product
      if create_cart
        if p = (current_cart.add product, (params[:count] ? params[:count].to_i : 1))
          render json: {product: product, cart_product: p}.to_json(methods: [:real_price, :sum_discount])
        else
          notification.add('error', t('cart.error.not_added'))
          render json: notification.get.to_json, status: :unprocessable_entity
        end
      else
        render json: notification.get.to_json, status: :unprocessable_entity
      end
    else
       notification.add('error', t('cart.error.product_not_find'))
       render json: notification.get.to_json, status: :unprocessable_entity
    end
  end

  def pop 
    
    if current_cart and (product = current_cart.products.find_by_id(params[:product_id]))
      render json: current_cart.pop(product, (params[:count] ? params[:count].to_i : 1)).to_json, status: :unprocessable_entity  
    else
      notification.add('error', t('cart.error.product_not_find'))
      render json: notification.get.to_json, status: :unprocessable_entity
    end

  end

  def destroy
    params[:id] = (params[:id].to_a.collect{|x| x.to_i if x.to_i > 0}).compact
    if current_cart
      d = current_cart.products_in_cart.select('id').where(id: params[:id]).destroy_all
      render json:(d.collect{|x| x.id}), status: :ok
    else
      head status: :not_acceptable 
    end
  end

  def set_count
    if current_cart and p = current_cart.products_in_cart.select('id, count').find_by_id(params[:id])
      p.count = params[:count].to_i

      if p.count > 0
        if p.save
          render json: p, status: :ok
        else
          render json: p.errors.to_json, status: :unprocessable_entity
        end
      else
        if p.destroy
          render json: p, status: :ok
        else
          render json: p.errors.to_json, status: :unprocessable_entity
        end
      end
    else
      notification.add('error', t('cart.error.product_not_edit'))
      render json: notification.get.to_json, status: :expectation_failed 
    end
  end


end