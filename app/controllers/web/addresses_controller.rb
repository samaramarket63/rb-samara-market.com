class Web::AddressesController < Web::ApplicationController
  before_filter :only_user, only: [:index, :new, :edit, :create, :update, :destroy]
  
  def index
    @user = current_session.user

    @addresses = @user.addresses.includes(city:[:region])
  
    respond_to do |format|
      format.html
      format.json { render json: @addresses.to_json(
        only:[:id, :postcode, :adds],
        include:{
          city:{
            only: [:id,:name],
            include:{
              region:{
                only:[:id, :name]
              }
            }
          }
        }
      )}
    end

  end


  # GET /web/user_addresses/new
  # GET /web/user_addresses/new.json
  def new
    @address = UserAddress.new
  end

  # GET /web/user_addresses/1/edit
  def edit

    begin
      @address = current_session.user.addresses.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      flash[:error] =  t('model.messages.address.not_belong_to_user')
      redirect_to addresses_url
    end

     @address.errors.add(:global, t('model.messages.address.editing_allowed_because_of_order')) if @address.address_editing_allowed?
  
  end

  # POST /web/user_addresses
  # POST /web/user_addresses.json
  def create
    @address = current_session.user.addresses.build({adds: params[:adds], postcode: params[:postcode]}, as: :user)

    @address.errors.add(:region, t('order.errors.region.not_select')) if params[:region].blank?
    @region_id = params[:region]
    
    @address.errors.add(:city, t('order.errors.city.not_select')) if  params[:city].blank?
    @city_id = params[:city]
    
    city = City.find_by_id(params[:city])

    @address.errors.add(:city, t('order.errors.city.not_found')) if  city.nil? or city.region_id != params[:region].to_i

    @address.city = city 

    if @address.errors.blank? and @address.save
      flash[:success] =  t('model.messages.address.created')
      redirect_to addresses_url
    else
      render action: "new"
    end

  end

  # PUT /web/user_addresses/1
  # PUT /web/user_addresses/1.json
  def update

    @address = current_session.user.addresses.find(params[:id])

    @address.errors.add(:global, t('model.messages.address.editing_allowed_because_of_order')) if @address.address_editing_allowed?

    @address.errors.add(:region, t('order.errors.region.not_select')) if params[:region].blank?
    @region_id = params[:region]
    
    @address.errors.add(:city, t('order.errors.city.not_select')) if  params[:city].blank?
    @city_id = params[:city]
    
    city = City.find_by_id(params[:city])
    
    @address.errors.add(:city, t('order.errors.city.not_found')) if  city.nil? or city.region_id != params[:region].to_i

    @address.city = city  

    if @address.errors.blank? and @address.update_attributes({adds: params[:adds], postcode: params[:postcode]},as: :user)
      flash[:success] =  t('model.messages.address.updated')
      redirect_to addresses_url
    else
      render action: "edit"
    end

  end

  # DELETE /web/user_addresses/1
  # DELETE /web/user_addresses/1.json
  def destroy
    @address = current_session.user.addresses.find(params[:id])

    if @address.address_editing_allowed?
      flash[:error] = t('model.messages.address.delet_allowed_because_of_order')
      redirect_to addresses_url
    else
      @address.destroy
      flash[:success] = t('model.messages.address.deleted')
      redirect_to addresses_url
    end

  end

end