# encoding: UTF-8
class OrderWorker
  include Sidekiq::Worker

  def self.new_order(order_id)


    time = 3.minute.from_now

    if time.hour <= 22 and time.hour >= 10
      OrderWorker.user_new_order(order_id)
    else
      if time.hour < 10
        send_time = Time.new(time.year, time.month, time.day.to_i, 10)
      else
        send_time = Time.new(time.year, time.month, time.day.to_i+1, 10)
      end
      OrderWorker.delay_until(send_time).user_new_order(order_id)
    end

    OrderWorker.employee_new_order(order_id)

  end

  def self.user_new_order(order_id)
    order = Order.find(order_id)
    Sms.send(
      '7'<<order.user.phone,
      I18n.t('order.issued.thanks', bill: order.bill)
    )
  end

  def self.employee_new_order(order_id)
    order = Order.find(order_id)
    Sms.send(
      Sm::Application.config.sm[:alert_phones],
      "Новый заказ #{order.bill}"
    )
  end

end