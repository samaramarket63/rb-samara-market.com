# encoding: UTF-8
module ApplicationHelper
  def price_default(price)
    number_to_currency(price, unit:  'руб.', separator: ',', delimiter: ' ', format: "%n %u", precision: 0)
  end

  def messages_from_flash
    html = ''
    html += message_to_html(:error, flash[:error]) if flash[:error]
    html += message_to_html(:success, flash[:success]) if flash[:success]
    html += message_to_html(:info, flash[:info]) if flash[:info]
    html += message_to_html(:block, flash[:block]) if flash[:block]
    html.blank? ? nil : html.html_safe
  end

  def message_to_html(type, msg)
    content_tag :div, class: "alert alert-block system-message alert-#{type} fade in" do
      content_tag(:button, '×', type: :button, data:{dismiss: :alert}, class: 'close') +
      content_tag(:p, msg)
    end
  end

  def logo
    content_tag :a, title: 'интернет магазин', href: root_url do
      content_tag :img,'', src: root_url + 'img/logo.jpg', alt: 'интернет магазин'
    end
  end

end
