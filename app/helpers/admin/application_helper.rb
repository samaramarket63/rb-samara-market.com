# encoding: UTF-8
module Admin::ApplicationHelper
  def price_default(price)
    number_to_currency(price, unit:  'руб.', separator: ',', delimiter: ' ', format: "%n %u", precision: 0)
  end

end
