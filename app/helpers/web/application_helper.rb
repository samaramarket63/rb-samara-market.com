# encoding: UTF-8
module Web::ApplicationHelper

  def price_default(price)
    number_to_currency(price, unit:  'руб.', separator: ',', delimiter: ' ', format: "%n %u", precision: 0)
  end

  def url_adwords adword
    url = '#'
    case adword.link_model
      when 0 #category
        r = Category.select('link_rewrite').find_by_id(adword.link_id).public
        url = category_path(r.link_rewrite, page: nil, format: :html) if r
      when 1 #product
        r = Product.select('link_rewrite').visible.find_by_id(adword.link_id)
        url = product_path(r.link_rewrite, :html) if r
      when 2 #type
        r = ProductsType.select('link_rewrite').find_by_id(adword.link_id)
        url = type_path(r.link_rewrite, :html) if r
      when 3 #type_products
        r = ProductsType.select('link_rewrite').find_by_id(adword.link_id)
        url = type_products_path(r.link_rewrite, page: nil, format: :html) if r
      when 4 #brand
        r = Brand.select('link_rewrite').find_by_id(adword.link_id)
        url = brand_path(r.link_rewrite, :html) if r
      when 5 #brand_products
        r = Brand.select('link_rewrite').find_by_id(adword.link_id)
        url = brand_products_path(r.link_rewrite, page: nil, format: :html) if r
    end
    url
  end
  def home_link(ancor, tag = nil)
    link_to (tag ? (content_tag tag, ancor) : ancor), root_path(only_path: false), class: 'link_home'
  end

  def category_title_link(category, tag= :h1)
    content_tag tag, link_to(category.name, category_path(category.link_rewrite,format: :html, page: nil), title: 'Каталог '+category.name), class: 'category_title'
  end



end
