module Web::TypesHelper

  def types_for_az_list(types)
    letters = []
    letter = nil
    html = ''
    types.each do |type|
      if letter != type.name[0]
        html += '</ul>' unless letter.nil?
        html += '<ul>'
        letter = type.name[0]
        letters = type.name[0]
        html += '<li><strong>'+letter+'</strong></li>'
      end
      html += '<li>'+type.name+'('+type.products_count.to_s+')</li>'
    end   
    html += '</ul>' unless letter.nil?
    html.html_safe 
  end

  def count_equal_first_laters(types, type)
    position = types.index(type) 
    letter = type.name[0]
    counts = 1
    delay = true

    while(delay) do
      position = position.next
      if types[position].nil? or types[position].name[0] != letter
        delay = false
      else
        counts = counts.next
      end
    end

    counts  
  end

end