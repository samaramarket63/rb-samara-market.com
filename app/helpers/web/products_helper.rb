module Web::ProductsHelper

  def product_add_bautton(product, count)
    form = ''
  end

  def stars_ration(ration)
    content_tag :span, class: 'default_stars_ratio' do
      content_tag :span, '', class: "stars stars#{ration}"
    end    
  end

  def review_vote_line(up, down)
    sum = up+down
    
    return nil if sum == 0
       
    content_tag :div, class: 'vote_line' do
      content_tag :div, '', class: "yes", style: "width: #{(100/sum)*up}%"
    end    

  end

end