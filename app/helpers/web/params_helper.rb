module Web::ParamsHelper
  
  #return [params, disabled|enabled]

  def filter_params(filter, value)
  	filter, value = filter.to_s, value.to_s

  	prms = Hash[params[:filters]]
  	css = nil

 	unless prms[filter].blank?

	  if prms[filter].to_a.include? value
	  	css = :active
	  	prms[filter] = prms[filter].to_a-[value]
	  else
	  	prms[filter] = prms[filter].to_a+[value]
	  end
	else
	  prms[filter]=[value]
	end	

	[prms, css]
  end

end