# encoding: utf-8

class ProductImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MimeTypes
  include CarrierWave::MiniMagick
  process :set_content_type
  process convert: 'jpg'

  # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
  # include Sprockets::Helpers::RailsHelper
  # include Sprockets::Helpers::IsolatedHelper

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:

  def store_dir
    "images/products/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url
    "/images/products/fallback/products" + [version_name, "default.png"].compact.join('_')
  end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :scale => [50, 50]
  # end
  version :thumb do
    process :resize_and_pad => [130, 130, '#ffffff'] 
  end
  version :micro do
    process :resize_and_pad => [50, 50, '#ffffff'] 
  end
  version :thumb_200 do
    process :resize_and_pad => [200, 200, '#ffffff'] 
  end
  version :mini do
    process :resize_and_pad => [80, 80, '#ffffff'] 
  end
  version :large do
    process :resize_and_pad => [300, 300, '#ffffff'] 
  end
  version :extra do
    process :resize_and_pad => [600, 600, '#ffffff'] 
  end
  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    "#{model.product.link_rewrite}.jpg" if original_filename
  end

end
