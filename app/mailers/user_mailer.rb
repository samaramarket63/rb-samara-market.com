# encoding: UTF-8
class UserMailer < ActionMailer::Base
  default from: 'Самара-Маркет - поддержка клиентов <support@samara-market.com>'

  def email_verified(verified)
    @user = verified.user
    @verified = verified
    @title = t('emails.user_mailer.title')
    mail(to: @user.email, subject: @title ) do |format|
      format.text { render :layout => 'standart_email.text' }
      format.html { render :layout => 'standart_email.html' }
    end
  end

  def contact_form_to(params)
    @params = params
    @title = @params[:type]
    headers['Reply-To'] = @params[:email]
#    headers['In-Reply-To'] = @params[:email]
    mail(to: 'support@samara-market.com',reply_to: @params[:email], subject: @params[:type]) do |format|
      format.text { render :layout => 'email_support/email_support.text' }
      format.html { render :layout => 'email_support/email_support.html' }
    end
  end

end
