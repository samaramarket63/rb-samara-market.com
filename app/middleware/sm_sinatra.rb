require "sinatra/base"
require "sinatra/cookies"

class SmSinatra < Sinatra::Base
  helpers Sinatra::Cookies

  get '/orders/delivery_from_city/:city_id.json' do
    city = City.find(params[:city_id])

    city.delivery_method.select('
      `delivery_methods`.`name`,
      `delivery_methods`.`description`,
      `delivery_methods`.`fields`,
      `deliveries`.`price_for_free`,
      `deliveries`.`id`,
      `deliveries`.`note`,
      `deliveries`.`price`,
      `deliveries`.`city_id`,
      `deliveries`.`extra_description`').to_json
  end

  get '/orders/payment_for_delivery/:delivery_id.json'do
    delivery = Delivery.find(params[:delivery_id])

    delivery.payments.select('
      `payments`.`id`,
      `payments`.`name`,
      `payments`.`description`,
      `payments`.`fields`,
      `payments`.`commission_fixed`,
      `payments`.`commission_pct`
    ').to_json

  end

  post '/products/recommended' do
    type_ids = Product.where(id: cookies[:viewed_products].to_s.split(",")).uniq.pluck(:type_id)
    products = Product.where(type_id: type_ids)
      .select('
        products.id,
        products.name,
        products.link_rewrite,
        products.cover_id,
        products.status,
        products.price,
        products.discount_type_id,
        products.discount_value'
      )
      .order('products.status DESC')
      .includes(:cover)
      .visible()
      .order_by_last_views
      .limit(20)
    Jbuilder.encode do |json|
      json.ignore_nil!(false)
      json.array!(products) do |product|
        json.link_rewrite product.link_rewrite
        json.name product.name
        
        if product.cover and product.cover.image
          json.cover product.cover.image.thumb.url
        else
          json.cover nil
        end
        
        json.status product.status
        json.price product.price
        json.real_price product.real_price

        if product.discount?
          json.discount do
            json.type product.discount_type_id
            json.value product.discount_value
          end 
        else
          json.discount nil
        end

      end
    
    end
  end

  post '/i_product_view/:product_id' do
    token = Digest::MD5.hexdigest(request.ip.to_s+request.user_agent)

    if !ProductViews.exists?(token: token, product_id: params[:product_id]) and request.xhr?
    
      begin
        ProductViews.create(
          {
            token: token,
            product_id: params[:product_id]
          }
        )
      rescue Exception => e
        
      end

    end

    return true

  end


  post '/i_category_view/:category_id' do
    token = Digest::MD5.hexdigest(request.ip.to_s+request.user_agent)

    if !CategoryViews.exists?(token: token, category_id: params[:category_id]) and request.xhr?
    
      begin
        CategoryViews.create(
          {
            token: token,
            category_id: params[:category_id]
          }
        )
      rescue Exception => e
        
      end

    end

    return true

  end

end