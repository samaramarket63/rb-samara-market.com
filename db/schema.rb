# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130831150749) do

  create_table "article_brands", :force => true do |t|
    t.integer  "article_id"
    t.string   "ancor"
    t.integer  "brand_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "article_images", :force => true do |t|
    t.integer  "article_id"
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "article_images", ["article_id"], :name => "index_article_images_on_article_id"

  create_table "article_products", :force => true do |t|
    t.integer  "article_id"
    t.string   "ancor"
    t.integer  "product_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "article_types", :force => true do |t|
    t.integer  "article_id"
    t.string   "ancor"
    t.integer  "type_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "articles", :force => true do |t|
    t.string   "name",             :null => false
    t.string   "link_rewrite",     :null => false
    t.string   "meta_keywords"
    t.string   "meta_description"
    t.string   "title"
    t.text     "text"
    t.text     "text_compile"
    t.text     "intro"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "articles", ["link_rewrite"], :name => "index_articles_on_link_rewrite", :unique => true

  create_table "brands", :force => true do |t|
    t.string   "name",                   :limit => 100, :null => false
    t.string   "link_rewrite",           :limit => 100, :null => false
    t.string   "short_description",      :limit => 600
    t.string   "meta_description",       :limit => 400
    t.string   "meta_keywords",          :limit => 400
    t.text     "description"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "logo"
    t.integer  "count_visible_products"
    t.integer  "count_sales_products"
    t.text     "categories_list"
    t.integer  "categories_count"
  end

  add_index "brands", ["link_rewrite"], :name => "index_brands_on_link_rewrite", :unique => true

  create_table "carts", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name",              :limit => 100,                :null => false
    t.string   "link_rewrite",      :limit => 100,                :null => false
    t.boolean  "status",                                          :null => false
    t.integer  "parent_id",         :limit => 2
    t.string   "meta_keywords",     :limit => 400
    t.string   "meta_description",  :limit => 400
    t.text     "description"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.integer  "lft",               :limit => 2
    t.integer  "rgt",               :limit => 2
    t.integer  "depth",             :limit => 1
    t.text     "brands_ids"
    t.integer  "adword_id"
    t.text     "types_ids"
    t.integer  "last_views",                       :default => 0
    t.integer  "products_count"
    t.integer  "products_visible"
    t.integer  "products_in_stock"
    t.boolean  "show"
  end

  add_index "categories", ["link_rewrite"], :name => "index_categories_on_link_rewrite", :unique => true
  add_index "categories", ["parent_id"], :name => "index_categories_on_parent_id"

  create_table "categories_products", :id => false, :force => true do |t|
    t.integer "category_id"
    t.integer "product_id"
  end

  create_table "category_advertisings", :force => true do |t|
    t.string   "name",              :limit => 150, :null => false
    t.integer  "position",          :limit => 1,   :null => false
    t.text     "description"
    t.integer  "p_top",             :limit => 2,   :null => false
    t.integer  "p_left",            :limit => 2,   :null => false
    t.integer  "p_bottom",          :limit => 2,   :null => false
    t.integer  "p_right",           :limit => 2,   :null => false
    t.string   "image"
    t.string   "background"
    t.string   "background_color",  :limit => 6
    t.string   "background_repeat", :limit => 1,   :null => false
    t.integer  "link_model",        :limit => 1,   :null => false
    t.integer  "link_id",                          :null => false
    t.integer  "width_category",    :limit => 2
    t.integer  "category_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "category_to_products", :force => true do |t|
    t.integer "product_id"
    t.integer "category_id"
  end

  add_index "category_to_products", ["category_id", "product_id"], :name => "index_category_to_products_on_category_id_and_product_id", :unique => true
  add_index "category_to_products", ["category_id"], :name => "index_category_to_products_on_category_id"
  add_index "category_to_products", ["product_id"], :name => "index_category_to_products_on_product_id"

  create_table "category_views", :id => false, :force => true do |t|
    t.string   "token"
    t.integer  "category_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "category_views", ["token", "category_id"], :name => "index_category_views_on_token_and_category_id", :unique => true

  create_table "cities", :force => true do |t|
    t.string   "name",       :limit => 100, :null => false
    t.integer  "position",   :limit => 1
    t.integer  "region_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.boolean  "status",                    :null => false
  end

  create_table "deliveries", :force => true do |t|
    t.float    "price"
    t.text     "extra_description",  :limit => 255
    t.text     "description"
    t.integer  "city_id"
    t.integer  "delivery_method_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.float    "price_for_free"
    t.string   "note",               :limit => 300
  end

  add_index "deliveries", ["city_id"], :name => "index_deliveries_on_city_id"
  add_index "deliveries", ["delivery_method_id"], :name => "index_deliveries_on_delivery_method_id"

  create_table "delivery_methods", :force => true do |t|
    t.string   "name",             :limit => 32,  :null => false
    t.text     "description"
    t.text     "full_description"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.string   "fields",           :limit => 250
  end

  create_table "delivery_payments", :force => true do |t|
    t.integer  "delivery_id"
    t.integer  "payment_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "delivery_payments", ["delivery_id"], :name => "index_delivery_payments_on_delivery_id"
  add_index "delivery_payments", ["payment_id"], :name => "index_delivery_payments_on_payment_id"

  create_table "employees", :force => true do |t|
    t.string   "phone"
    t.string   "password",   :limit => 32
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "ip_bans", :force => true do |t|
    t.string   "ip",         :limit => 32
    t.integer  "operation",  :limit => 1
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "ip_bans", ["ip"], :name => "index_ip_bans_on_ip"

  create_table "join_product_properties_of_values", :id => false, :force => true do |t|
    t.integer  "value_id"
    t.integer  "property_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "join_product_properties_of_values", ["property_id"], :name => "index_join_product_properties_of_values_on_property_id"
  add_index "join_product_properties_of_values", ["value_id"], :name => "index_join_product_properties_of_values_on_value_id"

  create_table "old_products", :force => true do |t|
    t.string   "name"
    t.string   "link_rewrite"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "old_products", ["link_rewrite"], :name => "index_old_products_on_link_rewrite", :unique => true

  create_table "order_status_histories", :force => true do |t|
    t.integer  "order_id"
    t.integer  "status"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "orders", :force => true do |t|
    t.integer  "user_id"
    t.integer  "payment_id"
    t.integer  "delivery_id"
    t.integer  "address_id"
    t.integer  "status",         :limit => 1
    t.float    "paid"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.text     "note"
    t.float    "delivery_price"
    t.integer  "cart_id"
    t.text     "market_note"
    t.float    "discount"
    t.float    "paid_bonus"
  end

  add_index "orders", ["user_id"], :name => "index_orders_on_user_id"

  create_table "payments", :force => true do |t|
    t.string   "name",             :limit => 32,  :null => false
    t.text     "description"
    t.text     "full_description"
    t.float    "commission_fixed"
    t.integer  "commission_pct",   :limit => 2
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.string   "fields",           :limit => 250
  end

  create_table "product_images", :force => true do |t|
    t.string   "alt"
    t.string   "image"
    t.integer  "position",   :limit => 1
    t.integer  "product_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "product_images", ["product_id"], :name => "index_product_images_on_product_id"

  create_table "product_properties", :force => true do |t|
    t.integer  "product_id"
    t.integer  "property_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "product_properties", ["product_id"], :name => "index_product_properties_on_product_id"
  add_index "product_properties", ["property_id"], :name => "index_product_properties_on_property_id"

  create_table "product_review_votes", :force => true do |t|
    t.integer  "review_id"
    t.integer  "user_id"
    t.integer  "vote",       :limit => 1
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "product_review_votes", ["review_id"], :name => "index_product_review_votes_on_review_id"
  add_index "product_review_votes", ["user_id", "review_id"], :name => "index_product_review_votes_on_user_id_and_review_id", :unique => true
  add_index "product_review_votes", ["user_id"], :name => "index_product_review_votes_on_user_id"

  create_table "product_reviews", :force => true do |t|
    t.integer  "stars",       :limit => 1
    t.string   "title"
    t.text     "value"
    t.text     "limitations"
    t.text     "comment"
    t.integer  "vote_up",     :limit => 3
    t.integer  "vote_down",   :limit => 3
    t.string   "user_name"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.integer  "period",      :limit => 1
    t.boolean  "moderated"
    t.integer  "user_id"
    t.integer  "product_id"
  end

  add_index "product_reviews", ["product_id"], :name => "index_product_reviews_on_product_id"
  add_index "product_reviews", ["user_id", "product_id"], :name => "index_product_reviews_on_user_id_and_product_id", :unique => true

  create_table "product_to_suppliers", :force => true do |t|
    t.string   "uid",         :limit => 400
    t.float    "price"
    t.integer  "product_id"
    t.integer  "supplier_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "product_views", :id => false, :force => true do |t|
    t.string   "token"
    t.integer  "product_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "product_views", ["token", "product_id"], :name => "index_product_views_on_token_and_product_id", :unique => true

  create_table "products", :force => true do |t|
    t.string   "name",                  :limit => 200,                :null => false
    t.integer  "status",                :limit => 1,                  :null => false
    t.string   "link_rewrite",          :limit => 200,                :null => false
    t.string   "meta_description",      :limit => 400
    t.string   "meta_keywords",         :limit => 400
    t.text     "short_description"
    t.text     "description"
    t.integer  "weight",                :limit => 3,   :default => 0
    t.float    "length"
    t.float    "height"
    t.float    "width"
    t.float    "price"
    t.integer  "type_id"
    t.integer  "brand_id"
    t.string   "color_name",            :limit => 200
    t.string   "color_code",            :limit => 6
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.integer  "cover_id"
    t.integer  "group_id"
    t.float    "ratio"
    t.integer  "ratio_u",               :limit => 2
    t.integer  "product_reviews_count"
    t.integer  "last_views",                           :default => 0
    t.integer  "discount_type_id",      :limit => 1
    t.float    "discount_value"
    t.float    "sort_price"
    t.string   "model_name",            :limit => 200,                :null => false
  end

  add_index "products", ["brand_id"], :name => "index_products_on_brand_id"
  add_index "products", ["link_rewrite"], :name => "index_products_on_link_rewrite", :unique => true
  add_index "products", ["price"], :name => "index_products_on_price"
  add_index "products", ["type_id"], :name => "index_products_on_type_id"

  create_table "products_groups", :force => true do |t|
    t.text     "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "products_in_carts", :force => true do |t|
    t.integer  "product_id"
    t.integer  "cart_id"
    t.integer  "count",                :limit => 2, :default => 0
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.float    "sale_price"
    t.float    "purchase_price"
    t.integer  "products_supplier_id"
  end

  add_index "products_in_carts", ["cart_id"], :name => "index_products_in_carts_on_cart_id"
  add_index "products_in_carts", ["product_id"], :name => "index_products_in_carts_on_product_id"

  create_table "products_suppliers", :force => true do |t|
    t.string   "name",       :limit => 200, :null => false
    t.text     "address"
    t.string   "site",       :limit => 200
    t.text     "note"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "products_type_properties", :force => true do |t|
    t.string   "name",        :limit => 100,                :null => false
    t.text     "description"
    t.string   "unit",        :limit => 10
    t.integer  "position",    :limit => 1,   :default => 0
    t.integer  "type_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "is_filter"
  end

  create_table "products_type_property_values", :force => true do |t|
    t.string   "value",      :limit => 300, :null => false
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "products_types", :force => true do |t|
    t.string   "name",             :limit => 100, :null => false
    t.string   "link_rewrite",     :limit => 100, :null => false
    t.string   "meta_description", :limit => 400
    t.string   "meta_keywords",    :limit => 400
    t.text     "description"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.text     "brands_ids"
    t.integer  "products_count"
  end

  add_index "products_types", ["link_rewrite"], :name => "index_products_types_on_link_rewrite", :unique => true

  create_table "regions", :force => true do |t|
    t.string   "name",       :limit => 100, :null => false
    t.integer  "position",   :limit => 1
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.boolean  "status",                    :null => false
  end

  create_table "sessions", :force => true do |t|
    t.string   "token",       :limit => 32,                   :null => false
    t.integer  "user_id"
    t.integer  "cart_id"
    t.string   "ip_hash",     :limit => 32,                   :null => false
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.boolean  "remember_me",               :default => true
  end

  create_table "user_addresses", :force => true do |t|
    t.string   "postcode",    :limit => 15
    t.text     "note"
    t.text     "market_note"
    t.string   "adds",        :limit => 250, :null => false
    t.integer  "user_id"
    t.integer  "city_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "user_auths", :force => true do |t|
    t.integer  "provider",   :limit => 1,   :null => false
    t.string   "uid",        :limit => 100, :null => false
    t.string   "password",   :limit => 32
    t.integer  "user_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "user_auths", ["provider", "uid"], :name => "index_user_auths_on_provider_and_uid", :unique => true

  create_table "user_data_verifieds", :force => true do |t|
    t.integer  "user_id"
    t.string   "token",      :limit => 32
    t.integer  "attempts",   :limit => 1
    t.integer  "field",      :limit => 1
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.boolean  "exp"
  end

  add_index "user_data_verifieds", ["user_id"], :name => "index_user_data_verifieds_on_user_id"

  create_table "user_password_recoveries", :force => true do |t|
    t.integer  "user_auth_id"
    t.string   "code"
    t.integer  "attempts",     :limit => 1
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "first_name",     :limit => 32
    t.string   "last_name",      :limit => 32
    t.string   "middle_name",    :limit => 32
    t.string   "email",          :limit => 64
    t.boolean  "email_verified"
    t.string   "phone",          :limit => 10
    t.boolean  "phone_verified"
    t.integer  "cart_id"
    t.text     "note"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

end
