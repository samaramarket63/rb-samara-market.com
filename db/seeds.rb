# encoding: UTF-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
main_category = Category.new(name:'Inside(bug_fixe)',status:-1,link_rewrite:'00')
main_category.save(validate:false)

r_samarskay = Region.create(name: 'Самарская область', status: true)
c_samara = City.create(name: 'Самара',  region_id: r_samarskay.id, status: true)
c_kinel = City.create(name: 'Кинель',  region_id: r_samarskay.id, status: true)

payment = Payment.create({
  name: 'Наличными при получении',
  description: 'Оплата заказа наличными при получении',
  full_description: '<p>Самый простой способ оплаты заказа в нашем <b>интернет магазине</b>.</p>
  <p>Оплата осуществляется наличными средствами при получении заказа.</p>',
  fields: ['phone']
  }, as: :admin)

delivery_mothod = DeliveryMethod.create({
  name: 'Доставка на дом',
  description: 'Доставка на дом',
  full_description: '<p>Доставка на дом является самым удобным способом получения заказа.</p>
    <p>Вам не придётся выходить дальше интернета, сотрудники нашего <b>интернет магазина</b> доставят заказ к дверям Вашего дома.</p>
  ',
  fields: ['phone', 'address']
  },as: :admin
)

delivery_mothod_2 = DeliveryMethod.create({
  name: 'Забрать в ПВЗ',
  description: 'Забрать заказа самостоятельно в пункте выдачи заказов',
  full_description: '<p>Заказ можно забрать самостоятельно в пункте выдачи заказов</p>
  ',
  fields: ['phone']
  }, as: :admin
)

DeliveryPayment.create({payment_id: payment.id, delivery_id: delivery_mothod.id}, as: :admin)
DeliveryPayment.create({payment_id: payment.id, delivery_id: delivery_mothod_2.id}, as: :admin)

Delivery.create({
  price: 99,
  extra_description: 'от Самара-Маркет',
  description: 'Доставка заказов на дом от Самара-Маркет',
  city_id: c_samara.id,
  delivery_method_id: delivery_mothod.id,
  price_for_free: 2000,
  note: 'Красноглинский и Куйбышевский районы + 50 рублей, даже если цена доставки 0(бесплатно).'
  },as: :admin)

Delivery.create({
  price: 100,
  extra_description: 'от Самара-Маркет',
  description: 'Доставка заказов на дом от Самара-Маркет',
  city_id: c_kinel.id,
  delivery_method_id: delivery_mothod.id,
  price_for_free: 3000,
  note: 'Доставка осуществляется только в выходные дни, при условии, что Вы оформите заказ до пятницы'
  },as: :admin)


Delivery.create({
  price: 0,
  extra_description: 'улица Маяковского, дом 81, магазин «Ромашка»',
  description: 'Забрать заказ в пункте выдачи по адресу улица Маяковского, дом 81, магазин «Ромашка»',
  city_id: c_kinel.id,
  delivery_method_id: delivery_mothod_2.id,
  price_for_free: 0,
  note: 'Доставка осуществляется только в выходные дни, при условии, что Вы оформите заказ до пятницы'
  },as: :admin)
