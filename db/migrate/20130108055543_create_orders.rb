class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user
      t.references :payment
      t.references :delivery
      t.references :address
      t.integer :status, limit: 1
      t.float :paid, precision: 6, scale:2
      t.timestamps
    end
    add_index :orders, :user_id
  end
end
