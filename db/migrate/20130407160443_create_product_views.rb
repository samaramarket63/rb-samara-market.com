class CreateProductViews < ActiveRecord::Migration
  def change
    create_table :product_views, id: false do |t|
      t.string :token
      t.references :product
      t.timestamps
    end
    add_index :product_views, [:token, :product_id], unique: true
    add_column :products, :last_views, :integer, default: 0
  end
end
