class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name, null: false, limit: 100
      t.integer :position, limit: 1
      t.references :region
      t.timestamps
    end
  end
end
