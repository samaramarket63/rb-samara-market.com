class CreateUserAddresses < ActiveRecord::Migration
  def change
    create_table :user_addresses do |t|
      t.string :postcode, limit: 15
      t.text :note, limit: 500
      t.text :market_note, limit: 500
      t.string :adds, limit: 250, null: false
      t.references :user
      t.references :city
      t.timestamps
    end
  end
end
