class CreateArticleBrands < ActiveRecord::Migration
  def change
    create_table :article_brands do |t|
      t.references :article
      t.string :ancor
      t.references :brand
      t.timestamps
    end
  end
end
