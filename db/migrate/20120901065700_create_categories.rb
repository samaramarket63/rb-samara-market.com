class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string    :name, limit: 100, null: false
      t.string    :link_rewrite, limit: 100, null: false
      t.boolean   :status, null: false
      t.integer   :parent_id, limit: 2
      t.string    :meta_keywords, limit: 400
      t.string    :meta_description, limit: 400
      t.text      :description
      t.timestamps
    end
    add_index(:categories, :link_rewrite, unique: true)
    add_index(:categories, :parent_id)
  end
end