class AddCountersProductsAndCategoriesCacheForBrands < ActiveRecord::Migration
  def change
    add_column :brands, :count_visible_products, :integer
    add_column :brands, :count_sales_products, :integer
    add_column :brands, :categories_list, :text
    add_column :brands, :categories_count, :integer
  end
end
