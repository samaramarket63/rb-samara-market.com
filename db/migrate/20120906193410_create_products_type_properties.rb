class CreateProductsTypeProperties < ActiveRecord::Migration
  def change
    create_table :products_type_properties do |t|
      t.string :name, limit: 100, null: false
      t.text :description
      t.string :unit, limit: 10
      t.integer :position, limit: 1, default: 0
      t.integer :type_id
      t.timestamps
    end
  end
end
