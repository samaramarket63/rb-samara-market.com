class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string  :name ,limit: 32, null: false
      t.text :description, limit: 1500
      t.text :full_description, limit: 5000
      t.float :commission_fixed, precision: 6, scale:2
      t.integer :commission_pct, limit:2
      t.timestamps
    end
  end
end
