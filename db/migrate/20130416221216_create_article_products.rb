class CreateArticleProducts < ActiveRecord::Migration
  def change
    create_table :article_products do |t|
      t.references :article
      t.string :ancor
      t.references :product
      t.timestamps
    end
  end
end
