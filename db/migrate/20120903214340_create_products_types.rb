class CreateProductsTypes < ActiveRecord::Migration
  def change
    create_table :products_types do |t|
      t.string :name, limit: 100, null: false
      t.string :link_rewrite, limit: 100, null: false
      t.string  :meta_description, limit: 400
      t.string  :meta_keywords, limit: 400
      t.text :description

      t.timestamps
    end
    #add_column :products, :type_id, :integer, limit: 3
    add_index :products_types, :link_rewrite, unique: true
  end
end