class AddFieldPeriodForReviews < ActiveRecord::Migration
  def change
    add_column :product_reviews, :period, :integer, limit: 1
  end

end
