class CreateCategoryViews < ActiveRecord::Migration
  def change
    create_table :category_views, id: false do |t|
      t.string :token
      t.references :category
      t.timestamps
    end
    add_index :category_views, [:token, :category_id], unique: true
    add_column :categories, :last_views, :integer, default: 0
  end
end
