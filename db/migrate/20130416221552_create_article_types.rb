class CreateArticleTypes < ActiveRecord::Migration
  def change
    create_table :article_types do |t|
      t.references :article
      t.string :ancor
      t.references :type
      t.timestamps
    end
  end
end
