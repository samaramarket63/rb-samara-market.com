class CreateOldProducts < ActiveRecord::Migration
  def change
    create_table :old_products do |t|
      t.string :name
      t.string :link_rewrite

      t.timestamps
    end
    add_index :old_products, :link_rewrite, unique: true
  end
end
