class ProductsInCartsAddPriceSaleAndPricePurchaseAndSupplier < ActiveRecord::Migration
  def change
    add_column :products_in_carts, :sale_price, :float,  precision: 6, scale:2
    add_column :products_in_carts, :purchase_price, :float,  precision: 6, scale:2
    add_column :products_in_carts, :products_supplier_id, :integer
  end
end