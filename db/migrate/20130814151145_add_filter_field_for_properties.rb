class AddFilterFieldForProperties < ActiveRecord::Migration
  def up
    add_column :products_type_properties, :is_filter, :boolean
  end

  def down
    remove_column :products_type_properties, :is_filter
  end
end
