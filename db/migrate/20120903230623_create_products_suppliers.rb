class CreateProductsSuppliers < ActiveRecord::Migration
  def change
    create_table :products_suppliers do |t|
      t.string :name, limit: 200, null: false
      t.text :address
      t.string :site, limit: 200
      t.text :note

      t.timestamps
    end
  end
end
