class CreateUserPasswordRecoveries < ActiveRecord::Migration
  def change
    create_table :user_password_recoveries do |t|
      t.references :user_auth
      t.string :code
      t.integer :attempts, limit: 1 
      t.timestamps
    end
  end
end
