class AddNoteForDelivery < ActiveRecord::Migration
  add_column :deliveries, :note, :string, limit: 300
end
