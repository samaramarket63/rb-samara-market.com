class CreateProductReviewVotes < ActiveRecord::Migration
  def change
    create_table :product_review_votes do |t|
      t.references :review 
      t.references :user
      t.integer :vote, limit: 1
      t.timestamps
    end
    add_index :product_review_votes, [:user_id, :review_id], unique: true
    add_index :product_review_votes, :review_id
    add_index :product_review_votes, :user_id
  end
end
