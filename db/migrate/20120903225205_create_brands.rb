class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string  :name, limit: 100, null: false
      t.string  :link_rewrite, limit: 100, null: false
      t.string  :short_description, limit: 600
      t.string  :meta_description, limit: 400
      t.string  :meta_keywords, limit: 400
      t.text :description

      t.timestamps
    end
    add_index :brands, :link_rewrite, unique: true
    add_column :categories, :brands_ids, :text
  end
end