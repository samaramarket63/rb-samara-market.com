class CreateIpBans < ActiveRecord::Migration
  def change
    create_table :ip_bans do |t|
      t.string :ip, limit: 32
      t.integer :operation, limit: 1
      t.timestamps
    end
    add_index :ip_bans, :ip
  end
end
