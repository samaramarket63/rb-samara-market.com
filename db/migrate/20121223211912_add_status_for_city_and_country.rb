class AddStatusForCityAndCountry < ActiveRecord::Migration
  def change
    add_column :regions, :status, :boolean, null: false 
    add_column :cities, :status, :boolean, null: false 
  end
end