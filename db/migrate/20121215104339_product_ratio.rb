class ProductRatio < ActiveRecord::Migration
  def change
    add_column :products, :ratio, :float, precision: 4, scale: 2
    add_column :products, :ratio_u, :integer, limit: 2
  end
end