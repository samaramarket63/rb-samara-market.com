class DeliveryAndPyymentAddRowFields < ActiveRecord::Migration
  def change
    add_column :delivery_methods, :fields, :string, limit: 250
    add_column :payments, :fields, :string, limit: 250
  end
end