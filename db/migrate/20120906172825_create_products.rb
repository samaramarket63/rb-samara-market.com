class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name, limit: 200, null: false
      t.integer :status, limit: 1, null: false
      t.string :link_rewrite, limit: 200, null: false
      t.string :meta_description, limit: 400
      t.string :meta_keywords, limit: 400
      t.text :short_description
      t.text :description
      t.integer :weight, limit: 3, default: 0
      t.float :length, precision: 3, scale:2
      t.float :height, precision: 3, scale:2
      t.float :width, precision: 3, scale:2
      t.float :price, precision: 6, scale:2
      t.integer :type_id
      t.integer :brand_id
      t.string :color_name, limit: 200
      t.string :color_code, limit: 6
      t.timestamps
    end
    add_index :products, :link_rewrite, unique: true
    add_index :products, :price
    add_index :products, :brand_id
    add_index :products, :type_id
  end
end
