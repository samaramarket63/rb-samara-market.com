class AddDeliveryFreeShipping < ActiveRecord::Migration
  def change
    add_column :deliveries, :price_for_free, :float,  precision: 6, scale:2
  end
end