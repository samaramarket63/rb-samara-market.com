class AddDiscountForOrder < ActiveRecord::Migration
  def change
    add_column :orders, :discount, :float, precision: 6, scale:2
    add_column :orders, :paid_bonus, :float, precision: 6, scale:2
  end
end
