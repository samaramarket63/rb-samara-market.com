class AddTypesCachesFileds < ActiveRecord::Migration
  def change
    add_column :products_types, :brands_ids, :text
    add_column :products_types, :products_count, :integer
  end
end