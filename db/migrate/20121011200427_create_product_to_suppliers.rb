class CreateProductToSuppliers < ActiveRecord::Migration
  def change
    create_table :product_to_suppliers do |t|
      t.string :uid, limit: 400
      t.float :price, precision: 3, scale:2
      t.references :product
      t.references :supplier
      t.timestamps
    end
  end
end
