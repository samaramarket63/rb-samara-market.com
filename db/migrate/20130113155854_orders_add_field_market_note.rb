class OrdersAddFieldMarketNote < ActiveRecord::Migration
  def change
    add_column :orders, :market_note, :text
  end
end
