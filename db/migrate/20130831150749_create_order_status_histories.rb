class CreateOrderStatusHistories < ActiveRecord::Migration
  def change
    create_table :order_status_histories do |t|
      t.references :order
      t.integer :status
      t.timestamps
    end
  end
end
