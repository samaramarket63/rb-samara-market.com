class UnionDiscountAndProduct < ActiveRecord::Migration
  def up
    add_column :products, :discount_type_id, :integer, limit: 1, default: nil
    add_column :products, :discount_value, :float, precision: 6, scale:2, default: nil
    add_column :products, :sort_price, :float,  precision: 6, scale:2, default: nil
    drop_table :product_discounts if table_exists? :product_discounts
    
  end

  def down
    remove_column :products, :discount_type_id
    remove_column :products, :discount_value
    remove_column :products, :sort_price
  end
end
