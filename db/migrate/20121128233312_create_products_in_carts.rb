class CreateProductsInCarts < ActiveRecord::Migration
  def change
    create_table :products_in_carts do |t|
      t.references :product
      t.references :cart
      t.integer :count, limit: 2, default: 0
      t.timestamps
    end
    add_index :products_in_carts, :product_id
    add_index :products_in_carts, :cart_id
  end
end
