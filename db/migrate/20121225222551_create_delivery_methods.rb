class CreateDeliveryMethods < ActiveRecord::Migration
  def change
    create_table :delivery_methods do |t|
      t.string  :name ,limit: 32, null: false
      t.text :description, limit: 1500
      t.text :full_description, limit: 5000
      t.timestamps
    end
  end
end
