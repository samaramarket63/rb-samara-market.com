class AddReviewsIndexAssociationsKeys < ActiveRecord::Migration
  add_index :product_reviews, [:user_id, :product_id], unique: true
  add_index :product_reviews, :product_id
end
