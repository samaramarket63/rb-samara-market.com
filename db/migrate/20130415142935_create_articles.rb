class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :name, null: false
      t.string :link_rewrite, null: false
      t.string :meta_keywords
      t.string :meta_description
      t.string :title
      t.text :text
      t.text :text_compile
      t.text :intro
      t.timestamps
    end
    add_index :articles, :link_rewrite, unique: true
  end
end
