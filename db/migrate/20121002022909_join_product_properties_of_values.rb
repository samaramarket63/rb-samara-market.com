class JoinProductPropertiesOfValues < ActiveRecord::Migration
  def change
    create_table :join_product_properties_of_values, id: false do |t|
      t.references :value
      t.references :property
      t.timestamps
    end
    add_index :join_product_properties_of_values, :value_id
    add_index :join_product_properties_of_values, :property_id
  end
end
