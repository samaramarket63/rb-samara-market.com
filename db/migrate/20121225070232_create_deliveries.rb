class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.float :price, precision: 6, scale:2
      t.text :extra_description, limit: 200
      t.text :description, limit: 3000
      t.references :city
      t.references :delivery_method
      t.timestamps
    end
    add_index :deliveries, :city_id
    add_index :deliveries, :delivery_method_id
  end
end
