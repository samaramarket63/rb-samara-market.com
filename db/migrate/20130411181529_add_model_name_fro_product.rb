class AddModelNameFroProduct < ActiveRecord::Migration
  def up
    add_column :products, :model_name, :string, limit: 200, null: false
  end

  def down
    remove_column :products, :model_name
  end
end
