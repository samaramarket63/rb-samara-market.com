class CreateProductReviews < ActiveRecord::Migration
  def change
    create_table :product_reviews do |t|
      t.integer :stars, limit:1
      t.string :title
      t.text :value
      t.text :limitations
      t.text :comment
      t.integer :vote_up, limit: 3
      t.integer :vote_down, limit: 3
      t.string :user_name
      t.timestamps
    end
  end
end
