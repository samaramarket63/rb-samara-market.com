class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name, limit: 32
      t.string :last_name, limit: 32
      t.string :middle_name, limit: 32
      t.string :email, limit: 64
      t.boolean :email_verified
      t.string :phone, limit: 10
      t.boolean :phone_verified
      t.references :cart
      t.text :note
      t.timestamps
    end
  end
end
