class AddToCategoryShow < ActiveRecord::Migration
  def up
    add_column :categories, :show, :boolean
  end

  def down
    remove_column :categories, :show
  end
end
