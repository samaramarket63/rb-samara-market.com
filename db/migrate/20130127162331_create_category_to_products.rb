class CreateCategoryToProducts < ActiveRecord::Migration
  def change
    create_table :category_to_products, timestamps: false do |t|
      t.references :product
      t.references :category
    end
    add_index :category_to_products, [:category_id, :product_id], unique: true
    add_index :category_to_products, :product_id
    add_index :category_to_products, :category_id
  end
end
