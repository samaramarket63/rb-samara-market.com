class CreateProductImages < ActiveRecord::Migration
  def change
    create_table :product_images do |t|
      t.string :alt
      t.string :image
      t.integer :position, limit: 1
      t.references :product
      t.timestamps
    end
    add_index :product_images, :product_id
    add_column :products, :cover_id, :integer
  end
end
