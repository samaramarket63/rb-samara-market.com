class AddReviewsAssociationsKeys < ActiveRecord::Migration
  def change
    add_column :product_reviews, :user_id, :integer
    add_column :product_reviews, :product_id, :integer
  end

end