class CreateUserAuths < ActiveRecord::Migration
  def change
    create_table :user_auths do |t|
      t.integer :provider, null: false, limit: 1
      t.string :uid, null: false, limit: 100
      t.string :password, limit: 32
      t.references :user
      t.timestamps
    end
    add_index :user_auths, [:provider, :uid], unique: true
  end
end
