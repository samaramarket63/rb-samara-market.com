class AddReviewsFieldModerated < ActiveRecord::Migration
  def change
    add_column :product_reviews, :moderated, :boolean
  end

end
