class AddCategoryCounters < ActiveRecord::Migration
  def up
    add_column :categories, :products_count, :integer
    add_column :categories, :products_visible, :integer
    add_column :categories, :products_in_stock, :integer
  end

  def down
    remove_column :categories, :products_count
    remove_column :categories, :products_visible
    remove_column :categories, :products_in_stock
  end
end
