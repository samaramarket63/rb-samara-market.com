class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :phone
      t.string :password, limit: 32

      t.timestamps
    end
  end
end
