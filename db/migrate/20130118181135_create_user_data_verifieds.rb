class CreateUserDataVerifieds < ActiveRecord::Migration
  def change
    create_table :user_data_verifieds do |t|
      t.references :user
      t.string :token, limit: 32
      t.integer :attempts, limit:1
      t.integer :field, limit:1
      t.timestamps
    end
    add_index :user_data_verifieds, :user_id
  end
end
