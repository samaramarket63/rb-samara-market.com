class CreateDeliveryPayments < ActiveRecord::Migration
  def change
    create_table :delivery_payments do |t|
      t.references :delivery
      t.references :payment
      t.timestamps
    end
    add_index :delivery_payments, :delivery_id
    add_index :delivery_payments, :payment_id
  end
end
