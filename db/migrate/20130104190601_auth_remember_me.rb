class AuthRememberMe < ActiveRecord::Migration
  def change
    add_column :sessions, :remember_me, :boolean, default: true
  end
end