class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :name, null: false, limit: 100
      t.integer :position, limit: 1
      
      t.timestamps
    end
  end
end
