class OrderExtra < ActiveRecord::Migration
  def change
    add_column :orders, :note, :text
    add_column :orders, :delivery_price, :float,  precision: 6, scale:2
  end
end