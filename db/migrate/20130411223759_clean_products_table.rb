class CleanProductsTable < ActiveRecord::Migration
  def up
    remove_column :products, :string if column_exists?(:products, :string)
  end
end
