# Add fields from gem Nested Set
class CategoryAddNestedSet < ActiveRecord::Migration
  def up
    add_column :categories, :lft, :integer, limit: 2
    add_column :categories, :rgt, :integer, limit: 2
    add_column :categories, :depth, :integer, limit: 1
  end

  def down
    remove_column :categories, :lft
    remove_column :categories, :rgt
    remove_column :categories, :depth
  end
end
