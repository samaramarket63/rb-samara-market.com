class CreateCategoryAdvertisings < ActiveRecord::Migration
  def change
    create_table :category_advertisings do |t|
      t.string :name, null: false, limit: 150
      t.integer :position, limit: 1, null: false
      t.text :description, limit: 1000
      t.integer :p_top, limit: 2, null: false
      t.integer :p_left, limit: 2, null: false
      t.integer :p_bottom, limit: 2, null: false
      t.integer :p_right, limit: 2, null: false
      t.string :image
      t.string :background
      t.string :background_color, limit: 6
      t.string :background_repeat, limit: 1, null: false
      t.integer :link_model, null: false, limit: 1
      t.integer :link_id, null: false
      t.integer :width_category,limit: 2
      t.references :category
      t.timestamps
    end
    add_column :categories, :adword_id, :integer
  end
end
