class CreateProductsTypePropertyValues < ActiveRecord::Migration
  def change
    create_table :products_type_property_values do |t|
      t.string :value, limit: 300, null: false

      t.timestamps
    end
  end
end
