class CreateProductProperties < ActiveRecord::Migration
  def change
    create_table :product_properties do |t|
      t.references :product
      t.references :property
      t.timestamps
    end
    add_index :product_properties, :product_id
    add_index :product_properties, :property_id
  end
end
