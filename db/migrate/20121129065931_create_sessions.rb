class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string  :token ,limit: 32, null: false
      t.references  :user
      t.references  :cart
      t.string  :ip_hash ,limit: 32, null: false
      t.timestamps
    end
  end
end
