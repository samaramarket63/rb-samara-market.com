class CreateProductsGroups < ActiveRecord::Migration
  def change
    create_table :products_groups do |t|
      t.text :name, limit: 300
 
      t.timestamps
    end
    add_column :products, :group_id, :integer
  end
end
