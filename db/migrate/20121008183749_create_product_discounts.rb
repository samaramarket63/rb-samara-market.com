class CreateProductDiscounts < ActiveRecord::Migration
  def change
    create_table :product_discounts do |t|
      t.integer :type_id, limit: 1
      t.float :value, precision: 6, scale:2
      t.references :product
      t.timestamps
    end
    add_index :product_discounts, :product_id, unique:true
  end
end
