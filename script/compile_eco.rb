require "eco"
require 'pathname'
require "yui/compressor"
require "coffee-script"

compressor = YUI::JavaScriptCompressor.new munge: true

t_path = File.expand_path('../../app/views/web/orders/form_templates_js/src',  __FILE__)

d = Dir.glob t_path+'/*.eco'

d.each do |f|
  pf = Pathname.new f
  File.open(t_path+'/../'+pf.basename.to_s.gsub!('.eco', '.html'),"w+"){|file|
    file.puts compressor.compress(Eco.compile(pf.read))
  }
  
end


t_path = File.expand_path('../../app/views/web/orders/js/src',  __FILE__)

d = Dir.glob t_path+'/*.js'

d.each do |f|
  pf = Pathname.new f
  File.open(t_path+'/../'+pf.basename.to_s.gsub!('.js', '.html'),"w+"){|file|
    file.puts compressor.compress(CoffeeScript.compile(pf.read))
    #file.puts CoffeeScript.compile(pf.read)
  }
  
end