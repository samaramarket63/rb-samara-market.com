# == Schema Information
#
# Table name: product_discounts
#
#  id         :integer          not null, primary key
#  type_id    :integer
#  value      :float
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_discount do
  end
end
