# == Schema Information
#
# Table name: products_suppliers
#
#  id         :integer          not null, primary key
#  name       :string(200)      not null
#  address    :text
#  site       :string(200)
#  note       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :products_supplier do
    name "MyString"
    address "MyString"
    site "MyString"
    note "MyString"
  end
end
