# == Schema Information
#
# Table name: category_to_products
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  category_id :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :category_to_product, :class => 'CategoryToProducts' do
  end
end
