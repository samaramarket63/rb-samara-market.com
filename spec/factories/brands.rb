# == Schema Information
#
# Table name: brands
#
#  id                     :integer          not null, primary key
#  name                   :string(100)      not null
#  link_rewrite           :string(100)      not null
#  short_description      :string(600)
#  meta_description       :string(400)
#  meta_keywords          :string(400)
#  description            :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  logo                   :string(255)
#  count_visible_products :integer
#  count_sales_products   :integer
#  categories_list        :text
#  categories_count       :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :brand do
    name "MyString"
    link_rewrite "MyString"
    meta_description "MyString"
    meta_keywords "MyString"
    short_description "MyString"
    description "MyText"
  end
end
