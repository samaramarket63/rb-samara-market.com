# == Schema Information
#
# Table name: payments
#
#  id               :integer          not null, primary key
#  name             :string(32)       not null
#  description      :text
#  full_description :text
#  action           :string(20)
#  data             :text
#  commission_fixed :float
#  commission_pct   :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  fields           :string(250)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
  end
end
