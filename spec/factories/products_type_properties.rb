# == Schema Information
#
# Table name: products_type_properties
#
#  id          :integer          not null, primary key
#  name        :string(100)      not null
#  description :text
#  unit        :string(10)
#  position    :integer          default(0)
#  type_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :products_type_property do
    name "MyString"
    description "MyText"
    unit "MyString"
    position 1
  end
end
