# == Schema Information
#
# Table name: delivery_methods
#
#  id               :integer          not null, primary key
#  name             :string(32)       not null
#  description      :text
#  full_description :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  fields           :string(250)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :delivery_method do
  end
end
