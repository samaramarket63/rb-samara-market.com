# == Schema Information
#
# Table name: products_type_property_values
#
#  id         :integer          not null, primary key
#  value      :string(300)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :products_type_property_value, :class => 'ProductsTypePropertyValues' do
    value "MyString"
  end
end
