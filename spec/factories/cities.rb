# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(100)      not null
#  position   :integer
#  region_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :boolean          not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :city do
    name "MyString"
    position 1
  end
end
