# == Schema Information
#
# Table name: product_review_votes
#
#  id         :integer          not null, primary key
#  review_id  :integer
#  user_id    :integer
#  vote       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_review_vote do
  end
end
