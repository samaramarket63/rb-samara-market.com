# == Schema Information
#
# Table name: category_advertisings
#
#  id                :integer          not null, primary key
#  name              :string(150)      not null
#  position          :integer          not null
#  description       :text
#  p_top             :integer          not null
#  p_left            :integer          not null
#  p_bottom          :integer          not null
#  p_right           :integer          not null
#  image             :string(255)
#  background        :string(255)
#  background_color  :string(6)
#  background_repeat :string(1)        not null
#  link_model        :integer          not null
#  link_id           :integer          not null
#  width_category    :integer
#  category_id       :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :category_advertising do
    name "MyString"
    position 1
    description "MyText"
    p_top 1
    p_left 1
    p_bottom 1
    p_right 1
    image "MyString"
    background "MyString"
    link_model 1
    link_id 1
  end
end
