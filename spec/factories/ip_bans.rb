# == Schema Information
#
# Table name: ip_bans
#
#  id         :integer          not null, primary key
#  ip         :string(32)
#  operation  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ip_ban do
  end
end
