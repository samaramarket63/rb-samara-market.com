# == Schema Information
#
# Table name: users
#
#  id             :integer          not null, primary key
#  first_name     :string(32)
#  last_name      :string(32)
#  middle_name    :string(32)
#  email          :string(64)
#  email_verified :boolean
#  phone          :string(10)
#  phone_verified :boolean
#  cart_id        :integer
#  note           :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
  end
end
