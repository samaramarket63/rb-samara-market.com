# == Schema Information
#
# Table name: orders
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  payment_id     :integer
#  delivery_id    :integer
#  address_id     :integer
#  status         :integer
#  paid           :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  note           :text
#  delivery_price :float
#  cart_id        :integer
#  market_note    :text
#  discount       :float
#  paid_bonus     :float
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order do
  end
end
