# == Schema Information
#
# Table name: user_data_verifieds
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  token      :string(32)
#  attempts   :integer
#  field      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  exp        :boolean
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_data_verified do
  end
end
