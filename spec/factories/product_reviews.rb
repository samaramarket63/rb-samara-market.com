# == Schema Information
#
# Table name: product_reviews
#
#  id          :integer          not null, primary key
#  stars       :integer
#  title       :string(255)
#  value       :text
#  limitations :text
#  comment     :text
#  vote_up     :integer
#  vote_down   :integer
#  user_name   :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  period      :integer
#  moderated   :boolean
#  user_id     :integer
#  product_id  :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_review do
  end
end
