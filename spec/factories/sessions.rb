# == Schema Information
#
# Table name: sessions
#
#  id          :integer          not null, primary key
#  token       :string(32)       not null
#  user_id     :integer
#  cart_id     :integer
#  ip_hash     :string(32)       not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  remember_me :boolean          default(TRUE)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :session do
  end
end
