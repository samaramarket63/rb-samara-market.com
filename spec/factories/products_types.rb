# == Schema Information
#
# Table name: products_types
#
#  id               :integer          not null, primary key
#  name             :string(100)      not null
#  link_rewrite     :string(100)      not null
#  meta_description :string(400)
#  meta_keywords    :string(400)
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  brands_ids       :text
#  products_count   :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :products_type do
    name "MyString"
    link_rewrite "MyString"
    meta_description "MyString"
    meta_keywords "MyString"
    description "MyText"
  end
end
