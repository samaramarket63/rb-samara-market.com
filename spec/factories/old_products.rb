# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :old_product do
    name "MyString"
    link_rewrite "MyString"
  end
end
