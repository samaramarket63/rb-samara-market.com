# == Schema Information
#
# Table name: product_to_suppliers
#
#  id          :integer          not null, primary key
#  uid         :string(400)
#  price       :float
#  product_id  :integer
#  supplier_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_to_supplier do
  end
end
