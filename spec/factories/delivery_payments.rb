# == Schema Information
#
# Table name: delivery_payments
#
#  id          :integer          not null, primary key
#  delivery_id :integer
#  payment_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :delivery_payment do
  end
end
