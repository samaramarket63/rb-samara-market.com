# == Schema Information
#
# Table name: regions
#
#  id         :integer          not null, primary key
#  name       :string(100)      not null
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :boolean          not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :region do
    name "MyString"
    number 1
    position 1
  end
end
