# == Schema Information
#
# Table name: user_auths
#
#  id         :integer          not null, primary key
#  provider   :integer          not null
#  uid        :string(100)      not null
#  password   :string(32)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_auth do
    provider 1
    uid "MyString"
    password "MyString"
  end
end
