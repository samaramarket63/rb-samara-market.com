# == Schema Information
#
# Table name: products_in_carts
#
#  id                   :integer          not null, primary key
#  product_id           :integer
#  cart_id              :integer
#  count                :integer          default(0)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  sale_price           :float
#  purchase_price       :float
#  products_supplier_id :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :products_in_cart do
  end
end
