# == Schema Information
#
# Table name: product_properties
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  property_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_property, :class => 'ProductProperties' do
  end
end
