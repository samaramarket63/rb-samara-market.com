# == Schema Information
#
# Table name: employees
#
#  id         :integer          not null, primary key
#  phone      :string(255)
#  password   :string(32)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :employee do
    phone "MyString"
    password "MyString"
  end
end
