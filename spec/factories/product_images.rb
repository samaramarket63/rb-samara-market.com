# == Schema Information
#
# Table name: product_images
#
#  id         :integer          not null, primary key
#  alt        :string(255)
#  image      :string(255)
#  position   :integer
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_image, :class => 'ProductImages' do
    alt "MyString"
    image "MyString"
    position 1
  end
end
