# == Schema Information
#
# Table name: user_password_recoveries
#
#  id           :integer          not null, primary key
#  user_auth_id :integer
#  code         :string(255)
#  attempts     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_password_recovery do
  end
end
