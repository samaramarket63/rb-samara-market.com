# == Schema Information
#
# Table name: deliveries
#
#  id                 :integer          not null, primary key
#  price              :float
#  extra_description  :text(255)
#  description        :text
#  city_id            :integer
#  delivery_method_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  price_for_free     :float
#  note               :string(300)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :delivery do
  end
end
