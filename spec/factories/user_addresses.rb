# == Schema Information
#
# Table name: user_addresses
#
#  id          :integer          not null, primary key
#  postcode    :string(15)
#  note        :text
#  market_note :text
#  adds        :string(250)      not null
#  user_id     :integer
#  city_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_address do
    postcode "MyString"
    note "MyText"
    market_note "MyText"
    adds "MyString"
  end
end
