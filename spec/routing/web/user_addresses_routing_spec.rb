require "spec_helper"

describe Web::UserAddressesController do
  describe "routing" do

    it "routes to #index" do
      get("/web/user_addresses").should route_to("web/user_addresses#index")
    end

    it "routes to #new" do
      get("/web/user_addresses/new").should route_to("web/user_addresses#new")
    end

    it "routes to #show" do
      get("/web/user_addresses/1").should route_to("web/user_addresses#show", :id => "1")
    end

    it "routes to #edit" do
      get("/web/user_addresses/1/edit").should route_to("web/user_addresses#edit", :id => "1")
    end

    it "routes to #create" do
      post("/web/user_addresses").should route_to("web/user_addresses#create")
    end

    it "routes to #update" do
      put("/web/user_addresses/1").should route_to("web/user_addresses#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/web/user_addresses/1").should route_to("web/user_addresses#destroy", :id => "1")
    end

  end
end
