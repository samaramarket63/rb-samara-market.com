# == Schema Information
#
# Table name: products
#
#  id                    :integer          not null, primary key
#  name                  :string(200)      not null
#  status                :integer          not null
#  link_rewrite          :string(200)      not null
#  meta_description      :string(400)
#  meta_keywords         :string(400)
#  short_description     :text
#  description           :text
#  weight                :integer          default(0)
#  length                :float
#  height                :float
#  width                 :float
#  price                 :float
#  type_id               :integer
#  brand_id              :integer
#  color_name            :string(200)
#  string                :string(6)
#  color_code            :string(6)
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  cover_id              :integer
#  group_id              :integer
#  ratio                 :float
#  ratio_u               :integer
#  product_reviews_count :integer
#

require 'spec_helper'

describe Product do
  pending "add some examples to (or delete) #{__FILE__}"
end
