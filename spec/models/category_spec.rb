# encoding: UTF-8
# == Schema Information
#
# Table name: categories
#
#  id               :integer          not null, primary key
#  name             :string(100)      not null
#  link_rewrite     :string(100)      not null
#  status           :boolean          not null
#  parent_id        :integer
#  meta_keywords    :string(400)
#  meta_description :string(400)
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  lft              :integer
#  rgt              :integer
#  depth            :integer
#  brands_ids       :text
#  adword_id        :integer
#  types_ids        :text
#

require 'spec_helper'

describe Category do
  
  it "validation name.length > 2" do
    category = FactoryGirl.build(:category)
    category.should be_valid
    category.name = ''
    category.should_not be_valid
    category.name = 'a'
    category.should_not be_valid
  end
  it "validation link_rewrite.length > 2" do
    category = FactoryGirl.build(:category)
    category.should be_valid
    category.link_rewrite = ''
    category.should_not be_valid
    category.link_rewrite = 'a'
    category.should_not be_valid
  end
  it "validation link_rewrite is unique" do
    category = FactoryGirl.create(:category)
    category2 = FactoryGirl.build(:category, link_rewrite: category.link_rewrite)
    category2.should_not be_valid

  end

end
