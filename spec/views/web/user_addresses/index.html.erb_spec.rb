require 'spec_helper'

describe "web/user_addresses/index" do
  before(:each) do
    assign(:web_user_addresses, [
      stub_model(Web::UserAddress),
      stub_model(Web::UserAddress)
    ])
  end

  it "renders a list of web/user_addresses" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
  end
end
