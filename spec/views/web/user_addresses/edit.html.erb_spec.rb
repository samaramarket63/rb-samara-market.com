require 'spec_helper'

describe "web/user_addresses/edit" do
  before(:each) do
    @web_user_address = assign(:web_user_address, stub_model(Web::UserAddress))
  end

  it "renders the edit web_user_address form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => web_user_addresses_path(@web_user_address), :method => "post" do
    end
  end
end
