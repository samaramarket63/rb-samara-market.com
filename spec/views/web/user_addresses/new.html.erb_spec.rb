require 'spec_helper'

describe "web/user_addresses/new" do
  before(:each) do
    assign(:web_user_address, stub_model(Web::UserAddress).as_new_record)
  end

  it "renders new web_user_address form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => web_user_addresses_path, :method => "post" do
    end
  end
end
